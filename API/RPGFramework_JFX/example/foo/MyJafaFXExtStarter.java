/**
 * 
 */
package foo;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.prelle.javafx.ModernUI;
import org.prelle.rpgframework.jfx.CardTile;
import org.prelle.rpgframework.jfx.CharacterViewScreen;

/**
 * @author prelle
 *
 */
public class MyJafaFXExtStarter extends Application {

	private final static Logger logger = Logger.getLogger("rpgtool.jfx");

	//--------------------------------------------------------------------
	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		launch(args);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		int value = 1;
		
		Parent content = null;
		
		switch (value) {
		case 0:
			CardTile tile = new CardTile("skills","Fertigkeiten");
			tile.setText("Fertigkeiten");
			tile.setAttentionFlag(true);
			tile.setOnAction(event -> logger.debug("Hallo Welt"));
			Button b = new Button("Button");
			tile.setBackside(b);
			content = tile;
			break;
		case 1:
			CharacterViewScreen vscreen = new CharacterViewScreen();
			vscreen.setId("deleteme");
			vscreen.setTitle("Hallo Welt");
			CardTile tile1 = new CardTile("skills","Fertigkeiten");
			tile1.setAttentionFlag(true);
			tile1.setOnAction(event -> logger.debug("Hallo Welt"));
			CardTile tile2 = new CardTile("spells","Zauber");
			tile2.setAttentionFlag(false);
			tile2.setOnAction(event -> logger.debug("Hello World"));
			CardTile tile3 = new CardTile("moveme","Schieb mich");
			tile3.setAttentionFlag(false);
			tile3.setOnAction(event -> logger.debug("Schieb ab"));
			tile3.setOnDragDetected(event -> logger.info("Drag"));
			vscreen.getItems().addAll(tile1, tile2, tile3);
			content = vscreen;
			break;
		}

		Scene scene = new Scene(content);
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);
		
		ModernUI.initialize(scene);
		scene.getStylesheets().add("css/rpgframework.css");

		primaryStage.show();
		System.out.println("Showing "+content);
	}

}

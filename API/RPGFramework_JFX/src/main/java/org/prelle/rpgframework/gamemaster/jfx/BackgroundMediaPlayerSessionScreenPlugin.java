/**
 * 
 */
package org.prelle.rpgframework.gamemaster.jfx;

import java.net.URI;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.gamemaster.jfx.SessionScreenPlugin;
import de.rpgframework.session.SessionScreen.DisplayHint;
import de.rpgframework.session.SessionScreenLevel;
import javafx.collections.FXCollections;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.shape.Rectangle;

/**
 * @author prelle
 *
 */
public class BackgroundMediaPlayerSessionScreenPlugin implements SessionScreenPlugin {

	private final static Logger logger = Logger.getLogger("salomon");

	private static ResourceBundle RES = ResourceBundle.getBundle("i18n/babylon-jfx");

	private int width, height;
	private MediaView iViewGM;
	private Media shownImage;
	
	private ChoiceBox<DisplayHint> cbScaleMode;
	
	private VBox configPane;
	private VBox contentGM; 
	
	//-------------------------------------------------------------------
	public BackgroundMediaPlayerSessionScreenPlugin() {
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbScaleMode = new ChoiceBox<>(FXCollections.observableArrayList(DisplayHint.values()));
		cbScaleMode.setValue(DisplayHint.SCALE);
		iViewGM = new MediaView();
		
		try {
			URI img = ClassLoader.getSystemResource("images/splash.png").toURI();
			setMediaURI(img);
		} catch (Exception e) {
			logger.error("Failed setting splash screen: "+e);
		}
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		configPane = new VBox();
		Label hdScaleMode = new Label(RES.getString("sessscreenplugin.bgimage.scalemode"));
		
		configPane.getChildren().addAll(hdScaleMode, cbScaleMode);
		
		Group grp = new Group(iViewGM);
		contentGM = new VBox(grp);
		contentGM.setStyle("-fx-background-color: black");
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbScaleMode.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshScale());
	}
	
	//-------------------------------------------------------------------
	public void setMediaURI(URI uri) {
		logger.debug("Set uri "+uri);
		String uriString = uri.toString();
		if (uriString.startsWith("file:/") && !uriString.startsWith("file:///"))
			uriString = "file:///"+uriString.substring(6);

		logger.debug("open "+uriString);
		Media pick = new Media(uriString);
		MediaPlayer player = new MediaPlayer(pick);
		player.setOnError(()->
		logger.error("media error"+player.getError().toString()));
		player.play();

		shownImage = pick;
		refreshScale();
	}
	
	//-------------------------------------------------------------------
	private void refreshScale() {
		if (shownImage==null)
			return;
		double idealScaleX = width / shownImage.getWidth();
		double idealScaleY = height / shownImage.getHeight();
		double factor = 0;
		switch (cbScaleMode.getValue()) {
		case SCALE:
			factor = Math.min(idealScaleX, idealScaleY);
			logger.info("SCALE: scale "+shownImage.getWidth()+"x"+shownImage.getHeight()+" with factor "+factor);
			if (factor==idealScaleX) {
				logger.debug("      Fith Width "+width);
				iViewGM.setFitWidth(width);
				iViewGM.setFitHeight(0);
				iViewGM.setPreserveRatio(true);
			} else {
				logger.debug("      Fith Height "+height);
				iViewGM.setFitHeight(height);
				iViewGM.setFitWidth(0);
				iViewGM.setPreserveRatio(true);
			}
			break;
		case FILL:
			factor = Math.max(idealScaleX, idealScaleY);
			logger.info("FILL: scale "+shownImage.getWidth()+"x"+shownImage.getHeight()+" with factor "+factor);
			if (factor==idealScaleX) {
				logger.debug("      Fith Width "+width);
				iViewGM.setFitWidth(width);
				iViewGM.setFitHeight(0);
				iViewGM.setPreserveRatio(true);
			} else {
				logger.debug("      Fith Height "+height);
				iViewGM.setFitHeight(height);
				iViewGM.setFitWidth(0);
				iViewGM.setPreserveRatio(true);
			}
			break;
		}
//		Rectangle maxClip = new Rectangle(1920, 1080);
		iViewGM.setClip(new Rectangle(1920, 1080));

		BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_CHANGED);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getName()
	 */
	@Override
	public String getName() {
		return RES.getString("sessscreenplugin.bgimage.name");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getLayer()
	 */
	@Override
	public SessionScreenLevel getLayer() {
		return SessionScreenLevel.BACKGROUND;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#setSize(int, int)
	 */
	@Override
	public void setSize(int width, int height) {
		this.height = height;
		this.width  = width;
		refreshScale();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getSettingsNode()
	 */
	@Override
	public Node getSettingsNode() {
		return configPane;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getContentNode(de.rpgframework.gamemaster.jfx.SessionScreenPlugin.Viewer)
	 */
	@Override
	public Node getContentNode(Viewer viewer) {
		return contentGM;
	}

}

package org.prelle.rpgframework.jfx;

import java.util.List;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class AttentionPane extends StackPane {
		
		private Canvas signLayer;
		private Node child;
		private Tooltip tip;
		
		//-------------------------------------------------------------------
		public AttentionPane(Node child) {
			super(child);
			this.child = child;
			
			signLayer = new Canvas(45,45);
			signLayer.setVisible(false);
			tip = new Tooltip();
//			signLayer.setPickOnBounds(value);
			this.getChildren().add(signLayer);
			StackPane.setAlignment(signLayer, Pos.BOTTOM_LEFT);
			GraphicsContext gc = signLayer.getGraphicsContext2D();
			gc.setFont(new Font("Segoe UI Symbol", 20));
			gc.setFill(Color.RED);
			gc.fillText("\uE218", 10, 30);
			gc.setFill(Color.WHITE);
			gc.fillText("\uE171", 10, 30);
			signLayer.setOnMouseClicked(event -> {
				child.fireEvent(event);
			});
		}
		
		//-------------------------------------------------------------------
		public AttentionPane(Node child, Pos alignment) {
			this(child);
			StackPane.setAlignment(signLayer, alignment);
		}
		
		//-------------------------------------------------------------------
		public void setAttentionFlag(boolean needsAttention) {
			signLayer.setVisible(needsAttention);
		}
		
		//-------------------------------------------------------------------
		public void setAttentionToolTip(List<String> list) {
			if (list==null || list.isEmpty()) {
				Tooltip.uninstall(signLayer, tip);
			} else {
				String txt = String.join("\n",  list);
				tip.setText(txt);
				Tooltip.install(signLayer, tip);
			}
		}
		
		//-------------------------------------------------------------------
		public Node getChild() {
			return child;
		}
	}
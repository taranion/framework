/**
 * 
 */
package org.prelle.rpgframework.jfx;

import org.apache.log4j.Logger;
import org.prelle.javafx.skin.ManagedScreenStructuredSkin;

import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class CharacterViewScreenSkin extends ManagedScreenStructuredSkin {

	private final static Logger logger = Logger.getLogger("rpgtool.jfx");

	private HBox content;
	private ImageView ivPortrait;
	private FlowPane flow;
	private VBox baseBlock;

	//-------------------------------------------------------------------
	public CharacterViewScreenSkin(CharacterViewScreen control) {
		super(control);
		initComponents();
		initLayout();
		initStyle();

		new CharacterViewScreenBehaviour(control, this);
		initInteractivity();
		
		getSkinnable().setContent(content);
		if (control.getImage()!=null)
			ivPortrait.setImage(control.getImage());
		if (control.getSide()!=null)
			baseBlock.getChildren().add(control.getSide());
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		baseBlock = new VBox();
		baseBlock.getStyleClass().add("base-block");
		baseBlock.setMaxHeight(Double.MAX_VALUE);
		ivPortrait = new ImageView();
		ivPortrait.setFitWidth(256);
		ivPortrait.setFitHeight(256);
		ivPortrait.setPreserveRatio(true);
		flow = new FlowPane(Orientation.VERTICAL);
		content = new HBox();
		content.getStyleClass().add("character-viewscreen");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		baseBlock.setAlignment(Pos.TOP_CENTER);
		HBox.setMargin(baseBlock, new Insets(40,0,40,0));
		baseBlock.getChildren().add(ivPortrait);
		VBox.setMargin(ivPortrait, new Insets(-40,0,0,0));
		
		flow.setVgap(20);
		flow.setHgap(20);
		flow.setMaxHeight(Double.MAX_VALUE);
		ScrollPane scroll = new ScrollPane(flow);
		scroll.setMaxHeight(Double.MAX_VALUE);
		scroll.setFitToHeight(true);
		
		content.setSpacing(40);
		content.getChildren().addAll(baseBlock, scroll);
		
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		baseBlock.getStyleClass().add("section-block");
		baseBlock.getStyleClass().addAll("section-bar","text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		((CharacterViewScreen)getSkinnable()).getItems().addListener(new ListChangeListener<CardTile>() {

			@Override
			public void onChanged(javafx.collections.ListChangeListener.Change<? extends CardTile> c) {
				while (c.next()) {
					if (c.wasAdded()) {
						logger.debug("Add cardtiles on list change "+c);
						flow.getChildren().addAll(c.getFrom(), c.getAddedSubList());
					} else
					if (c.wasRemoved()) {
						flow.getChildren().removeAll(c.getRemoved());
					} else
						logger.error(getClass()+".initInteractivity: Operation "+c+" not supported");
				}
			}
		});
		
		((CharacterViewScreen)getSkinnable()).imageProperty().addListener((ov,o,n) -> ivPortrait.setImage(n));
		((CharacterViewScreen)getSkinnable()).sideProperty().addListener((ov,o,n) -> {
			logger.info("basicData changed");
			baseBlock.getChildren().clear();
			if (n!=null)
				baseBlock.getChildren().add(n);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @return the flow
	 */
	FlowPane getFlow() {
		return flow;
	}

}

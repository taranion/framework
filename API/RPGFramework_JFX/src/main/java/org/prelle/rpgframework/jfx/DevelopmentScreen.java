/**
 * 
 */
package org.prelle.rpgframework.jfx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.skin.ManagedScreenStructuredSkin;

import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.modification.Modification;
import javafx.beans.property.BooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public abstract class DevelopmentScreen extends ManagedScreen {
	
	private final static double MIN_SCROLL = 0.1;
	
	private PropertyResourceBundle UI;
	private ObservableList<HistoryElement> items;
	private SelectionModel<HistoryElement> selectionModel;

	private Button btnAddExp;
	private FlowPane history;
	private RadioButton cbSortOrderOldest;
	private RadioButton cbSortOrderNewest;
	private ToggleGroup grpSortOrder;
	private CheckBox cbAggregate;

	private Map<HistoryElement, HistoryElementBox> boxesByItem;

	private StringConverter<Modification> converter;
	
	//-------------------------------------------------------------------
	/**
	 */
	public DevelopmentScreen(PropertyResourceBundle uiResources) {
		boxesByItem = new HashMap<>();
		items       = FXCollections.observableArrayList();
		selectionModel = new SingleSelectionModel<HistoryElement>() {
			@Override
			protected HistoryElement getModelItem(int index) {
				return items.get(index);
			}
			@Override
			protected int getItemCount() {
				return items.size();
			}
		};
		this.UI = uiResources;
		
		initComponents();
		initLayout();
		initInteractivity();
		setSkin(new ManagedScreenStructuredSkin(this));
	}
	
	//--------------------------------------------------------------------
	public ObservableList<HistoryElement> getItems() { return items; }
	
	//--------------------------------------------------------------------
	public BooleanProperty aggregatedProperty() { return cbAggregate.selectedProperty(); }
	public void setAggregated(boolean value) { cbAggregate.selectedProperty().setValue(value); }
	public boolean shallBeAggregated() { return cbAggregate.selectedProperty().get(); }
	
	//--------------------------------------------------------------------
	public SelectionModel<HistoryElement> getSelectionModel() { return selectionModel; }
	
	//--------------------------------------------------------------------
	private void initComponents() {
		getNavigButtons().add(CloseType.BACK);
		setTitle(UI.getString("label.development"));
		
		history = new FlowPane();
		history.setOrientation(Orientation.VERTICAL);
		history.setVgap(20);
		history.setHgap(20);
		history.prefWrapLengthProperty().bind(history.heightProperty());
//		history.setPrefWrapLength(1000);

		/*
		 * Add Button for XP
		 */
		FontIcon iconAddExp = new FontIcon();
		iconAddExp.addFontSymbol("\uE17E\uE109");
		btnAddExp   = new Button(null, iconAddExp);
		btnAddExp  .setTooltip(new Tooltip(UI.getString("button.addExp.tooltip")));
		btnAddExp  .setStyle("-fx-padding: 8px");
		getStaticButtons().add(btnAddExp);

		grpSortOrder = new ToggleGroup();
		cbSortOrderOldest = new RadioButton(UI.getString("screen.development.sortorder.oldestFirst"));
		cbSortOrderNewest = new RadioButton(UI.getString("screen.development.sortorder.newestFirst"));
		cbSortOrderOldest.setToggleGroup(grpSortOrder);
		cbSortOrderNewest.setToggleGroup(grpSortOrder);
		grpSortOrder.selectToggle(cbSortOrderOldest);
		
		cbAggregate = new CheckBox(UI.getString("screen.development.aggregate.byAdventure"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox content = new HBox(40);
		
		/*
		 * Side bar
		 */
		VBox sideBar = new VBox(5);
		sideBar.getStyleClass().add("section-bar");
		sideBar.setStyle("-fx-min-width: 12em");
		content.getChildren().add(sideBar);
		Label lblSortOrder = new Label(UI.getString("screen.development.sortorder"));
		Label lblAggregate = new Label(UI.getString("screen.development.aggregate"));
		sideBar.getChildren().addAll(lblSortOrder, cbSortOrderOldest, cbSortOrderNewest, lblAggregate, cbAggregate);
		VBox.setMargin(lblAggregate, new Insets(20,0,0,0));
		HBox.setMargin(sideBar, new Insets(0,0,20,0));
		
		
		/*
		 * Elements
		 */
		ScrollPane scroll = new ScrollPane(history);
		scroll.setVbarPolicy(ScrollBarPolicy.NEVER);
		scroll.setPannable(true);
		scroll.setFitToHeight(true);
		
		content.getChildren().add(scroll);
		scroll.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(scroll, Priority.ALWAYS);
		HBox.setMargin(scroll, new Insets(0,0,20,0));
		
		setContent(content);
		// Enable sidescrolling by interprating vertical scroll events horizontal
		scroll.setOnScroll(new EventHandler<ScrollEvent>() {
		    public void handle(ScrollEvent event) {
		        event.consume();
		        
		        double outside = history.getWidth() - scroll.getViewportBounds().getWidth();
		        
		        if (event.getDeltaY() == 0) 
		            return;

		        double toAdd = event.getDeltaY() / outside;
		        if (Math.abs(toAdd)<MIN_SCROLL)
		        	toAdd = (event.getDeltaY()<0)?-MIN_SCROLL:MIN_SCROLL;
	        	scroll.setHvalue(scroll.getHvalue()+toAdd);
		    }
		});
	}

	//-------------------------------------------------------------------
	private HistoryElementBox makeHistoryElementBox(HistoryElement elem) {
		HistoryElementBox box = new HistoryElementBox(elem,UI, converter);
		box.setUserData(elem);
		box.setOnMouseClicked(event -> {
			HistoryElementBox source = (HistoryElementBox)event.getSource();
			selectionModel.select( (HistoryElement)source.getUserData());
			if (event.getClickCount()==2) {
				boolean edited = openEdit(elem);
				if (edited) {
					int index = history.getChildren().indexOf(source);
					history.getChildren().remove(source);
					history.getChildren().add(index, makeHistoryElementBox(elem));
				}
			}
		} );
		boxesByItem.put(elem, box);
		return box;
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnAddExp.setOnMouseClicked(event -> {
			HistoryElement toAdd = openAdd();
			if (toAdd!=null) 
				items.add(toAdd);
			} 
		);
		
		items.addListener(new ListChangeListener<HistoryElement>() {
			public void onChanged(javafx.collections.ListChangeListener.Change<? extends HistoryElement> c) {
				while (c.next()) {
					if (c.wasAdded()) {
						int pos = c.getFrom();
						for (HistoryElement elem : c.getAddedSubList()) {
							HistoryElementBox box = makeHistoryElementBox(elem);
							history.getChildren().add(pos++, box);
						}
					}
					if (c.wasRemoved()) {
						history.getChildren().remove(c.getFrom(), c.getTo());
						for (HistoryElement key : c.getRemoved()) {
							history.getChildren().remove(boxesByItem.get(key));
							boxesByItem.remove(key);
						}
					}
				}
				
			}
		});
		
		selectionModel.selectedItemProperty().addListener( (ov,o,n)  -> {
			HistoryElementBox oldBox = boxesByItem.get(o);
			HistoryElementBox newBox = boxesByItem.get(n);
			if (oldBox!=null) oldBox.getStyleClass().remove("selected");
			if (newBox!=null) newBox.getStyleClass().add("selected");
		});
		
		grpSortOrder.selectedToggleProperty().addListener( (ov,o,n) -> {
			Collections.sort(items, new Comparator<HistoryElement>() {
				public int compare(HistoryElement o1, HistoryElement o2) {
					if (o1.getStart()==null && o2.getStart()!=null) return -1;
					if (o1.getStart()!=null && o2.getStart()==null) return  1;
					return o1.getStart().compareTo(o2.getStart());
				}
			});
			if (n==cbSortOrderNewest) {
				Collections.reverse(items);
			}
			history.getChildren().clear();
			ArrayList<HistoryElement> tmp = new ArrayList<>(items);
			items.clear();
			items.addAll(tmp);
		});
		
		cbAggregate.selectedProperty().addListener( (ov,o,n) -> refresh());
	}

	//-------------------------------------------------------------------
	public void setConverter(StringConverter<Modification> converter) {
		this.converter = converter;
	}
	
	//--------------------------------------------------------------------
	public abstract void refresh();
	
	//--------------------------------------------------------------------
	public abstract HistoryElement openAdd();
	
	//--------------------------------------------------------------------
	/**
	 * @return TRUE, if a changes was made
	 */
	public abstract boolean openEdit(HistoryElement elem);

}

/**
 * 
 */
package org.prelle.rpgframework.gamemaster.jfx;

import org.apache.log4j.Logger;

import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.jfx.Token;
import de.rpgframework.support.combat.jfx.TokenFactory;
import de.rpgframework.support.combat.jfx.TokenUpdater;
import de.rpgframework.support.combat.map.BattleMapListener;
import de.rpgframework.support.combat.map.MatrixBattleMap;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * @author prelle
 *
 */
public class MatrixBattleMapView<C extends Combatant,R> extends StackPane implements BattleMapListener {

	private final static Logger logger = Logger.getLogger("babylon");

	private final static Paint TRANSPARENT = new Color(1, 1, 1, 0);

	private IntegerProperty cellSizeProperty = new SimpleIntegerProperty(80);
	private MatrixBattleMap<C, R> model;
	private TokenFactory<C> tokenFactory;
	private TokenUpdater<C> tokenUpdater;

	private Canvas background;
	private Canvas grid;
	private GridPane tokens;

	private int highX, highY;
	private Region region;

	//-------------------------------------------------------------------
	/**
	 */
	public MatrixBattleMapView() {
		initComponents();
		initLayout();
		initInteractivity();

		setStyle("-fx-background-color: rgba(255,255,255,0.8);");
		setStyle("-fx-background-image: url(images/battlemap_river.jpg);");
		//		setGridLinesVisible(true);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public MatrixBattleMapView(MatrixBattleMap<C, R> data) {
		this();
		setData(data);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		background = new Canvas();
		grid = new Canvas();
		tokens = new GridPane();

		region = new Region();
		region.setPrefSize(cellSizeProperty.get(), cellSizeProperty.get());
		region.setStyle("-fx-background-color: rgba(0,0,0,0.8);");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getChildren().addAll(background, grid, tokens);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		grid.widthProperty().bind(widthProperty());
		grid.heightProperty().bind(heightProperty());
		grid.widthProperty().addListener( (ov,o,n) -> refresh());
		grid.heightProperty().addListener( (ov,o,n) -> refresh());

		this.setOnDragOver   (event -> dragOver   (event));
		this.setOnDragDropped(event -> dragDropped(event));
	}

	//-------------------------------------------------------------------
	public void setTokenFactory(TokenFactory<C> factory) {
		this.tokenFactory = factory;
		refresh();
	}

	//-------------------------------------------------------------------
	public void setTokenUpdater(TokenUpdater<C> data) {
		this.tokenUpdater = data;
		refresh();
	}

	//-------------------------------------------------------------------
	private void refresh() {
		logger.info("Draw "+grid.getWidth()+"x"+grid.getHeight());
		GraphicsContext gc = grid.getGraphicsContext2D();
		gc.setFill(TRANSPARENT);
		gc.fill();

		/*
		 * Draw grid
		 */
		int cellSize = cellSizeProperty.get();
		gc.setStroke(Color.BLACK);
		int numX = (int)grid.getWidth()/cellSize;
		int numY = (int)grid.getHeight()/cellSize;
		logger.debug("Cols/Rows "+numX+"/"+numY);
		for (int x=0; x<=numX; x++) {
			gc.strokeLine(x*cellSize, 0, x*cellSize, grid.getHeight());
		}
		for (int y=0; y<=numY; y++) {
			gc.strokeLine(0, y*cellSize, grid.getWidth(), y*cellSize);
		}


		tokens.getChildren().clear();
		if (model==null)
			return;
		
		for (C comb : model.getPositioned()) {
			int[] pos = model.getPosition(comb);
			if (pos==null)
				continue;

			Token<C> token = tokenFactory.createToken(comb);
//				token2.setOnDragDetected(event -> dragDetected(token2, event));

			if (tokenUpdater!=null) 
				tokenUpdater.updateToken(token, comb);
			else
				logger.error("Missing token updater");

			tokens.add(token, pos[0], pos[1]);
			
		}
	}

	//-------------------------------------------------------------------
	public void setData(MatrixBattleMap<C, R> data) {
		logger.debug("setData "+data);
		if (data==null)
			throw new NullPointerException("BattleMap is null");

		if (this.model!=null) {
			this.model.removeBattleMapListener(this);
			tokens.getColumnConstraints().clear();
			tokens.getRowConstraints().clear();
		}

		this.model = data;
		this.model.addBattleMapListener(this);
		
		ColumnConstraints cols = new ColumnConstraints(cellSizeProperty.get());
		RowConstraints rows = new RowConstraints(cellSizeProperty.get());

		for (int i=0; i<model.getWidth(); i++)
			tokens.getColumnConstraints().add(cols);
		for (int i=0; i<model.getHeight(); i++)
			tokens.getRowConstraints().add(rows);

		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMapListener#battleMapChanged()
	 */
	@Override
	public void battleMapChanged() {
		logger.debug("battleMapChanged");
		refresh();
	}

	//-------------------------------------------------------------------
	private void dragOver(DragEvent event) {
		int offX = (int)event.getX()/cellSizeProperty.get();
		int offY = (int)event.getY()/cellSizeProperty.get();
		//		logger.debug("Drag over "+offX+","+offY);

		if (offX!=highX || offY!=highY) {
			// Clear old highlight
			tokens.getChildren().remove(region);
		} else {
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			return;
		}
		highX = offX;
		highY = offY;

		tokens.add(region, highX, highY);
		if (event.getGestureSource() != event.getSource()) 
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
	}

	//-------------------------------------------------------------------
	private C getCombatantById(String combId) {
		for (C tmp : model.getPositioned()) {
			if (tmp.toString().equals(combId)) 
				return tmp;
		}
			for (C tmp : model.getUnpositioned()) {
				if (tmp.toString().equals(combId)) 
					return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragDropped(DragEvent event) {
		int offX = (int)event.getX()/cellSizeProperty.get();
		int offY = (int)event.getY()/cellSizeProperty.get();
		logger.debug("Dropped over "+offX+","+offY);

		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			C comb = getCombatantById(db.getString());

			logger.debug("Move '"+comb.getName()+"' to "+offX+","+offY);
			model.setPosition(comb, offX, offY);
			success = true;
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();

		tokens.getChildren().remove(region);
	}

}

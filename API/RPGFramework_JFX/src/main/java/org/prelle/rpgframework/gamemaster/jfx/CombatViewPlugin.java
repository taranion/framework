/**
 *
 */
package org.prelle.rpgframework.gamemaster.jfx;

import javafx.scene.layout.Pane;

/**
 * @author Stefan
 *
 */
public interface CombatViewPlugin {

	public Pane getPane();

}

/**
 * 
 */
package org.prelle.rpgframework.gamemaster.jfx;

import java.util.ResourceBundle;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.gamemaster.jfx.SessionScreenPlugin;
import de.rpgframework.session.SessionScreenLevel;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author prelle
 *
 */
public class RegularGridScreenPlugin implements SessionScreenPlugin {

	private static ResourceBundle RES = ResourceBundle.getBundle("i18n/babylon-jfx");

	private CheckBox cbVisible;
	private Slider slGridWidth;
	private Slider slOffsetX;
	private Slider slOffsetY;
	private ColorPicker cbColor;
	
	private VBox configPane;
	private Canvas canvasGM; 
	private Canvas canvasPL; 
	
	//-------------------------------------------------------------------
	public RegularGridScreenPlugin() {
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbVisible = new CheckBox(RES.getString("sessscreenplugin.gridregular.visible"));
		slGridWidth = new Slider(20, 100, 40);
		slOffsetX   = new Slider(0, slGridWidth.getValue(), 0);
		slOffsetY   = new Slider(0, slGridWidth.getValue(), 0);
		cbColor     = new ColorPicker();
		cbColor.setValue(Color.BLACK);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		configPane = new VBox();
		configPane.getChildren().add(cbVisible);
		
		Label hdGridWidth = new Label(RES.getString("sessscreenplugin.gridregular.gridwidth"));		
		configPane.getChildren().addAll(hdGridWidth, slGridWidth);
		
		Label hdOffsetX = new Label(RES.getString("sessscreenplugin.gridregular.offsetx"));		
		configPane.getChildren().addAll(hdOffsetX, slOffsetX);
		
		Label hdOffsetY = new Label(RES.getString("sessscreenplugin.gridregular.offsety"));		
		configPane.getChildren().addAll(hdOffsetY, slOffsetY);
		
		Label hdColor = new Label(RES.getString("sessscreenplugin.gridregular.color"));		
		configPane.getChildren().addAll(hdColor, cbColor);
		
		canvasGM = new Canvas(1920, 1080);
		canvasPL = new Canvas(1920, 1080);
		canvasGM.setMouseTransparent(true);

	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbVisible.selectedProperty().addListener( (ov,o,n) -> BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_CHANGED));
		slGridWidth.valueProperty().addListener( (ov,o,n) -> {
			slOffsetX.setMax((double) n);
			slOffsetY.setMax((double) n);
			BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_REGULAR_GRID_CHANGED, slGridWidth.getValue(), slOffsetX.getValue(), slOffsetY.getValue());
			BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_CHANGED);});
		slOffsetX.valueProperty().addListener( (ov,o,n) -> {
			BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_REGULAR_GRID_CHANGED, slGridWidth.getValue(), slOffsetX.getValue(), slOffsetY.getValue());
			BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_CHANGED);
		});
		slOffsetY.valueProperty().addListener( (ov,o,n) -> { 
			BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_REGULAR_GRID_CHANGED, slGridWidth.getValue(), slOffsetX.getValue(), slOffsetY.getValue());
			BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_CHANGED); });
		cbColor.valueProperty().addListener( (ov,o,n) -> BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_CHANGED));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getName()
	 */
	@Override
	public String getName() {
		return RES.getString("sessscreenplugin.gridregular.name");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getLayer()
	 */
	@Override
	public SessionScreenLevel getLayer() {
		return SessionScreenLevel.GRID;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#setSize(int, int)
	 */
	@Override
	public void setSize(int width, int height) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getSettingsNode()
	 */
	@Override
	public Node getSettingsNode() {
		return configPane;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getContentNode(de.rpgframework.gamemaster.jfx.SessionScreenPlugin.Viewer)
	 */
	@Override
	public Node getContentNode(Viewer viewer) {
		if (!cbVisible.isSelected())
			return null;
		
		Canvas canvas = (viewer==Viewer.GAMEMASTER)?canvasGM:canvasPL;
		
		// Draw for GM
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
//		gc.setGlobalBlendMode(BlendMode.MULTIPLY);
		gc.setStroke(cbColor.getValue());
		for (int x=(int)slOffsetX.getValue(); x<canvas.getWidth(); x+=(int)slGridWidth.getValue()) {
			gc.strokeLine(x, 0, x, canvas.getHeight());
		}
		for (int y=(int)slOffsetY.getValue(); y<canvas.getHeight(); y+=(int)slGridWidth.getValue()) {
			gc.strokeLine(0, y, canvas.getWidth(), y);
		}
		return canvas;
	}

}

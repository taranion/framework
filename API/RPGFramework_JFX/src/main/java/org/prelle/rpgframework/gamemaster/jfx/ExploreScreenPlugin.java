/**
 * 
 */
package org.prelle.rpgframework.gamemaster.jfx;

import java.util.ResourceBundle;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.gamemaster.jfx.SessionScreenPlugin;
import de.rpgframework.session.SessionScreenLevel;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author prelle
 *
 */
public class ExploreScreenPlugin implements SessionScreenPlugin {

	private static ResourceBundle RES = ResourceBundle.getBundle("i18n/babylon-jfx");

	private CheckBox cbVisible;
	private Slider slGridWidth;
	private Button btnReset;
	
	private VBox configPane;
	private Canvas canvasGM, canvasPL; 
	
	private Group grpGM;
	
	//-------------------------------------------------------------------
	public ExploreScreenPlugin() {
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbVisible = new CheckBox(RES.getString("sessscreenplugin.explore.visible"));
		cbVisible.setWrapText(true);
		slGridWidth = new Slider(20, 100, 40);
		btnReset  = new Button(RES.getString("sessscreenplugin.explore.reset"));
		btnReset.getStyleClass().add("border-all");
		
		canvasGM = new Canvas(1920, 1080);
		canvasGM.getGraphicsContext2D().setFill(Color.BLACK);
		canvasGM.getGraphicsContext2D().fillRect(0, 0, canvasGM.getWidth(), canvasGM.getHeight());
		canvasPL = new Canvas(1920, 1080);
		canvasPL.getGraphicsContext2D().setFill(Color.BLACK);
		canvasPL.getGraphicsContext2D().fillRect(0, 0, canvasGM.getWidth(), canvasGM.getHeight());
		
		grpGM = new Group(canvasGM);
		grpGM.setStyle("-fx-opacity: 0.5");
		grpGM.setOnMousePressed(ev -> uncover(ev.getX(), ev.getY()));
		grpGM.setOnMouseDragged(ev -> uncover(ev.getX(), ev.getY()));
	}
	
	//-------------------------------------------------------------------
	private void uncover(double x, double y) {
		double offset = slGridWidth.getValue()/2.0;
		canvasGM.getGraphicsContext2D().clearRect(x-offset, y-offset, slGridWidth.getValue(), slGridWidth.getValue());
		canvasPL.getGraphicsContext2D().clearRect(x-offset, y-offset, slGridWidth.getValue(), slGridWidth.getValue());
		BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_UNCOVERED);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		configPane = new VBox(20);
		configPane.getChildren().addAll(cbVisible, btnReset);
		
		Label hdGridWidth = new Label(RES.getString("sessscreenplugin.explore.brushwidth"));		
		configPane.getChildren().addAll(hdGridWidth, slGridWidth);
	}
	
	//-------------------------------------------------------------------
	private void reset() {
		canvasGM.getGraphicsContext2D().setFill(Color.BLACK);
		canvasGM.getGraphicsContext2D().fillRect(0, 0, canvasGM.getWidth(), canvasGM.getHeight());
		BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_UNCOVERED);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbVisible.selectedProperty().addListener( (ov,o,n) -> BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_CHANGED));
		btnReset.setOnAction(ev -> reset());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getName()
	 */
	@Override
	public String getName() {
		return RES.getString("sessscreenplugin.explore.name");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getLayer()
	 */
	@Override
	public SessionScreenLevel getLayer() {
		return SessionScreenLevel.EXPLORED;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#setSize(int, int)
	 */
	@Override
	public void setSize(int width, int height) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getSettingsNode()
	 */
	@Override
	public Node getSettingsNode() {
		return configPane;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.SessionScreenPlugin#getContentNode(de.rpgframework.gamemaster.jfx.SessionScreenPlugin.Viewer)
	 */
	@Override
	public Node getContentNode(Viewer viewer) {
		if (!cbVisible.isSelected())
			return null;
		
		if (viewer==Viewer.PLAYER)
			return canvasPL;
		return grpGM;
	}

}

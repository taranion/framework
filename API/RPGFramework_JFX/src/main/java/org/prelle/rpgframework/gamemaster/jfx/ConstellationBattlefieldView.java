/**
 * 
 */
package org.prelle.rpgframework.gamemaster.jfx;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.jfx.BattlefieldView;
import de.rpgframework.support.combat.jfx.TokenFactory;
import de.rpgframework.support.combat.jfx.TokenUpdater;
import de.rpgframework.support.combat.map.BattleMap;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * @author prelle
 *
 */
public class ConstellationBattlefieldView<C extends Combatant,R> extends StackPane implements BattlefieldView<C,R> {
	
	private final static Logger logger = Logger.getLogger("rpgframework.combat.jfx");
	
	private final static Color BACKGROUND = Color.WHITE;

	private BattleMap<C,R> battlefield;
	private Canvas canvas;
	
	private TokenFactory<C> tokenFactory;
	private Map<Combatant, Node> tokens;
	
	//-------------------------------------------------------------------
	/**
	 */
	public ConstellationBattlefieldView() {
		tokens = new HashMap<Combatant, Node>();
		
		initComponents();
		initInteractivity();
		
		this.setMaxHeight(Double.MAX_VALUE);
		this.setMaxWidth(Double.MAX_VALUE);
//		this.setMinWidth(500);
//		this.setMinHeight(500);
		
		redraw();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		canvas = new Canvas(500, 500);
		getChildren().add(canvas);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		widthProperty().addListener( (ov,o,n) -> sizeChanged());
		heightProperty().addListener( (ov,o,n) -> sizeChanged());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.jfx.BattlefieldView#setData(de.rpgframework.support.combat.map.BattleMap)
	 */
	@Override
	public void setData(BattleMap<C,R> model) {
		this.battlefield = model;
		
		if (tokenFactory==null) {
			logger.error("Missing CombatantTokenFactory");
			return;
		}
		/*
		 * Create tokens
		 */
		for (C comb : model.getPositioned()) {
			tokens.put(comb, tokenFactory.createToken(comb));
		}
	}
	
	//-------------------------------------------------------------------
	private void sizeChanged() {
		logger.debug("Size changed to "+this.getWidth()+"x"+getHeight());
		canvas.setWidth(getWidth());
		canvas.setHeight(getHeight());
		redraw();
	}

	//-----------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.dsatool.dialogs.DrawLogicListener#drawCombatant(org.prelle.dsatool.combat.Combatant, int, int, int)
	 */
	public void drawCombatant(GraphicsContext gc, Combatant dc, double x, double y) {
		double w=40;
		double h=25;
		gc.setFill(Color.RED);
		gc.fillArc(x-w, y-h, w*2, h*2, 0, 360, ArcType.OPEN);
		gc.setLineWidth(1);
		gc.setStroke(Color.BLACK);
		gc.strokeText(dc.getName(), x-w*0.8, y, w*1.7);
		gc.drawImage(new Image(new ByteArrayInputStream(dc.getAvatar())), x, y, w, h);
	}
	
	//-------------------------------------------------------------------
	private void redraw() {
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.setFill(BACKGROUND);
		gc.fillRect(0,0,getWidth(),getHeight());
		
		double centerX = canvas.getWidth()/2;
		double centerY = canvas.getHeight()/2;
		double radiusX = centerX*0.8;
		double radiusY = centerY*0.8;
		double innerRadiusX = centerX*0.2;
		double innerRadiusY = centerY*0.2;

		/*
		 * Draw player circle
		 */
		gc.setStroke(Color.rgb(20, 20, 20));
		gc.setLineWidth(3);
		gc.strokeArc((centerX-radiusX), (centerY-radiusY), radiusX*2, radiusY*2, 0, 360, ArcType.OPEN);
		
		if (battlefield==null)
			return;
		
		/*
		 * Collect some data
		 */
		List<C> unpositioned = battlefield.getUnpositioned();
		List<C> posOpponents = new ArrayList<>();
		List<C> unposOpponents = new ArrayList<>();
		for (C comb : battlefield.getCombat().getGroups().get(1)) {
			if (!unpositioned.contains(comb))
				posOpponents.add(comb);
			else
				unposOpponents.add(comb);
		}
		
		/*
		 * Heroes
		 */
		double anglePerHero = 360.0/battlefield.getCombat().getGroups().get(0).size();
		int pos=0;
		for (Combatant comb : battlefield.getCombat().getGroups().get(0)) {
			double angle = anglePerHero*pos;
			double angle2= (Math.PI*2) / (360/angle);
			double x = Math.cos(angle2)*radiusX  + centerX;
			double y = Math.sin(angle2)*radiusY  + centerY;
			logger.debug("Draw "+pos+" at "+x+"x"+y);
			gc.setFill(Color.BLACK);
			gc.fillOval(x, y, 10, 10);
			drawCombatant(gc, comb, x, y);
			
			pos++;
		}
		
		/*
		 * Unpositioned opponents
		 */
		double anglePerOpp = 360.0/unposOpponents.size();
		pos=0;
		for (Combatant comb : unposOpponents) {
			double angle = anglePerOpp*pos;
			double angle2= (Math.PI*2) / (360/angle);
			double x = Math.cos(angle2)*innerRadiusX  + centerX;
			double y = Math.sin(angle2)*innerRadiusY  + centerY;
			logger.debug("Draw "+pos+" at "+x+"x"+y);
			gc.setFill(Color.BLACK);
			gc.fillOval(x, y, 10, 10);
			drawCombatant(gc, comb, x, y);
			
			pos++;
		}
	}

	@Override
	public void setTokenFactory(TokenFactory<C> factory) {
		tokenFactory = factory;
	}

	@Override
	public void setTokenUpdater(TokenUpdater<C> updater) {
		// TODO Auto-generated method stub
		
	}

}

/**
 * 
 */
package org.prelle.rpgframework.jfx;

import org.prelle.javafx.ManagedScreen;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Skin;
import javafx.scene.control.Skinnable;

/**
 * @author Stefan
 *
 */
public class CharacterViewScreen extends ManagedScreen implements Skinnable {

	private static final String DEFAULT_STYLE_CLASS = "character-view-screen";
	
//	private ObjectProperty<Image> portrait;
//	private ObjectProperty<Parent> basicDataNode;
	private ObservableList<CardTile> items;

	//--------------------------------------------------------------------
	/**
	 */
	public CharacterViewScreen() {
//		portrait = new SimpleObjectProperty<>();
//		basicDataNode = new SimpleObjectProperty<>();
		items = FXCollections.observableArrayList();
		
        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
	}

//	//--------------------------------------------------------------------
//	public ObjectProperty<Image> portraitProperty() { return portrait; }
//	public Image getPortrait() { return portrait.getValue(); }
//	public void setPortrait(Image value) { this.portrait.setValue(value); }
//	//--------------------------------------------------------------------
//	public ObjectProperty<Parent> basicDataNodeProperty() { return basicDataNode; }
//	public Parent getBasicDatNode() { return basicDataNode.getValue(); }
//	public void setBasicDataNode(Parent value) { this.basicDataNode.setValue(value); }
	//--------------------------------------------------------------------
	public ObservableList<CardTile> getItems() { return items; }

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	@Override
	public Skin<?> createDefaultSkin() {
		return new CharacterViewScreenSkin(this);
	}

}

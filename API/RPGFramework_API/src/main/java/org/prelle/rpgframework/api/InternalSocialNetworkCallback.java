/**
 * 
 */
package org.prelle.rpgframework.api;

import de.rpgframework.social.Friend;

/**
 * @author prelle
 *
 */
public interface InternalSocialNetworkCallback {
	
	//-------------------------------------------------------------------
	public void addedFriend(Friend friend);
	
	//-------------------------------------------------------------------
	public void updatedFriend(Friend friend);
	
}

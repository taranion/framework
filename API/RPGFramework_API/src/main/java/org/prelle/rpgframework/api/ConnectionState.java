/**
 * 
 */
package org.prelle.rpgframework.api;

/**
 * @author prelle
 *
 */
public enum ConnectionState {
	
	DISCONNECTED,
	/*
	 * The connection was successful. Now reading contact states is in progress 
	 */
	INITIALIZING,
	/*
	 * The network setup was finished. The internal network is ready to go
	 */
	READY,
	
}

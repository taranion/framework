/**
 * 
 */
package org.prelle.online.contacts;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ServiceLoader;

/**
 * @author prelle
 *
 */
public class AddressBookLoader {
	
	private static AddressBook instance = null;

	//-----------------------------------------------------------------
	public static AddressBook getInstance() {
		if (instance!=null)
			return instance;
		
		Iterator<AddressBook> it = ServiceLoader.load(AddressBook.class).iterator();
		while (it.hasNext()) {
			instance = it.next();
			System.err.println("AB = "+instance);
			return instance; 
		}
		throw new NoSuchElementException("No instance of AddressBook found");
	}

}

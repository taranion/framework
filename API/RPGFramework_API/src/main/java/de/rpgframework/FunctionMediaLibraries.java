/**
 * 
 */
package de.rpgframework;

import de.rpgframework.media.MediaService;

/**
 * @author prelle
 *
 */
public interface FunctionMediaLibraries {

	public MediaService getMediaService();

}

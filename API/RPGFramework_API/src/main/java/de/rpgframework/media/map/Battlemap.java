/**
 * 
 */
package de.rpgframework.media.map;

import java.util.List;

import de.rpgframework.media.ImageMedia;

/**
 * @author prelle
 *
 */
public interface Battlemap extends ImageMedia {
	
	public List<String> getLayer();

}

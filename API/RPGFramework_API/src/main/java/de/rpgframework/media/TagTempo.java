/**
 * 
 */
package de.rpgframework.media;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public enum TagTempo implements MediaTag {

	SPHERIC,
	SLOW,
	NEUTRAL,
	DYNAMIC,
	DRAMATIC,
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaTag#getName()
	 */
	@Override
	public String getName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.tempo."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.tempo."+this.name().toLowerCase();
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.tempo");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.tempo";
	}
	
}

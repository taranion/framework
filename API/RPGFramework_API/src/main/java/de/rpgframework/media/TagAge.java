/**
 * 
 */
package de.rpgframework.media;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public enum TagAge implements MediaTag {
	
	CHILD,
	YOUNG,
	MEDIUM,
	OLD,
	VERY_OLD
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaTag#getName()
	 */
	@Override
	public String getName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.age."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.age."+this.name().toLowerCase();
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.age");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.age";
	}

	
}

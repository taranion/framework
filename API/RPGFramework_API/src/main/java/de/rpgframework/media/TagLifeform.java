/**
 * 
 */
package de.rpgframework.media;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public enum TagLifeform implements MediaTag {

	NORMAL_BORN,
	ANIMAL,
	FAMILIAR,
	ARTIFICAL,
	CREATURE,
	OTHER,
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaTag#getName()
	 */
	@Override
	public String getName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.lifeform."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.lifeform."+this.name().toLowerCase();
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.lifeform");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.lifeform";
	}
	
}

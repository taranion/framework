/**
 * 
 */
package de.rpgframework.media;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public enum TagPlaces implements MediaTag {
	
	CASTLE,
	TOWER,
	MARKET,
	SHOP,
	BAR_TAVERN,
	HOTEL_INN,
	CLINIC_SPITAL,
	
	CEMETARY,
	CRYPT,
	CAVE,     // Natural
	MINE,     // Natural, expedient modified
	DUNGEON,  // Built, has artificial walls etc.
	
	SUBWAY,
	TRAIN,
	PLANE,
	HANGAR,
	
	BOAT,
	SHIP,
	HARBOR,
	
	INDUSTRIAL,
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaTag#getName()
	 */
	@Override
	public String getName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.places."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.places."+this.name().toLowerCase();
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("tag.places");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+ResourceBundle.getBundle("i18n/rpgframework").getBaseBundleName());
		}
		return "tag.places";
	}
	
}

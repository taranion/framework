package de.rpgframework.devices;

import java.util.ResourceBundle;

public enum DeviceFunction { 
	
	MEDIASERVER,
	PLAYER_HANDOUT, 
	PLAYER_MESSAGE, 
	SESSION_HANDOUT, 
	SESSION_AUDIO, 
	;
	
	//--------------------------------------------------------------------
	public String getName() {
		return ResourceBundle.getBundle("i18n/rpgframework").getString("rpgtooldevice."+this.name().toLowerCase());
	}
	
}
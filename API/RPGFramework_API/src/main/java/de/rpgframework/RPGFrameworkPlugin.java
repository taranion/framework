/**
 * 
 */
package de.rpgframework;

/**
 * @author prelle
 *
 */
public interface RPGFrameworkPlugin {

	public void initialize(RPGFramework framework);
	
}

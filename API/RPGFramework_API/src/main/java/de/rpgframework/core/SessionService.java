/**
 * 
 */
package de.rpgframework.core;

import java.io.IOException;
import java.util.List;

import de.rpgframework.products.Adventure;

/**
 * @author prelle
 *
 */
public interface SessionService {
	
	//--------------------------------------------------------------------
	public Group createGroup(String name) throws IOException;
	
	//--------------------------------------------------------------------
	public List<Group> getGroups() throws IOException;
	
	//--------------------------------------------------------------------
	public void addPlayer(Group group, Player player) throws IOException;
	
	//--------------------------------------------------------------------
	public void removePlayer(Group group, Player player) throws IOException;
	
	//--------------------------------------------------------------------
	public void saveGroup(Group group) throws IOException;

	
	
	//--------------------------------------------------------------------
	/**
	 * Create a new context of an adventure played by a group
	 */
	public SessionContext createSession(Group group, Adventure adv) throws IOException;
	
	//--------------------------------------------------------------------
	/**
	 * Get all sessions for that group
	 */
	public List<SessionContext> getSessions(Group group) throws IOException;
	
	//--------------------------------------------------------------------
	/**
	 * Initiate or continue the session
	 */
	public void runSession(SessionContext session);

}

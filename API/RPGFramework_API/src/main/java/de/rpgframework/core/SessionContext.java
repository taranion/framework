/**
 * 
 */
package de.rpgframework.core;

import java.time.Instant;

import de.rpgframework.character.CharacterHandleFull;
import de.rpgframework.products.Adventure;

/**
 * @author prelle
 *
 */
public interface SessionContext extends Comparable<SessionContext> {

	//--------------------------------------------------------------------
	public Adventure getAdventure();
	
	//--------------------------------------------------------------------
	public Group getGroup();
	
	//--------------------------------------------------------------------
	/**
	 * Set the character played by the given player.
	 * 
	 * @param charac Character played - may be null
	 */
	public void setCharacter(Player player, CharacterHandleFull charac);
	
	//--------------------------------------------------------------------
	/**
	 * Obtain the character played by the given player
	 */
	public CharacterHandleFull getCharacter(Player player);

	//-------------------------------------------------------------------
	public Instant getLastTime();

	//-------------------------------------------------------------------
	public void setLastTime(Instant lastTime);

}

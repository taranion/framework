/**
 * 
 */
package de.rpgframework.core;

/**
 * @author prelle
 *
 */
public enum Genre {

	APOCALYPTIC,
	FANTASY,
	HORROR,
	SCIFI,
	CYBERPUNK,
	TODAY,
	VINTAGE_1920,
	VINTAGE_1930,
	VINTAGE_1980,
	WESTERN,
	STEAMPUNK,
	SUPERHEROES,
}

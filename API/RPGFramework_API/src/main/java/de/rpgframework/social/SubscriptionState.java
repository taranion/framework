/**
 * 
 */
package de.rpgframework.social;

/**
 * @author prelle
 *
 */
public enum SubscriptionState {

	/**
	 * No subscription request has been sent. This is the default state
	 * for all responses to search request.
	 */
	NOT_SUBSCRIBED,
	
	/**
	 * A subscription request has been sent, but not confirmed yet
	 */
	REQUEST_SENT,
	
	/**
	 * A subscription request has been received, but not confirmed yet
	 */
	REQUEST_RECEIVED,
	
	/**
	 * A subscription request has been sent or received and confirmed
	 */
	SUBSCRIBED,
}

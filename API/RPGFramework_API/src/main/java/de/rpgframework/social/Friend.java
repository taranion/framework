/**
 * 
 */
package de.rpgframework.social;

import java.net.URI;

import de.rpgframework.addressbook.Identity;

/**
 * @author prelle
 *
 */
public interface Friend extends Identity {
	
	//-------------------------------------------------------------------
	public OnlineService getOnlineService();
	
	//-------------------------------------------------------------------
	/**
	 * @return A unique identifier valid within the online service
	 */
	public String getUser();
	
	//-------------------------------------------------------------------
	/**
	 * @return A unique identifier 
	 */
	public URI getIdentifier();

}

/**
 * 
 */
package de.rpgframework.social;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import de.rpgframework.addressbook.Contact;
import de.rpgframework.core.Player;

/**
 * The interface to a low level social network provider (e.g. Cospace, XMPP, Facebook ...)
 * 
 * @author prelle
 *
 */
public interface SocialNetworkProvider {

	//--------------------------------------------------------------------
	/**
	 * Get all available online services.
	 */
	public Collection<OnlineService> getOnlineServices();

	//--------------------------------------------------------------------
	/**
	 * Set which online service to use for what feature.
	 */
	public void setUsedService(OnlineService.Feature feature, OnlineService service);

	//--------------------------------------------------------------------
	public List<Friend> getFriends();

	//--------------------------------------------------------------------
	/**
	 * Search the network for all users matching the search term.
	 * 
	 * @param key Search term
	 * @return
	 */
	public List<Contact> search(String key);

	//--------------------------------------------------------------------
	public boolean requestSubscription(Contact contactToAdd, Map<String, String> properties);

	//--------------------------------------------------------------------
	public boolean confirmSubscription(Contact contactToAdd);

	//--------------------------------------------------------------------
	public Player getBoundPlayer(Friend contact);

	//--------------------------------------------------------------------
	public List<Friend> getBoundFriends(Player player);

	//--------------------------------------------------------------------
	/**
	 * Bind the contact to the player.
	 */
	public void bindPlayer(Friend Contact, Player player);

	//--------------------------------------------------------------------
	/**
	 * Remove the bonding of player and contact.
	 */
	public void unbindPlayer(Friend Contact, Player player);
	
}

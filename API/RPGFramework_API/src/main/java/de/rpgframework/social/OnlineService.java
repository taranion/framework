/**
 * 
 */
package de.rpgframework.social;

import java.util.Collection;
import java.util.ResourceBundle;

import de.rpgframework.ConfigContainer;
import de.rpgframework.character.CharacterProvider;

/**
 * @author prelle
 *
 */
public interface OnlineService {
	
	public enum Feature {
		CHARACTER_STORAGE,
		APPLICATION_RPC
		;
		
		//--------------------------------------------------------------------
		public String getName() {
			return ResourceBundle.getBundle("i18n/rpgframework").getString("feature."+this.name().toLowerCase());
		}
	}

	//--------------------------------------------------------------------
	public String getName();

	//--------------------------------------------------------------------
	public String getURLScheme();

	//--------------------------------------------------------------------
	public Collection<Feature> getFeatures();

	//--------------------------------------------------------------------
	public ConfigContainer getConfigurationOptions();

	//--------------------------------------------------------------------
	/**
	 * A logo of 64x64 pixel
	 */
	public byte[] getLogo();

	//--------------------------------------------------------------------
	/**
	 * Try to connect to the network - if not already done yet.
	 */
	public boolean checkConnect();

	//--------------------------------------------------------------------
	/**
	 * If Feature CHARACTER_STORAGE is supported, this method returns the
	 * access class for it
	 */
	public CharacterProvider getCharacterStorage();

}

/**
 *
 */
package de.rpgframework.print;

/**
 * @author Stefan
 *
 */
public interface PrintCell {

	public int getRequiredColumns();

}

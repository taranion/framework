/**
 * 
 */
package de.rpgframework.print;

import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public enum PrintType {

	DIRECT,
	PDF,
	IMAGE,
	HTML,
	BBCODE
	;
	
	//--------------------------------------------------------------------
	public String getName() {
		return ResourceBundle.getBundle("i18n/rpgframework").getString("printtype."+this.name().toLowerCase());
	}
	
}

package de.rpgframework.genericrpg;

import java.util.Date;
import java.util.List;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;

public interface HistoryElement {

	//-------------------------------------------------------------------
	public Date getStart();

	//-------------------------------------------------------------------
	public Date getEnd();

	//-------------------------------------------------------------------
	/**
	 * @return the adventure
	 */
	public Adventure getAdventure();

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName();

	//-------------------------------------------------------------------
	/**
	 * @return the gained
	 */
	public List<Reward> getGained();

	//-------------------------------------------------------------------
	/**
	 * @return the spent
	 */
	public List<Modification> getSpent();

}
/**
 * 
 */
package de.rpgframework.genericrpg;

import java.util.List;

import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public interface SelectionController<T extends Modifyable> {

	public List<T> getAvailable();

	public List<ModifyableValue<T>> getSelected();
	
	public double getSelectionCost(T data);
	
	public double getDeselectionCost(ModifyableValue<T> value);

	public boolean canBeSelected(T data);

	public boolean canBeDeselected(ModifyableValue<T> value);

	public ValueModification<T> createModification(ModifyableValue<T> value);

	public ModifyableValue<T> select(T data);

	public void deselect(ModifyableValue<T> value);

}

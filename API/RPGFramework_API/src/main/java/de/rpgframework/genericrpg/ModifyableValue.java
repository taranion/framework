/**
 * 
 */
package de.rpgframework.genericrpg;

import de.rpgframework.genericrpg.modification.Modifyable;


/**
 * @author Stefan
 *
 */
public interface ModifyableValue<T> extends Modifyable {

	//--------------------------------------------------------------------
	public T getModifyable();

	//--------------------------------------------------------------------
	/**
	 * Returns the points invested into this value
	 * @return Invested points
	 */
	public int getPoints();

	//--------------------------------------------------------------------
	/**
	 * Set the points invested into this value;
	 */
	public void setPoints(int points);

	//--------------------------------------------------------------------
	public int getModifier();

	//--------------------------------------------------------------------
	/**
	 * Return the value after all modifications have been
	 * applied
	 */
	public int getModifiedValue();

}

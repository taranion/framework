/**
 *
 */
package de.rpgframework.adventure.jfx;

import de.rpgframework.adventure.AdventureDocumentElement;
import javafx.scene.Node;

/**
 * @author Stefan
 *
 */
public interface ExtensionRenderer {

	//--------------------------------------------------------------------
	/**
	 * Get the FPI of the namespace this renderer takes care of
	 * @return formal public identifier
	 */
	public String getNamespace();

	//--------------------------------------------------------------------
	public Node renderReadOnly(AdventureDocumentElement element);

	//--------------------------------------------------------------------
	public Node renderEditable(AdventureDocumentElement element);

}

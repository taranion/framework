/**
 *
 */
package de.rpgframework.support.combat.jfx;

import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.map.BattleMap;

/**
 * @author Stefan
 *
 */
public interface BattlefieldView<C extends Combatant,R> {
	
	public void setTokenFactory(TokenFactory<C> factory);
	
	public void setTokenUpdater(TokenUpdater<C> updater);

	public void setData(BattleMap<C,R> model);

}

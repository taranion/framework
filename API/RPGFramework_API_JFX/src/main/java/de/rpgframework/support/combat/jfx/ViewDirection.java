/**
 * 
 */
package de.rpgframework.support.combat.jfx;

/**
 * @author prelle
 *
 */
public enum ViewDirection {

	EAST,
	SOUTHEAST,
	SOUTH,
	SOUTHWEST,
	WEST,
	NORTHWEST,
	NORTH,
	NORTHEAST,
	NONE
	
}

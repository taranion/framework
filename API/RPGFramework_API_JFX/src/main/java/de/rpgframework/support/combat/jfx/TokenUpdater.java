/**
 * 
 */
package de.rpgframework.support.combat.jfx;

import de.rpgframework.support.combat.Combatant;

/**
 * @author prelle
 *
 */
public interface TokenUpdater<C extends Combatant> {

	public void updateToken(Token<C> token, C combatant);
	
}

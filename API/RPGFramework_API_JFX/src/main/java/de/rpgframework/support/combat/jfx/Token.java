/**
 * 
 */
package de.rpgframework.support.combat.jfx;

import de.rpgframework.support.combat.Combatant;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
public class Token<C extends Combatant> extends Control {

	private final static int DEFAULT_MAX = 10;
	
	private C combatant;
	private Image image;
	private String text;
	private IntegerProperty rightValue    = new SimpleIntegerProperty(DEFAULT_MAX);
	private IntegerProperty rightValueMax = new SimpleIntegerProperty(DEFAULT_MAX);
	private IntegerProperty leftValue     = new SimpleIntegerProperty(DEFAULT_MAX);
	private IntegerProperty leftValueMax  = new SimpleIntegerProperty(DEFAULT_MAX);
	private IntegerProperty imageRadius   = new SimpleIntegerProperty(30);
	private ObjectProperty<ViewDirection> direction = new SimpleObjectProperty<ViewDirection>(ViewDirection.NONE); 
	
	//-------------------------------------------------------------------
	/**
	 */
	public Token(C combatant, Image image, String text) {
		this.combatant = combatant;
		this.image = image;
		this.text  = text;
		setSkin(new TokenSkin<C>(this));
	}
	
	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}
	
	//-------------------------------------------------------------------
	public Image getImage() { return image; }
	
	//-------------------------------------------------------------------
	public IntegerProperty imageRadiusProperty() { return imageRadius; }
	public int getImageRadius() { return imageRadius.get(); }
	public void setImageRadius(int value) { imageRadius.set(value); }
	
	//-------------------------------------------------------------------
	public IntegerProperty rightValueProperty() { return rightValue; }
	public int getRightValue() { return rightValue.get(); }
	public void setRightValue(int value) {
		if (value>rightValueMax.get())
			throw new IllegalArgumentException(value+" > "+rightValueMax);
		rightValue.set(value); 
	}
	
	//-------------------------------------------------------------------
	public IntegerProperty rightValueMaxProperty() { return rightValueMax; }
	public int getRightValueMax() { return rightValueMax.get(); }
	public void setRightValueMax(int value) { 
		rightValueMax.set(value); 
		if (rightValue.get()>value)
			rightValue.set(value);
	}
	
	//-------------------------------------------------------------------
	public IntegerProperty leftValueProperty() { return leftValue; }
	public int getLeftValue() { return leftValue.get(); }
	public void setLeftValue(int value) { 
		if (value>leftValueMax.get())
			throw new IllegalArgumentException(value+" > "+leftValueMax);
		leftValue.set(value); 
	}
	
	//-------------------------------------------------------------------
	public IntegerProperty leftValueMaxProperty() { return leftValueMax; }
	public int getLeftValueMax() { return leftValueMax.get(); }
	public void setLeftValueMax(int value) { 
		leftValueMax.set(value); 
		if (leftValue.get()>value)
			leftValue.set(value);
	}
	
	//-------------------------------------------------------------------
	public ObjectProperty<ViewDirection> directionProperty() { return direction; }
	public ViewDirection getDirection() { return direction.get(); }
	public void setDirection(ViewDirection value) { direction.set(value); }

	//-------------------------------------------------------------------
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the combatant
	 */
	public C getCombatant() {
		return combatant;
	}

}
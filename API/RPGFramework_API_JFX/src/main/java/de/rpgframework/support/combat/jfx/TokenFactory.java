/**
 * 
 */
package de.rpgframework.support.combat.jfx;

import de.rpgframework.support.combat.Combatant;

/**
 * @author prelle
 *
 */
public interface TokenFactory<C extends Combatant> {

	public Token<C> createToken(C combatant);
	
}

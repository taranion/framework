/**
 * 
 */
package de.rpgframework.gamemaster.jfx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author prelle
 *
 */
public class SessionScreenPluginRegistry {
	
	private static List<SessionScreenPlugin> plugins;

	//-------------------------------------------------------------------
	static {
		plugins = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public static void add(SessionScreenPlugin plugin) {
		if (!plugins.contains(plugin)) {
			plugins.add(plugin);
			Collections.sort(plugins, new Comparator<SessionScreenPlugin>() {
				public int compare(SessionScreenPlugin o1, SessionScreenPlugin o2) {
					return o1.getLayer().compareTo(o2.getLayer());
				}
			});
			BabylonEventBus.fireEvent(BabylonEventType.SESSION_SCREEN_LAYER_CHANGED, plugin);
		}
	}

	//-------------------------------------------------------------------
	public static List<SessionScreenPlugin> getPlugins() {
		return new ArrayList<>(plugins);
	}

}

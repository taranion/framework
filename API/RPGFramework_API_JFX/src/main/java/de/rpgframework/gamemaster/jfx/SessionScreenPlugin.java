package de.rpgframework.gamemaster.jfx;

import de.rpgframework.session.SessionScreenLevel;
import javafx.scene.Node;

public interface SessionScreenPlugin {
	
	public enum Viewer {
		GAMEMASTER,
		PLAYER
	}

	//-------------------------------------------------------------------
	public String getName();

	//-------------------------------------------------------------------
	public SessionScreenLevel getLayer();

	//-------------------------------------------------------------------
	public void setSize(int width, int height);

	//-------------------------------------------------------------------
	public Node getSettingsNode();

	//-------------------------------------------------------------------
	public Node getContentNode(Viewer viewer);

}

/**
 *
 */
package de.rpgframework.gamemaster.jfx;

import java.util.Map;

import de.rpgframework.core.RoleplayingSystem;
import javafx.scene.layout.Region;

/**
 * @author prelle
 *
 */
public interface OldSessionScreenGUIPlugin {

	//-------------------------------------------------------------------
	/**
	 * Initialize plugin with application specific context. This method
	 * is possibly not called by the application.
	 * 
	 * @param specific Application specific parameters - e.g. a parent
	 *   windows, JavaFXExtension ScreenManager and other
	 */
	public void init(Map<String,Object> specific);

	//-------------------------------------------------------------------
	public String getName();

	//-------------------------------------------------------------------
	public PageType getType();

	//-------------------------------------------------------------------
	public RoleplayingSystem getRoleplayingSystem();

	//-------------------------------------------------------------------
	public Region getPane();

}

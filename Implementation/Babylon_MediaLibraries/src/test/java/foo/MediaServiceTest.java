/**
 * 
 */
package foo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.CountDownLatch;
import java.util.prefs.Preferences;

import javax.swing.SwingUtilities;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.media.MediaServiceImpl;

import de.rpgframework.ConfigOption;
import de.rpgframework.ConfigOption.Type;
import de.rpgframework.RPGFramework;
import de.rpgframework.media.MediaLibrary;
import de.rpgframework.media.MediaService;
import javafx.embed.swing.JFXPanel;

/**
 * @author prelle
 *
 */
public class MediaServiceTest {
	
	private final static Logger logger = Logger.getLogger("test");
	
	private static Path dir;
	private static MediaService service;

	//-------------------------------------------------------------------
	protected static void setupJavaFX() throws RuntimeException {
		final CountDownLatch latch = new CountDownLatch(1);
		try {
			SwingUtilities.invokeAndWait(() -> {
				new JFXPanel(); // initializes JavaFX environment
				latch.countDown();
			});
		} catch (InvocationTargetException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		BasicConfigurator.configure();
		Enumeration<Appender> app = Logger.getRootLogger().getAllAppenders();	
		while (app.hasMoreElements()) {
			Appender a = app.nextElement();
			a.setLayout(new PatternLayout("%5p [%c] (%F:%L) - %m%n"));
		}
		Logger.getLogger("xml").setLevel(Level.WARN);
		Logger.getLogger("hsqldb").setLevel(Level.WARN);
		dir = Files.createTempDirectory("babylon-medialib-test");
		Files.createDirectory(dir.resolve("media"));
		logger.debug("Created "+dir);
		
		setupJavaFX();
	}

	//-------------------------------------------------------------------
	private static void deleteDirContent(Path value) throws Exception {
		for (Path child : Files.newDirectoryStream(value)) {
			if (Files.isDirectory(child))
				deleteDirContent(child);
			Files.delete(child);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (Files.isDirectory(dir))
			deleteDirContent(dir);
		Files.deleteIfExists(dir);
		logger.debug("Deleted "+dir);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ConfigContainerImpl configRoot = new ConfigContainerImpl(Preferences.userRoot().node("test"), "test");
		ConfigOption<String> dataDir = configRoot.createOption(RPGFramework.PROP_DATADIR, Type.DIRECTORY, null);
		dataDir.set(dir.toAbsolutePath().toString());
		service = new MediaServiceImpl(configRoot);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	//-------------------------------------------------------------------
	/**
	 * Test method for {@link org.prelle.rpgframework.media.MediaServiceImpl#addMyLibrary(java.net.URL, java.lang.String)}.
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws URISyntaxException 
	 */
	@Test
	public void testAddMyLibraryURLString() throws Exception {
		File lib = dir.resolve("dummy").toFile();
		URL url = lib.toURI().toURL();
		MediaLibrary media = service.addMyLibrary(url, "UnitTest");
		assertNotNull(media);
		assertFalse( service.getMyLibraries().isEmpty() );
		assertTrue( service.getMyLibraries().contains(media));
		
		Path xmlFile = dir.resolve("media").resolve("UnitTest.xml");
		assertTrue(Files.exists(xmlFile));
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Files.copy(xmlFile, os);
		String output = new String(os.toByteArray(),"UTF-8");
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<library>\n   <name>UnitTest</name>\n   <url>"+url.toString()+"</url>\n</library>\n";
		assertEquals(expected, output);
	}

	//-------------------------------------------------------------------
	/**
	 * Test method for {@link org.prelle.rpgframework.media.MediaServiceImpl#addMyLibrary(java.net.URL, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAddMyLibraryURLStringStringString() throws Exception {
		String login = "Hallo";
		String pass  = "Welt";
		URL url = new URL("http://www.dummydomain.com/");
		MediaLibrary media = service.addMyLibrary(url, "UnitTest2", login, pass);
		assertNotNull(media);
		assertFalse( service.getMyLibraries().isEmpty() );
		assertTrue( service.getMyLibraries().contains(media));
		
		Path xmlFile = dir.resolve("media").resolve("UnitTest2.xml");
		assertTrue(Files.exists(xmlFile));
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Files.copy(xmlFile, os);
		String output = new String(os.toByteArray(),"UTF-8");
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<library>\n   <name>UnitTest2</name>\n   <url>"+url.toString()+"</url>\n   <login>Hallo</login>\n   <password>Welt</password>\n</library>\n";
		assertEquals(expected, output);
	}

	//-------------------------------------------------------------------
	/**
	 * Test method for {@link org.prelle.rpgframework.media.MediaServiceImpl#removeMyLibrary(de.rpgframework.media.MediaLibrary)}.
	 * @throws Exception 
	 */
	@Test
	public void testRemoveMyLibrary() throws Exception {
		testAddMyLibraryURLString();
		testAddMyLibraryURLStringStringString();
		assertEquals( 2,  service.getMyLibraries().size() );
		MediaLibrary localLib = service.getMyLibraries().get(0);
		MediaLibrary httpLib = service.getMyLibraries().get(1);
		
		service.removeMyLibrary(httpLib);
		assertEquals("Library was not removed", 1,  service.getMyLibraries().size() );
		assertTrue( service.getMyLibraries().contains(localLib));

		// Has the definition file be deleted
		Path xmlFile = dir.resolve("media").resolve("UnitTest2.xml");
		assertFalse("File not deleted",Files.exists(xmlFile));
	}

}

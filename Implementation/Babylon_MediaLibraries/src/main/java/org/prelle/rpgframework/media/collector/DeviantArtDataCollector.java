/**
 * 
 */
package org.prelle.rpgframework.media.collector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.media.LicenseType;
import de.rpgframework.media.Media;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author prelle
 *
 */
public class DeviantArtDataCollector implements DataCollector {

	private Logger logger = Logger.getLogger("babylon.media.deviantart");

	final static Pattern PATTERN = Pattern.compile("http[s]*://.*.deviantart.com/art/(.*)");
	final static Pattern DOWNLOAD_PATTERN = Pattern.compile(".*\"(http[s]*://.*.deviantart.com/download/.*?token=.*;ts=[^\"]*)\".*");

	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private RoboBrowser browser;
	
	public DeviantArtDataCollector() {
		browser = new RoboBrowser();
		if (Platform.isFxApplicationThread()) {
			logger.debug("2 run now");
			browser.prepare();
		} else {
			logger.debug("2 Calling Platform.runLater");
			Platform.runLater(new Runnable() {
				public void run() {
					browser.prepare();
				}});
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		return PATTERN.matcher(url.toString()).matches();
	}

	protected void setupJavaFX() throws RuntimeException {
		try {
			Platform.setImplicitExit(false);
			final CountDownLatch latch = new CountDownLatch(1);
			SwingUtilities.invokeAndWait(() -> {
				new JFXPanel(); // initializes JavaFX environment
				latch.countDown();
			});
		} catch (InvocationTargetException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//		try {
		//			latch.await();
		//		} catch (InterruptedException e) {
		//			throw new RuntimeException(e);
		//		}
	}
	//-------------------------------------------------------------------
	private URL detectFullDownloadURL(URL url) {
		Platform.setImplicitExit(false);
		logger.debug("1");
		final CountDownLatch latch = new CountDownLatch(1);
		
		browser.load(url);
		if (Platform.isFxApplicationThread()) {
			logger.debug("2 run now");
			browser.run();
		} else {
			logger.debug("2 Calling Platform.runLater");
			Platform.runLater(browser);
		}
		
		logger.debug("3");
//		synchronized (browser) {
//			try {
//				browser.wait(10000);
//			} catch (InterruptedException e) {
//			}
//		}
		logger.debug("4");
		try {
			latch.await(10000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		logger.debug("Wait complete");
		if (browser.getDownloadURL()==null) {
			logger.fatal("Nothing found");
			System.exit(0);
		}
		
		
		return browser.getDownloadURL();
	}

	//-------------------------------------------------------------------
	private URL detectFullDownloadURLOld(URL url) {
		logger.debug("  Query HTML "+url);
		String cookie = null;
		try {
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.addRequestProperty("User-Agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()==200) {
				cookie = con.getHeaderField("Set-Cookie");
				if (cookie!=null) {
					logger.debug("  RCV cookie (before ): "+cookie);
					cookie = cookie.substring(0, cookie.indexOf(';'));
					logger.debug("  RCV cookie (encoded): "+cookie);
					logger.debug("  RCV cookie (decoded): "+URLDecoder.decode(cookie, "ISO-8859-1"));
				}
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while (true) {
					String line = in.readLine();
					if (line==null)
						break;
					if (line.contains("download")) {
						//						System.out.println("\n"+line);
						Matcher matcher = DOWNLOAD_PATTERN.matcher(line);
						if (matcher.matches()) {
							logger.info("  found download URL: "+matcher.group(1));
							//							logger.info("Line was "+line);
							return new URL(matcher.group(1));
						}

					}
				}
			}			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#preFillMetadata(java.net.URL, java.lang.String[])
	 */
	@Override
	public List<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		try {
			URL apiURL = new URL("https://backend.deviantart.com/oembed?url="+URLEncoder.encode(url.toExternalForm(), "UTF-8"));
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
			//			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200)
				return new ArrayList<>();

			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);

			OEmbedDA image = gson.fromJson(json, OEmbedDA.class);
			logger.debug("pretty = "+gson.toJson(image));

			preMeta.setArtist(image.getAuthorName());
			preMeta.setTitle(image.getTitle());
			preMeta.setWidth(image.getWidth());
			preMeta.setHeight(image.getHeight());

			if (image.getLicense()!=null) {
				OEmbedDA.License lic = image.getLicense();
				if (lic.getAttributes()!=null) {
					if (lic.getAttributes().get("href")!=null) {
						String licURL = lic.getAttributes().get("href");
						if (licURL.startsWith("http://creativecommons.org/licenses/by-nc-sa"))
							preMeta.setLicense(LicenseType.CC_BY_NC_SA);
						else if (licURL.startsWith("http://creativecommons.org/licenses/by-nc-nd"))
							preMeta.setLicense(LicenseType.CC_BY_NC_ND);
						else if (licURL.startsWith("http://creativecommons.org/licenses/by-nd"))
							preMeta.setLicense(LicenseType.CC_BY_ND);
						else if (licURL.startsWith("http://creativecommons.org/licenses/by-nc"))
							preMeta.setLicense(LicenseType.CC_BY_NC);
						else if (licURL.startsWith("http://creativecommons.org/licenses/by-sa"))
							preMeta.setLicense(LicenseType.CC_BY_SA);
						else if (licURL.startsWith("http://creativecommons.org/licenses/by"))
							preMeta.setLicense(LicenseType.CC_BY);
						else
							logger.error("Unsupported license URL found: "+licURL);
					}
				}
			}

			logger.debug("  copyright = "+image.getCopyright());
			if (image.getTags()!=null) {
				StringTokenizer tok = new StringTokenizer(image.getTags(),",");
				List<String> keywords = new ArrayList<String>();
				while (tok.hasMoreTokens()) {
					keywords.add(tok.nextToken().trim());
				}
				preMeta.setKeywords(keywords);
			}
			// Build copyright string
			if (image.getCopyright()!=null) {
				preMeta.setCopyright(String.format("%s by %s (%s)",
						image.getCopyright().getAttributes().get("year"),
						image.getCopyright().getAttributes().get("entity"),
						image.getCopyright().getAttributes().get("url")
						));
			}

			/*
			 * Search for a full download button
			 */
			logger.debug("  if available, replace with full download URL");
			URL url2 = detectFullDownloadURL(url); 
			if (url2==null) {
				logger.info("  Replaced URL with full download URL "+image.getUrl());
				url2 = new URL(image.getUrl());
			}


			logger.debug("  done");
			String filename = url2.getPath();
			if (filename.lastIndexOf('/')>=0)
				filename = filename.substring(filename.lastIndexOf('/')+1);

			return Arrays.asList(new MediaURL(url2, filename));
		} catch (IOException e) {
			logger.error("Error importing",e);
		}
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("resource")
	static String convertStreamToString(java.io.InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
		Matcher matcher = PATTERN.matcher(url.toString());
		if (!matcher.matches())
			return;
//		String hash = matcher.group(1);

		try {
			URL apiURL = new URL("https://backend.deviantart.com/oembed?url="+URLEncoder.encode(url.toExternalForm(), "UTF-8"));
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
			//			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200)
				return;

			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);

			OEmbed image = (new Gson()).fromJson(json, OEmbed.class);
			if (image.getTitle()!=null && media.getName()==null) {
				logger.info("  Set image title to: "+image.getTitle());
				media.setName(image.getTitle());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class RoboBrowser implements Runnable {

	private Logger logger = Logger.getLogger("babylon.media.deviantart");

	private boolean cancelOnDocument;
	private URL downloadURL;

	private WebEngine engine;
	private Stage primaryStage;
	private ObjectProperty<Boolean> initialized;
	
	//-------------------------------------------------------------------
	public RoboBrowser() {
		initialized = new SimpleObjectProperty<Boolean>(false);
	}
	
	//-------------------------------------------------------------------
	public void prepare() {
		WebView view = new WebView();
		view.setPrefSize(200, 200);
		engine = view.getEngine();
//		engine.setUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0");
		engine.setUserAgent("User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1");

		engine.locationProperty().addListener( (ov,o,n) -> locationChanged(n));
		engine.documentProperty().addListener( (ov,o,n) -> documentChanged(n));
		engine.setOnError(webEv -> logger.error("WebError: "+webEv));
		engine.setOnAlert(webEv -> logger.error("WebAlert: "+webEv));
		engine.setJavaScriptEnabled(true);
		Scene scene = new Scene(view, 900, 500);
		primaryStage = new Stage(StageStyle.DECORATED);
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);
		logger.debug("Prepared WebView");
	}

	//-------------------------------------------------------------------
	public ObjectProperty<Boolean> initializedProperty() { return initialized; }

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.debug("START:  run()");
		try {
//			primaryStage = new Stage(StageStyle.TRANSPARENT);
//
//			WebView view = new WebView();
//			view.setPrefSize(200, 200);
//			engine = view.getEngine();
//			engine.setUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0");
//
//			engine.locationProperty().addListener( (ov,o,n) -> locationChanged(n));
//			engine.documentProperty().addListener( (ov,o,n) -> documentChanged(n));
//			Scene scene = new Scene(view, 1, 1);
//			primaryStage.setTitle("Test");
//			primaryStage.setScene(scene);
			logger.debug("    call showAndWait()");
			
			primaryStage.showAndWait();
			logger.debug("    return from showAndWait()");
			initialized.setValue(true);
		} finally {
			logger.debug("STOP:  run()");
		}
	}

	//-------------------------------------------------------------------
	public void load(URL url) {
		logger.debug("load "+url);
		cancelOnDocument = true;
		Platform.runLater(new Runnable() {
			public void run() {
				engine.load(url.toExternalForm());
				logger.debug("  now wait for events");
			}});
	}

	//-------------------------------------------------------------------
	private void locationChanged(String n) {
		logger.debug("  URL changed to "+n);
		if (n.contains("orig00.deviantart")) {
			logger.debug("Full Download URL is "+n);
			try {
				downloadURL = new URL(n);
			} catch (MalformedURLException e) {
				logger.error("Full download URL is malformed: "+e);
			}
			primaryStage.hide();
			primaryStage.close();
			synchronized (RoboBrowser.this) {
				RoboBrowser.this.notifyAll();
			}
		}
	}

	//-------------------------------------------------------------------
	private void documentChanged(Document n) {
		//			 System.out.println("Document "+n);
		if (cancelOnDocument && n!=null) {
			engine.getLoadWorker().cancel();
			Document doc = n;
			NodeList list = doc.getElementsByTagName("a");
			boolean found = false;
			for (int i=0; i<list.getLength(); i++) {
				Element a = (Element)list.item(i);
				String href = a.getAttribute("href");
				if (href!=null && href.contains("www.deviantart.com/download")) {
					logger.debug("  Download URL to click "+href);
					found = true;
					String javascript = "document.getElementsByTagName('a')["+i+"].click()";
					engine.executeScript(javascript);
					logger.debug("  Clicked "+javascript);
				}
			}
			// If no download link found, hide all
			if (!found) {
				logger.info("  No full download found in page");
				engine.getLoadWorker().cancel();
				primaryStage.hide();
				primaryStage.close();
				synchronized (RoboBrowser.this) {
					RoboBrowser.this.notifyAll();
				}
			} else {
				logger.info("  Full download Button URL has been found and clicked");
			}
		}
		cancelOnDocument = false;
	}
	
	public URL getDownloadURL() {
		return downloadURL;
	}
}
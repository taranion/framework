/**
 * 
 */
package org.prelle.rpgframework.media;

import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.core.Genre;
import de.rpgframework.media.LicenseType;
import de.rpgframework.media.Media;
import de.rpgframework.media.MediaLibrary;
import de.rpgframework.media.MediaTag;

/**
 * @author prelle
 *
 */
public abstract class AbstractMedia implements Media {
	
	protected final static Logger logger = Logger.getLogger("babylon.media");
	
	protected transient MediaLibrary library;
	protected transient URL accessURL;
	/**
	 * Metadata relevant for persistance has changed
	 */
	protected transient boolean dirty;
	
	private UUID uuid;
	protected Path path;
	
	private String name;
	
	protected URL source;
	protected Category category;
	
	private List<MediaTag> tags;
	private List<Genre> genres;
	private LicenseType license;
	private String copyright;
	private String artist;
	private String series;
	private List<String> keywords;

	//-------------------------------------------------------------------
	protected AbstractMedia() {
		tags = new ArrayList<>();
		genres = new ArrayList<>();
		keywords = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	protected AbstractMedia(SerializableMetadata meta) {
		this();
		uuid = meta.getUUID();
		category  = meta.getCategory();
		artist    = meta.getArtist();
		series    = meta.getSeries();
		name      = meta.getTitle();
		source    = meta.getSourceURL();
		license   = meta.getLicense();
		copyright = meta.getCopyright();
		tags      = meta.getTags();
		keywords  = meta.getKeywords();
		genres    = meta.getGenre();
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (keywords==null)
			keywords = new ArrayList<>();
		List<String> tmp = new ArrayList<>();
		tmp.add("uuid="+uuid);
		tmp.add("relPath="+path);
		tmp.add("category="+category);
		if (name!=null) tmp.add("name="+name);
		if (series!=null) tmp.add("series="+series);
		if (source!=null) tmp.add("source="+source);
		if (copyright!=null) tmp.add("copyright="+copyright);
		if (license!=null) tmp.add("license="+license);
		if (artist!=null) tmp.add("artist="+artist);
		if (!keywords.isEmpty()) tmp.add("keywords="+keywords);
		return "("+String.join(", ", tmp)+")";
	}

	//-------------------------------------------------------------------
	@Override
	public boolean isDirty() { return dirty; }
	@Override
	public void clearDirty() { dirty = false; }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getLibrary()
	 */
	@Override
	public MediaLibrary getLibrary() {
		return library;
	}

	//-------------------------------------------------------------------
	public void setLibrary(MediaLibrary value) {
		this.library = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getUUID()
	 */
	@Override
	public UUID getUUID() {
		return uuid;
	}

	//-------------------------------------------------------------------
	public void setUUID(UUID uuid) {
		if (uuid!=null && uuid.equals(this.uuid))
			return;
		this.uuid = uuid;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getRelativePath()
	 */
	@Override
	public Path getRelativePath() {
		return path;
	}

	//-------------------------------------------------------------------
	public void setRelativePath(Path path) {
		if (path.isAbsolute()) 
			throw new IllegalArgumentException("Path is not relative");
		if (path!=null && path.equals(this.path))
			return;
		this.path = path;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getSource()
	 */
	@Override
	public URL getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#setSource(java.net.URL)
	 */
	@Override
	public void setSource(URL src) {
		if (src!=null && src.equals(this.source))
			return;
		this.source = src;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getCategory()
	 */
	@Override
	public Category getCategory() {
		return category;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#addTag(de.rpgframework.media.MediaTag)
	 */
	@Override
	public void addTag(MediaTag tag) {
		if (tags==null)
			tags = new ArrayList<MediaTag>();
		if (tags.contains(tag))
			return;

		// Eventually remove old tag from same category
		for (MediaTag exist : new ArrayList<MediaTag>(tags)) {
			if (exist.getClass()==tag.getClass())
				tags.remove(exist);
		}
		
		tags.add(tag);
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#removeTag(de.rpgframework.media.MediaTag)
	 */
	@Override
	public void removeTag(MediaTag tag) {
		if (tags==null)
			tags = new ArrayList<MediaTag>();
		tags.remove(tag);
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getTags()
	 */
	@Override
	public List<MediaTag> getTags() {
		if (tags==null)
			tags = new ArrayList<MediaTag>();
		return new ArrayList<>(tags);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.RoleplayingMetadata#setGenres(java.util.List)
	 */
	@Override
	public void setGenres(List<Genre> value) {
		if (value==null)
			throw new NullPointerException();
		if (value.equals(this.genres))
			return;
		this.genres = value;
		dirty = true;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#addGenre(de.rpgframework.core.Genre)
	 */
	@Override
	public void addGenre(Genre value) {
		if (!genres.contains(value)) {
			dirty = true;
			genres.add(value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#removeGenre(de.rpgframework.core.Genre)
	 */
	@Override
	public void removeGenre(Genre tag) {
		genres.remove(tag);
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getGenres()
	 */
	@Override
	public List<Genre> getGenres() {
		if (genres==null)
			genres = new ArrayList<Genre>();
		return new ArrayList<>(genres);
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name!=null && name.equals(this.name))
			return;
		this.name = name;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		if (category!=null && category.equals(this.category))
			return;
		this.category = category;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getLicense()
	 */
	@Override
	public LicenseType getLicense() {
		return license;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#setLicense(de.rpgframework.media.LicenseType)
	 */
	@Override
	public void setLicense(LicenseType value) {
		if (license!=null && license.equals(this.license))
			return;
		this.license = value;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getArtist()
	 */
	@Override
	public String getArtist() {
		return artist;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#setArtist(java.lang.String)
	 */
	@Override
	public void setArtist(String artist) {
		if (artist!=null && artist.equals(this.artist))
			return;
		this.artist = artist;
		dirty = true;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#setKeywords(java.util.List)
	 */
	@Override
	public void setKeywords(List<String> keywords) {
		if (keywords==null)
			throw new NullPointerException();
		if (keywords.equals(this.keywords))
			return;
		this.keywords = keywords;
		dirty = true;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getKeywords()
	 */
	@Override
	public List<String> getKeywords() {
		if (keywords==null)
			keywords = new ArrayList<>();
		return new ArrayList<>(keywords);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getAccessURL()
	 */
	@Override
	public URL getAccessURL() {
		return accessURL;
	}
	
	//-------------------------------------------------------------------
	public void setAccessURL(URL value) {
		if (value!=null && value.equals(this.accessURL))
			return;
		accessURL = value;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getCopyright()
	 */
	@Override
	public String getCopyright() {
		return copyright;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#setCopyright(java.lang.String)
	 */
	@Override
	public void setCopyright(String value) {
		if (value!=null && value.equals(this.copyright))
			return;
		copyright = value;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getSeries()
	 */
	@Override
	public String getSeries() {
		return series;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#setSeries(java.lang.String)
	 */
	@Override
	public void setSeries(String value) {
		if (value!=null && value.equals(this.series))
			return;
		this.series = value;
		dirty = true;
	}

}

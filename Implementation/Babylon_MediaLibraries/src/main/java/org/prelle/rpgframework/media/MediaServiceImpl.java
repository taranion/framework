/**
 *
 */
package org.prelle.rpgframework.media;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.simplepersist.Persister;

import de.rpgframework.ConfigOption;
import de.rpgframework.RPGFramework;
import de.rpgframework.media.MediaLibrary;
import de.rpgframework.media.MediaService;
import de.rpgframework.music.Filter;
import de.rpgframework.products.Adventure;

/**
 * @author prelle
 *
 */
public class MediaServiceImpl implements MediaService {


	private final static Logger logger = Logger.getLogger("babylon.media");

	private ConfigContainerImpl configRoot;
	private ConfigOption<String> cfgDataDir;

	/**
	 * Pointer to local directory containing media data
	 */
	private Path localBaseDir;

	private Persister persister;
	private Map<URI, LibraryDefinition> myLibraries;

	//--------------------------------------------------------------------
	public MediaServiceImpl(ConfigContainerImpl configRoot) throws IOException {
		this.configRoot = configRoot;

		prepareConfigNode();

		String dataDir = cfgDataDir.getStringValue();
		if (dataDir==null) {
			throw new NullPointerException("Config parameter "+cfgDataDir+" not set");
		}
		// Add player specific path
		localBaseDir = FileSystems.getDefault().getPath(dataDir, "media");
		logger.info("Expect media data at "+localBaseDir);
		if (!Files.exists(localBaseDir)) {
			logger.info("Create directory "+localBaseDir);
			Files.createDirectory(localBaseDir);
		}

		// Find all known libraries
		myLibraries = new HashMap<>();
		persister = new Persister();
		loadMyLibraries();
	}

	//------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void prepareConfigNode() {
		cfgDataDir = (ConfigOption<String>) configRoot.getOption(RPGFramework.PROP_DATADIR);
	}

	//--------------------------------------------------------------------
	private void loadMyLibraries() {

		try {
			for (Path xml : Files.newDirectoryStream(localBaseDir, p -> p.getFileName().toString().toLowerCase().endsWith(".xml"))) {
				logger.debug("Load "+xml);
				try {
					LibraryDefinition libDef = persister.read(LibraryDefinition.class, new FileInputStream(xml.toFile()));
					addLibrary(libDef);

					// Load database

				} catch (IOException | SQLException | URISyntaxException e) {
					logger.error("Failed loading library definition from "+xml,e);
				}
			}
		} catch (IOException e) {
			logger.error("Failed accessing directory with library definitions",e);
		}

		//		logger.fatal("Stop here");
		//		System.exit(0);
	}

	//-------------------------------------------------------------------
	@Override
	public List<MediaLibrary> getMyLibraries() {
		List<MediaLibrary> lib = new ArrayList<>();
		myLibraries.values().forEach(libDef -> lib.add(libDef.instance));
		return lib;
	}

	//-------------------------------------------------------------------
	private MediaLibrary addLibrary(LibraryDefinition libDef) throws URISyntaxException, SQLException, IOException {
		String urlString = libDef.getURL();
		if (!urlString.endsWith("/"))
			urlString+="/";

		URI uri = URI.create(urlString);

		MediaLibrary library = null;
		if (uri.getScheme().equals("file")) {
			library = new LocalFSMediaLibrary(libDef, Paths.get(uri));
		} else if (uri.getScheme().equals("http")) {
			library = new HttpMediaLibrary(libDef, uri);
		} else {
			logger.error("No support for libraries with URI scheme "+uri.getScheme());
			throw new IllegalArgumentException("No support for libraries with URI scheme "+uri.getScheme());
		}
		libDef.instance = library;
		myLibraries.put(uri, libDef);

		logger.info("Added '"+libDef.getName()+"' from "+libDef.getURL());
		return library;
	}

	//-------------------------------------------------------------------
	/**
	 * @throws URISyntaxException
	 * @throws SQLException
	 * @throws IOException
	 * @see de.rpgframework.media.MediaService#addMyLibrary(java.net.URL, java.lang.String)
	 */
	@Override
	public MediaLibrary addMyLibrary(URL url, String name) throws URISyntaxException, SQLException, IOException {
		LibraryDefinition libDef = new LibraryDefinition();
		libDef.setURL(url.toExternalForm());
		libDef.setName(name);
		libDef.setEnabled(true);

		MediaLibrary ret = addLibrary(libDef);
		logger.debug("Write new library definition to localBaseDir="+localBaseDir);

		Path xml = localBaseDir.resolve(name+".xml");
		persister.write(libDef, xml.toFile());
		logger.info("Wrote "+xml.toAbsolutePath());

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @throws URISyntaxException
	 * @throws SQLException
	 * @throws IOException
	 * @see de.rpgframework.media.MediaService#addMyLibrary(java.net.URL, java.lang.String)
	 */
	@Override
	public MediaLibrary addMyLibrary(URL url, String name, String login, String pass) throws URISyntaxException, SQLException, IOException {
		LibraryDefinition libDef = new LibraryDefinition();
		libDef.setURL(url.toExternalForm());
		libDef.setName(name);
		libDef.setLogin(login);
		libDef.setPassword(pass);
		libDef.setEnabled(true);

		MediaLibrary ret = addLibrary(libDef);

		Path xml = localBaseDir.resolve(name+".xml");
		persister.write(libDef, xml.toFile());
		logger.info("Wrote "+xml.toAbsolutePath());

		return ret;
	}

	//-------------------------------------------------------------------
	@Override
	public void removeMyLibrary(MediaLibrary library) {
		AbstractMediaLibrary media = (AbstractMediaLibrary)library;
		LibraryDefinition libDef = media.libDef;
		
		// Delete definition file
		Path xml = localBaseDir.resolve(libDef.getName()+".xml");
		try {
			Files.delete(xml);
		} catch (IOException e) {
			logger.error("Failed removing "+xml);
		}

		// Remove from cache
		String urlString = libDef.getURL();
		if (!urlString.endsWith("/"))
			urlString+="/";

		URI uri = URI.create(urlString);
		if (!myLibraries.containsKey(uri))
			throw new NoSuchElementException("No key "+uri+" in "+myLibraries.keySet());
		
		logger.warn("Removed library "+library);
		myLibraries.remove(uri);
	}

	//-------------------------------------------------------------------
	@Override
	public void updateMyLibrary(MediaLibrary library) {
		AbstractMediaLibrary aLib = (AbstractMediaLibrary)library;
		Path xml = localBaseDir.resolve(aLib.libDef.getName()+".xml");
		logger.info("Update "+xml);
		try {
			persister.write(aLib.libDef, xml.toFile());
		} catch (IOException e) {
			logger.error("Error updating "+xml,e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaService#setEnabled(de.rpgframework.media.WriteableMediaLibrary, boolean)
	 */
	@Override
	public void setEnabled(MediaLibrary library, boolean enabled) {
		((AbstractMediaLibrary)library).libDef.setEnabled(enabled);
		updateMyLibrary(library);
	}

	//-------------------------------------------------------------------
	public boolean isEnabled(MediaLibrary library) {
		return library.isEnabled();
	}

	//	//--------------------------------------------------------------------
	//	public LocalFSMediaLibrary getLocalLibrary() {
	//		logger.warn("\n\nRemove me\n\n");
	//		for (LibraryDefinition lib : myLibraries.values()) {
	//			logger.debug("Check "+lib+" bzw. "+lib.instance);
	//			if (lib.instance instanceof LocalFSMediaLibrary)
	//				return (LocalFSMediaLibrary)lib.instance;
	//		}
	//		return null;
	//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaService#getMediaLibrary(de.rpgframework.products.Adventure, de.rpgframework.music.Filter[])
	 */
	@Override
	public MediaLibrary getMediaLibrary(Adventure adv, Filter... filter) {
		logger.warn("TODO: Build a combined media library from all enabled libraries");
		// Get first media library
		for (LibraryDefinition lib : myLibraries.values()) {
			if (lib.isEnabled()) {
				return lib.instance;
			}
		}
		
		return null;
	}

	//-------------------------------------------------------------------
	private static String deCamelCase(String value) {
		StringBuffer buf = new StringBuffer();
		boolean wasLower = false;
		for (int i=0; i<value.length(); i++) {
			char ch = value.charAt(i);
			if (Character.isUpperCase(ch) && wasLower) {
				buf.append(' ');
			} else
				wasLower = true;
			buf.append(ch);
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	private static String[] parseFilename(String filename) {
		String artist = null;
		String series = null;
		String title  = null;

		if (filename.startsWith("HeroicMaps")) {
			StringTokenizer tok = new StringTokenizer(filename, "_");
			artist = deCamelCase(tok.nextToken());
			series = deCamelCase(tok.nextToken());
			title  = deCamelCase(tok.nextToken());
		} else {
			logger.warn("Don't know how to parse: "+filename);
			title = filename;
		}
		return new String[] {artist, series, title};
	}

	//	//-------------------------------------------------------------------
	//	/**
	//	 * @see de.rpgframework.media.MediaService#showOnSessionScreen(de.rpgframework.media.Media)
	//	 */
	//	@Override
	//	public void showOnSessionScreen(Media image) {
	//		// TODO Auto-generated method stub
	//		logger.warn("TODO: show image");
	//
	//		DeviceService deviceService = RPGFrameworkLoader.getInstance().getDeviceService();
	//		for (RPGToolDevice dev : deviceService.getAvailableDevices()) {
	////			logger.debug("  check "+dev.getName()+" / "+dev.getClass()+" // "+dev.getSupportedFunctions()+" ?");
	//			FunctionSessionHandout handout = (FunctionSessionHandout) dev.getFunction(DeviceFunction.SESSION_HANDOUT);
	//			if (handout==null)
	//				continue;
	//			logger.info("  can display on "+dev.getName());
	//
	//			if (dev.getName().startsWith("Kodi") || dev.getName().startsWith("JVC")) {
	//				handout.showHandout(image);
	//			}
	//
	//		}
	//	}

}


/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;

/**
 * @author prelle
 *
 */
public interface MetadataExtractor {
	
	public List<String> getSupportedSuffixes();

	//-------------------------------------------------------------------
	public SerializableMetadata readMetadata(InputStream remoteStream);

	//-------------------------------------------------------------------
	public SerializableMetadata readMetadata(Path localFile);

	//-------------------------------------------------------------------
	public byte[] readThumbnail(Path localFile);
	
	//-------------------------------------------------------------------
	public void writeMetadata(SerializableMetadata meta, Path imageFile) throws IOException;
	
	//-------------------------------------------------------------------
	public void writeThumbnail(byte[] data, Path imageFile) throws IOException;
	
}

/**
 * 
 */
package org.prelle.rpgframework.music;

import de.rpgframework.music.ScanResult;
import de.rpgframework.music.Track;

/**
 * @author prelle
 *
 */
public class FileSystemScanResult implements ScanResult {

	private FileSystemTrack track;
	private State state;
	
	//-------------------------------------------------------------------
	/**
	 */
	public FileSystemScanResult(FileSystemTrack track, State state) {
		this.track = track;
		this.state = state;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.music.ScanResult#getState()
	 */
	@Override
	public State getState() {
		return state;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.music.ScanResult#setState(de.rpgframework.music.ScanResult.State)
	 */
	@Override
	public void setState(State newState) {
		this.state = newState;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.music.ScanResult#getTrack()
	 */
	@Override
	public Track getTrack() {
		return track;
	}

}

/**
 * 
 */
package org.prelle.rpgframework.media;

/**
 * @author prelle
 *
 */
public enum DatabaseDefinition {

	UUID("uuid", 1),
	PATH("path", 2),
	TITLE("title", 3),
	ARTIST("artist",4),
	SERIES("series",5),
	ORIGIN("origin",6),
	CATEGORY("category",7),
	LICENSE("license",8),
	COPYRIGHT("copyright",9),
	DPI("dpi",10),
	TILETYPE("tileType",11),
	GEOMORPH("geomorph",12)
	;
	
	private String colName;
	private int colPos;
	
	private DatabaseDefinition(String name, int pos) {
		this.colName = name;
		this.colPos  = pos;
	}
	
	public String columnName() { return colName; }
	public int columnPos() { return colPos; }
	
}

package org.prelle.rpgframework.media.collector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.collector.DataCollector.MediaURL;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import com.google.gson.Gson;

import de.rpgframework.media.Media;

public class ImgurDataCollector implements DataCollector {

	private Logger logger = Logger.getLogger("babylon.media.imgur");

	final static Pattern PATTERN = Pattern.compile("http[s]*://i.imgur.com/(.*)\\....");


	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		logger.debug("check for "+url+" = "+PATTERN.matcher(url.toString()).matches());
		return PATTERN.matcher(url.toString()).matches();
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("resource")
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public List<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		return new ArrayList<DataCollector.MediaURL>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
		Matcher matcher = PATTERN.matcher(url.toString());
		if (!matcher.matches())
			return;
		String hash = matcher.group(1);
		
		try {
			URL apiURL = new URL("https://api.imgur.com/3/image/"+hash);
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200)
				return;
			
			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);
			
			ImgurImage image = (new Gson()).fromJson(json, ImgurImage.class);
			if (image.getTitle()!=null && media.getName()==null) {
				logger.info("  Set image title to: "+image.getTitle());
				media.setName(image.getTitle());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
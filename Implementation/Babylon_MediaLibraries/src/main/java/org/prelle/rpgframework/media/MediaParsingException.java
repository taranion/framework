/**
 * 
 */
package org.prelle.rpgframework.media;

import java.io.IOException;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class MediaParsingException extends IOException {

	//-------------------------------------------------------------------
	/**
	 */
	public MediaParsingException() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param message
	 */
	public MediaParsingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param cause
	 */
	public MediaParsingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param message
	 * @param cause
	 */
	public MediaParsingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}

/**
 * 
 */
package org.prelle.rpgframework.music;

import de.rpgframework.music.UniqueTrackID;

/**
 * @author prelle
 *
 */
public class BabylonUniqueTrackID implements UniqueTrackID {

	private String mbID;
	
	//-------------------------------------------------------------------
	/**
	 */
	public BabylonUniqueTrackID(String mbID) {
		this.mbID = mbID;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.music.UniqueTrackID#getMusicBrainzID()
	 */
	@Override
	public String getMusicBrainzID() {
		return mbID;
	}

}

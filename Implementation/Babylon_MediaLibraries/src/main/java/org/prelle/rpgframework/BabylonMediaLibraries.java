/**
 * 
 */
package org.prelle.rpgframework;

import java.util.List;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.BabylonPlugin;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.RPGFrameworkImpl;
import org.prelle.rpgframework.media.MediaServiceImpl;

import de.rpgframework.FunctionMediaLibraries;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader.FunctionType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.media.MediaService;

/**
 * @author prelle
 *
 */
public class BabylonMediaLibraries implements BabylonPlugin, FunctionMediaLibraries {

	private final static Logger logger = Logger.getLogger("babylon.media");

	private MediaServiceImpl mediaService;

	//-------------------------------------------------------------------
	public BabylonMediaLibraries() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#getType()
	 */
	@Override
	public FunctionType getType() {
		return FunctionType.MEDIA_LIBRARIES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionMediaLibraries#getMediaService()
	 */
	@Override
	public MediaService getMediaService() {
		return mediaService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#initialize(org.prelle.rpgframework.RPGFrameworkImpl, org.prelle.rpgframework.ConfigContainerImpl, de.rpgframework.RPGFrameworkInitCallback)
	 */
	@Override
	public void initialize(RPGFrameworkImpl fwImpl, ConfigContainerImpl configRoot, RPGFrameworkInitCallback callback, List<RoleplayingSystem> limit) {
		logger.info("Initialize media libraries");
		try {
			mediaService     = new MediaServiceImpl(configRoot);
			fwImpl.setMediaLibraries(this);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

}

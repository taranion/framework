package org.prelle.rpgframework.media.collector;

class ImgurGallery {
	private String id;
    private String title;
    private String description;
    private String cover;
    private String datetime;
    private String link;
    private ImgurImage[] images;
 
    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCover() {
        return cover;
    }

    public String getDatetime() {
        return datetime;
    }

	//-------------------------------------------------------------------
	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the images
	 */
	public ImgurImage[] getImages() {
		return images;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
}
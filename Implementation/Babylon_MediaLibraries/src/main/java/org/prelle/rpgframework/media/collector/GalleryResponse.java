/**
 * 
 */
package org.prelle.rpgframework.media.collector;

/**
 * @author prelle
 *
 */
public class GalleryResponse {
	
	private ImgurGallery data;

	//-------------------------------------------------------------------
	/**
	 */
	public GalleryResponse() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the data
	 */
	public ImgurGallery getData() {
		return data;
	}

}

/**
 * 
 */
package org.prelle.rpgframework.media.collector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.collector.DataCollector.MediaURL;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.media.Media;

/**
 * @author prelle
 *
 */
public class PinterestDataCollector implements DataCollector {

	private Logger logger = Logger.getLogger("babylon.media.pinterest");

	final static Pattern PATTERN = Pattern.compile("http[s]*://www.pinterest.*/pin/(.*)");
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		logger.debug("check for "+url+" = "+PATTERN.matcher(url.toString()).matches());
		return PATTERN.matcher(url.toString()).matches();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#preFillMetadata(java.net.URL, java.lang.String[])
	 */
	@Override
	public List<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		try {
			URL apiURL = new URL("http://open.iframe.ly/api/oembed?url="+URLEncoder.encode(url.toExternalForm(), "UTF-8")+"&origin=preview");
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
			con.addRequestProperty("Referer", "https://iframely.com/");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200) {
				System.err.println(convertStreamToString(con.getErrorStream()));
				return new ArrayList<DataCollector.MediaURL>();
			}
			
			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);
			
			OEmbed image = gson.fromJson(json, OEmbed.class);
			logger.debug("pretty = "+gson.toJson(image));
			
			preMeta.setArtist(image.getAuthorName());
			preMeta.setTitle(image.getTitle());
			preMeta.setWidth(image.getWidth());
			preMeta.setHeight(image.getHeight());
			logger.debug("copy "+image.getCopyright().getAttributes());
			if (image.getTags()!=null) {
				StringTokenizer tok = new StringTokenizer(image.getTags(),",");
				List<String> keywords = new ArrayList<String>();
				while (tok.hasMoreTokens()) {
					keywords.add(tok.nextToken().trim());
				}
				preMeta.setKeywords(keywords);
			}
			// Build copyright string
			preMeta.setCopyright(String.format("%s by %s (%s)",
					image.getCopyright().getAttributes().get("year"),
					image.getCopyright().getAttributes().get("entity"),
					image.getCopyright().getAttributes().get("url")
					));
			URL url2 = new URL(image.getUrl());
			String filename = url2.getPath();
			if (filename.lastIndexOf('/')>=0)
				filename = filename.substring(filename.lastIndexOf('/')+1);
			return Arrays.asList(new MediaURL(url2, filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<DataCollector.MediaURL>();
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("resource")
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
		Matcher matcher = PATTERN.matcher(url.toString());
		if (!matcher.matches())
			return;
		String hash = matcher.group(1);
		
		try {
			URL apiURL = new URL("http://open.iframe.ly/api/oembed?url=?url="+URLEncoder.encode(url.toExternalForm(), "UTF-8"));
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
//			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200)
				return;
			
			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);
			
			OEmbed image = (new Gson()).fromJson(json, OEmbed.class);
			if (image.getTitle()!=null && media.getName()==null) {
				logger.info("  Set image title to: "+image.getTitle());
				media.setName(image.getTitle());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

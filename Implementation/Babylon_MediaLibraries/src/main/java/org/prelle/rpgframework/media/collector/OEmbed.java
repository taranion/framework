/**
 * 
 */
package org.prelle.rpgframework.media.collector;

import java.util.Map;

/**
 * @author prelle
 * "version": "1.0",
  "type": "photo",
  "title": "Cope",
  "url": "https://fc04.deviantart.net/fs50/f/2009/336/4/7/Cope_by_pachunka.jpg",
  "author_name": "pachunka",
  "author_url": "https://pachunka.deviantart.com",
  "provider_name": "DeviantArt",
  "provider_url": "https://www.deviantart.com",
  "thumbnail_url": "https://th03.deviantart.net/fs50/300W/f/2009/336/4/7/Cope_by_pachunka.jpg",
  "thumbnail_width": 300,
  "thumbnail_height": 450,
  "width": 448,
  "height": 672
 *
 */
public class OEmbed {
	
	public class Copyright {
		private Map<String,String> _attributes;
		public Map<String,String> getAttributes() { return _attributes; }
	}
	
	private String version;
	private String type;
	private String title;
	private String category;
	private String url;
	private String author_name;
	private String author_url;
	private String tags;
	private int width;
	private int height;
	private Copyright copyright;
	private String license;
	
	//-------------------------------------------------------------------
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the tile
	 */
	public String getTitle() {
		return title;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the author_name
	 */
	public String getAuthorName() {
		return author_name;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the author_url
	 */
	public String getAuthorUrl() {
		return author_url;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the copyright
	 */
	public Copyright getCopyright() {
		return copyright;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the license
	 */
	public String getLicense() {
		return license;
	}
	
}

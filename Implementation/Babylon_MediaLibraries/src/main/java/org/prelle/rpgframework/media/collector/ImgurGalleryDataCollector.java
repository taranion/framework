package org.prelle.rpgframework.media.collector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import com.google.gson.Gson;

import de.rpgframework.media.Media;

public class ImgurGalleryDataCollector implements DataCollector {

	private Logger logger = Logger.getLogger("babylon.media.imgur");

	final static Pattern PATTERN = Pattern.compile("http[s]*://imgur.com/gallery/(.*)");
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		return PATTERN.matcher(url.toString()).matches();
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("resource")
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#preFillMetadata(java.net.URL, java.lang.String[])
	 */
	@Override
	public List<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		logger.debug("START: fillMetadata for "+url);
		Matcher matcher = PATTERN.matcher(url.toString());
		if (!matcher.matches())
			return new ArrayList<DataCollector.MediaURL>();
		String hash = matcher.group(1);
		
		try {
			URL apiURL = new URL("https://api.imgur.com/3/gallery/"+hash);
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200)
				return new ArrayList<DataCollector.MediaURL>();
			
			String json = convertStreamToString(con.getInputStream());
			
			logger.debug("  JSON = "+json);
			GalleryResponse resp = (new Gson()).fromJson(json, GalleryResponse.class);
			ImgurGallery gallery = resp.getData();
//			logger.debug("Gallery = "+gallery);
//			logger.debug("Title = "+gallery.getTitle());
//			logger.debug("Descr = "+gallery.getDescription());
			preMeta.setTitle(gallery.getTitle());
//			logger.debug("Link  = "+gallery.getLink());
//			logger.debug("Images  = "+gallery.getImages());
			for (ImgurImage image : gallery.getImages()) {
				if (image.getLink()!=null) {
					URL newURL = new URL(image.getLink());
					logger.info("  Change URL to "+newURL);
					return Arrays.asList(new MediaURL(newURL, newURL.getFile()));
				}
			}
		} catch (IOException e) {
			logger.error("Failed in Imgur",e);
		} finally {
			logger.debug("STOP : fillMetadata for "+url);
		}
		return new ArrayList<DataCollector.MediaURL>();
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
	}

}
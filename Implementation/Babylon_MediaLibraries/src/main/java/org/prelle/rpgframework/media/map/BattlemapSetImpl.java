/**
 * 
 */
package org.prelle.rpgframework.media.map;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.prelle.rpgframework.media.AbstractMedia;

import de.rpgframework.media.Media;
import de.rpgframework.media.map.Battlemap;
import de.rpgframework.media.map.BattlemapSet;

/**
 * @author prelle
 *
 */
public class BattlemapSetImpl extends AbstractMedia implements BattlemapSet {
	
	private transient Path baseDir;
	private List<UUID> elements;

	//-------------------------------------------------------------------
	public BattlemapSetImpl(Path baseDir) {
		this.baseDir = baseDir;
		super.category = Category.BATTLEMAPSET;
		elements = new ArrayList<UUID>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaCollection#getLocalPath()
	 */
	@Override
	public Path getLocalPath() {
		return baseDir;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaCollection#add(de.rpgframework.media.Media)
	 */
	@Override
	public void add(Battlemap element) {
		if (!elements.contains(element.getUUID()))
			elements.add(element.getUUID());
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Battlemap> iterator() {
		List<Battlemap> ret = new ArrayList<>();
		for (UUID key : elements) {
			Media media = library.getMedia(key);
			if (media instanceof Battlemap)
				ret.add( (Battlemap)media );
		}
		return ret.iterator();
	}

}

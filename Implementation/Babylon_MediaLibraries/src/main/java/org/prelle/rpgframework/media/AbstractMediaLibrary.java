/**
 * 
 */
package org.prelle.rpgframework.media;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.map.BattleMapImpl;
import org.prelle.rpgframework.media.map.BattlemapSetImpl;
import org.prelle.rpgframework.media.map.TileDefinitionImpl;
import org.prelle.rpgframework.media.map.TilesetImpl;

import de.rpgframework.core.Genre;
import de.rpgframework.media.LicenseType;
import de.rpgframework.media.Media;
import de.rpgframework.media.MediaCollection;
import de.rpgframework.media.MediaLibrary;
import de.rpgframework.media.RoleplayingMetadata.Category;
import de.rpgframework.media.TagRegistry;

/**
 * @author prelle
 *
 */
public abstract class AbstractMediaLibrary implements MediaLibrary {

	protected Logger logger = Logger.getLogger("babylon.media");

	protected LibraryDefinition libDef;
	protected Connection con;
	protected Map<UUID, AbstractMedia> knownMedia;

	//-------------------------------------------------------------------
	/**
	 */
	public AbstractMediaLibrary(LibraryDefinition libDef) {
		knownMedia = new HashMap<>();
		this.libDef = libDef;
		if (libDef==null)
			throw new NullPointerException();
	}

	//-------------------------------------------------------------------
	public void close() {
		try {
			con.close();
		} catch (SQLException e) {
			logger.error("Failed closing database: "+e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getName()
	 */
	@Override
	public String getName() {
		return libDef.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getDescription()
	 */
	@Override
	public String getDescription() {
		return libDef.getDescription();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getLocation()
	 */
	@Override
	public URI getLocation() {
		try {
			return (new URL(libDef.getURL())).toURI();
		} catch (MalformedURLException | URISyntaxException e) {
			logger.error(e);
			return null;
		}
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return libDef.isEnabled();
	}

	//-------------------------------------------------------------------
	private void parseGeneric(ResultSet set, AbstractMedia media) throws SQLException {
		media.setUUID(UUID.fromString(set.getString(DatabaseDefinition.UUID.columnName())));
		media.setRelativePath(Paths.get(set.getString(DatabaseDefinition.PATH.columnName())));
		media.setName(set.getString(DatabaseDefinition.TITLE.columnName()));
		media.setArtist(set.getString(DatabaseDefinition.ARTIST.columnName()));
		media.setSeries(set.getString(DatabaseDefinition.SERIES.columnName()));
		String url = set.getString(DatabaseDefinition.ORIGIN.columnName());
		if (url!=null)
			try {
				media.setSource(new URL(url));
			} catch (MalformedURLException e) {
				logger.error("Invalid URL in ResultSet: "+set.getString(DatabaseDefinition.ORIGIN.columnName()));
			}
		media.setCategory(Category.valueOf(set.getString(DatabaseDefinition.CATEGORY.columnName())));
		String lic = set.getString(DatabaseDefinition.LICENSE.columnName());
		if (lic!=null)
			media.setLicense(LicenseType.valueOf(lic));
		
		// Store in database (remaining parsing of media type pending in caller)
//		logger.debug("store "+media.getUUID());
		if (knownMedia.containsKey(media.getUUID()))
			throw new IllegalStateException("Already know that UUID");
		knownMedia.put(media.getUUID(), media);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected <T extends Media> MediaCollection<T> parseAsCollection(MediaCollection<T> parent) throws SQLException {
		Statement stat = con.createStatement();
		ResultSet set = stat.executeQuery("SELECT child FROM Collections WHERE parent='"+parent.getUUID()+"'");
		while (set.next()) {
			UUID key = UUID.fromString(set.getString(1));
			Media media = getMedia(key);
			parent.add( (T)media );
		}
		return parent;
	}

	//-------------------------------------------------------------------
	private ImageMediaImpl parseAsImage(ResultSet set, ImageMediaImpl media) throws SQLException {
		parseGeneric(set, media);
		media.setDPI(set.getInt(DatabaseDefinition.DPI.columnName())); 
		return media;
	}

	//-------------------------------------------------------------------
	private TileDefinitionImpl parseAsTile(ResultSet set, TileDefinitionImpl media) throws SQLException {
		parseAsImage(set, media);
		return media;
	}

	//-------------------------------------------------------------------
	private TilesetImpl parseAsTileset(ResultSet set, TilesetImpl media) throws SQLException {
		parseGeneric(set, media);
		return media;
	}

	//-------------------------------------------------------------------
	private BattleMapImpl parseAsBattlemap(ResultSet set, BattleMapImpl media) throws SQLException {
		parseAsImage(set, media);
		return media;
	}

	//-------------------------------------------------------------------
	private BattlemapSetImpl parseAsBattlemapSet(ResultSet set, BattlemapSetImpl media) throws SQLException {
		parseGeneric(set, media);
//		parseAsImage(set, media);
		return media;
	}
	
	//-------------------------------------------------------------------
	protected Media parseResultSet(ResultSet set) throws SQLException {
		Category cat = Category.valueOf(set.getString(DatabaseDefinition.CATEGORY.columnName()));
		Path relPath = Paths.get(set.getString(DatabaseDefinition.PATH.columnName()));
		logger.info("TODO: parse "+cat+" / "+relPath);
		switch (cat) {
		case TILESET:
			TilesetImpl tiles = parseAsTileset(set, new TilesetImpl(relPath));
			tiles.setLibrary(this);
			return tiles;
		case MAPTILE:
			TileDefinitionImpl tile = parseAsTile(set, new TileDefinitionImpl(relPath));
			tile.setLibrary(this);
			return tile;
		case NPC:
			ImageMediaImpl image = parseAsImage(set, new ImageMediaImpl(relPath, cat));
			image.setLibrary(this);
			return image;
		case BATTLEMAP_STATIC:
			BattleMapImpl bmap = parseAsBattlemap(set, new BattleMapImpl(relPath, cat));
			bmap.setLibrary(this);
			return bmap;
		case BATTLEMAPSET:
			BattlemapSetImpl bmaps = parseAsBattlemapSet(set, new BattlemapSetImpl(relPath));
			bmaps.setLibrary(this);
			return bmaps;
		default:
			logger.warn("Don't know how to parse "+cat);
			System.exit(0);
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	private void loadMediaTagsFromDB(Statement stat, Media media) throws SQLException {
		ResultSet set = stat.executeQuery("SELECT type,value FROM MediaTags WHERE media='"+media.getUUID()+"'");
		while (set.next()) {
			media.addTag(TagRegistry.resolveTag(set.getString(1), set.getString(2)));
		}
	}

	//-------------------------------------------------------------------
	private void loadMediaGenresFromDB(Statement stat, Media media) throws SQLException {
		ResultSet set = stat.executeQuery("SELECT genre FROM MediaGenre WHERE media='"+media.getUUID()+"'");
		while (set.next()) {
			media.addGenre(Genre.valueOf(set.getString(1)));
		}
	}

	//-------------------------------------------------------------------
	private void loadMediaKeywordsFromDB(Statement stat, Media media) throws SQLException {
		ResultSet set = stat.executeQuery("SELECT word FROM MediaKeywords WHERE media='"+media.getUUID()+"'");
		List<String> keywords = new ArrayList<>();
		while (set.next()) {
			keywords.add(set.getString(1));
		}
		if (!keywords.isEmpty())
			media.setKeywords(keywords);
	}

	//-------------------------------------------------------------------
	protected void openDatabase() {
		logger.info("open database");
		
		ResultSet set = null;
		try {
			Statement stat    = con.createStatement();
			stat.closeOnCompletion();
			set = stat.executeQuery("SELECT * FROM Media");
			while (set.next()) {
				try {
					Media media = parseResultSet(set);
					loadMediaTagsFromDB(stat, media);
					loadMediaGenresFromDB(stat, media);
					loadMediaKeywordsFromDB(stat, media);
					logger.debug("* Loaded "+media.getUUID()+" with "+media.getTags()+" keywords="+media.getKeywords());
					media.clearDirty();
				} catch (Exception e) {
					logger.error("Failed parsing result set",e);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			logger.info("Database contains "+knownMedia.size()+" elements");
			// Close result set
			try {if (set!=null) set.close(); } catch (SQLException e) {}
		}
		
		/*
		 * Read collections
		 */
		for (Media tmp : knownMedia.values()) {
			if (tmp instanceof MediaCollection<?>) {
				logger.debug("collection "+tmp);
				try {
					parseAsCollection((MediaCollection) tmp);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
//		/*
//		 * Read tags
//		 */
//		try {
//			Statement stat    = con.createStatement();
//			stat.closeOnCompletion();
//			set = stat.executeQuery("SELECT media,type,value FROM MediaTags");
//			while (set.next()) {
//				try {
//					Media media = knownMedia.get(UUID.fromString(set.getString(1)));
//					media.addTag(TagRegistry.resolveTag(set.getString(2), set.getString(3)));
//				} catch (Exception e) {
//					logger.error("Failed parsing result set to MediaTag: "+e);
//				}
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			logger.info("Database contains "+knownMedia.size()+" elements");
//			// Close result set
//			try {if (set!=null) set.close(); } catch (SQLException e) {}
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getMedia(java.util.UUID)
	 */
	@Override
	public Media getMedia(UUID key) {
		// Use cached version - should be available
		if (knownMedia.containsKey(key))
			return knownMedia.get(key);
		
		/*
		 * Fall back to querying SQL
		 */
		logger.error("******************* Need to fall back .... why? ********************* "+key);
		logger.error("Search "+key);
		for (UUID tmp : knownMedia.keySet())
			logger.debug("  have "+tmp);
		System.exit(0);
		Statement stat = null;
		ResultSet set = null;
		try {
			stat = con.createStatement();
			set = stat.executeQuery("SELECT * FROM Media WHERE uuid='"+key+"'");
			while (set.next()) {
				return parseResultSet(set);
			}
		} catch (SQLException e) {
			logger.error("Failed querying database for media by UUID: "+e);
		} finally {
			try {
				if (set!=null) set.close();
				if (stat!=null) stat.close();
			} catch (SQLException e) {
			}
		}

		return null;
	}

}

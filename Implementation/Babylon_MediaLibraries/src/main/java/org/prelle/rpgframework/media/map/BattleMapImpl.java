/**
 * 
 */
package org.prelle.rpgframework.media.map;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.prelle.rpgframework.media.ImageMediaImpl;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.media.map.Battlemap;

/**
 * @author prelle
 *
 */
public class BattleMapImpl extends ImageMediaImpl implements Battlemap {
	
	private List<String> layerNames;

	//-------------------------------------------------------------------
	public BattleMapImpl(Path file, Category cat) {
		super(file, cat);
		layerNames = new ArrayList<String>();
	}
	
	//-------------------------------------------------------------------
	public BattleMapImpl(SerializableMetadata meta) {
		super(meta);
		layerNames = meta.getLayerNames();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.map.Battlemap#getLayer()
	 */
	@Override
	public List<String> getLayer() {
		return layerNames;
	}

}

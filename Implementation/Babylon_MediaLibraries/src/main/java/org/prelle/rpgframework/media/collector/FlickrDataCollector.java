/**
 * 
 */
package org.prelle.rpgframework.media.collector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.media.Media;

/**
 * @author prelle
 *
 */
public class FlickrDataCollector implements DataCollector {

	private class Raw {
		String _content;
	}
	
	private class Tag {
		String tagspace;
		String tagspaceid;
		String tag;
		String label;
		Raw raw;
	}

	private class Photo {
		String id;
		String camera;
		List<Tag> exif;
	}

	private class JSONFlickerAPI {
		String stat;
		int code;
		String message;
		Photo photo;
	}
	
	private final static String API_KEY = "31422796b20438cafc84fdba51044694";
	private final static String API_SECRET = "5fb8eb37f15fddbc";
	
	private Logger logger = Logger.getLogger("babylon.media.flickr");

	final static Pattern PATTERN = Pattern.compile("http[s]*://www.flickr.com/photos/([^/]*)/([^/]*).*");
	final static String API_URL = "https://www.flickr.com/services/oembed/";
	final static String API_URL2 = "https://www.flickr.com/services/rest/";
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		return PATTERN.matcher(url.toString()).matches();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#preFillMetadata(java.net.URL, java.lang.String[])
	 */
	@Override
	public List<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		Matcher matcher = PATTERN.matcher(url.toString());
		if (!matcher.matches())
			return new ArrayList<DataCollector.MediaURL>();
		String userID  = matcher.group(1);
		String imageID = matcher.group(2);
		try {
			url = new URL("https://www.flickr.com/photos/"+userID+"/"+imageID);
			preMeta.setSourceURL(url);
			logger.debug("Reduced to URL "+url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		try {
			URL apiURL = new URL(API_URL+"?url="+URLEncoder.encode(url.toExternalForm(), "UTF-8")+"&format=json");
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
//			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200) {
				System.err.println(convertStreamToString(con.getInputStream()));
				return new ArrayList<DataCollector.MediaURL>();
			}
			
			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);
			
			OEmbed image = gson.fromJson(json, OEmbed.class);
			logger.debug("pretty = "+gson.toJson(image));
			
			preMeta.setArtist(image.getAuthorName());
			preMeta.setTitle(image.getTitle());
			preMeta.setWidth(image.getWidth());
			preMeta.setHeight(image.getHeight());
			if (image.getTags()!=null) {
				StringTokenizer tok = new StringTokenizer(image.getTags(),",");
				List<String> keywords = new ArrayList<String>();
				while (tok.hasMoreTokens()) {
					keywords.add(tok.nextToken().trim());
				}
				preMeta.setKeywords(keywords);
			}
			// Build copyright string
			if (image.getCopyright()!=null) {
				preMeta.setCopyright(String.format("%s by %s (%s)",
						image.getCopyright().getAttributes().get("year"),
						image.getCopyright().getAttributes().get("entity"),
						image.getCopyright().getAttributes().get("url")
						));
			}
			URL url2 = new URL(image.getUrl());
			String filename = url2.getPath();
			if (filename.lastIndexOf('/')>=0)
				filename = filename.substring(filename.lastIndexOf('/')+1);
			return Arrays.asList(new MediaURL(url2, filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<DataCollector.MediaURL>();
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("resource")
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
		Matcher matcher = PATTERN.matcher(url.toString());
		if (!matcher.matches())
			return;
		String userID  = matcher.group(1);
		String imageID = matcher.group(2);
		String photoID = userID+"/"+imageID;

		
		/*
		 * Get general info
		 */
		boolean getInfoFailed = true;
		try {
			URL apiURL = new URL(API_URL2+"?method=flickr.photos.getInfo&format=json&photo_id="+photoID+"&api_key="+API_KEY);
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
//			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200) {
//				System.err.println("Error = "+convertStreamToString(con.getErrorStream()));
				return;
			}
			
			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);
			json = json.substring(14, json.length()-1);
			logger.debug("  JSON= "+json);
			
			JSONFlickerAPI image = (new Gson()).fromJson(json, JSONFlickerAPI.class);
			getInfoFailed = (image.code!=0);
			if (image.photo!=null) {
				for (Tag tag : image.photo.exif) {
					logger.debug(" * "+tag.label+" = "+tag.raw._content);
				}
			}
//			if (image.getTitle()!=null && media.getName()==null) {
//				logger.info("  Set image title to: "+image.getTitle());
//				media.setName(image.getTitle());
//			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Get EXIF
		 */
		try {
			URL apiURL = new URL(API_URL2+"?method=flickr.photos.getExif&format=json&photo_id="+photoID+"&api_key="+API_KEY);
			logger.debug("  Query "+apiURL);
			HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
//			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
			logger.debug("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200) {
//				System.err.println("Error = "+convertStreamToString(con.getErrorStream()));
				return;
			}
			
			String json = convertStreamToString(con.getInputStream());
			logger.debug("  JSON= "+json);
			json = json.substring(14, json.length()-1);
			logger.debug("  JSON= "+json);
			
			JSONFlickerAPI image = (new Gson()).fromJson(json, JSONFlickerAPI.class);
			for (Tag tag : image.photo.exif) {
				logger.debug(" * "+tag.label+" = "+tag.raw._content);
			}
//			if (image.getTitle()!=null && media.getName()==null) {
//				logger.info("  Set image title to: "+image.getTitle());
//				media.setName(image.getTitle());
//			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

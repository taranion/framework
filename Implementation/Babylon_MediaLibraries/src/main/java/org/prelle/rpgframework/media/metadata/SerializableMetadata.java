/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.net.URL;
import java.util.List;
import java.util.UUID;

import de.rpgframework.core.Genre;
import de.rpgframework.media.ImageType;
import de.rpgframework.media.LicenseType;
import de.rpgframework.media.MediaTag;
import de.rpgframework.media.RoleplayingMetadata.Category;
import de.rpgframework.media.map.TileType;

/**
 * @author prelle
 *
 */
public class SerializableMetadata {
	
	private UUID uuid;
	private Category category;
	
	private List<String> keywords;
	private List<MediaTag> tags;
	private List<Genre> genre;
	
	private String artist;
	private String series;
	private String title;
	private LicenseType license;
	private String copyright;
	private URL sourceURL;
	
	private ImageType imageType;
	private int width, height;
	private int dpi;
	
	private TileType tileType;
	private boolean geomorph;
	
	private List<String> layerNames;

	private boolean loopable;
	
	private byte[] thumbnail;

	//-------------------------------------------------------------------
	public SerializableMetadata() {
	}

	//-------------------------------------------------------------------
	/**
	 * Fill missing metadata from other
	 */
	public void addFrom(SerializableMetadata other) {
		if (uuid==null) uuid = other.getUUID();
		if (category==null) category = other.getCategory();
		if (keywords==null) keywords = other.getKeywords();
		if (tags==null) tags = other.getTags();
		if (genre==null) genre = other.getGenre();
		if (artist==null) artist = other.getArtist();
		if (title==null) title = other.getTitle();
		if (license==null) license = other.getLicense();
		if (copyright==null) copyright = other.getCopyright();
		if (sourceURL==null) sourceURL = other.getSourceURL();
		if (width==0) width = other.getWidth();
		if (height==0) height = other.getHeight();
		if (dpi==0) dpi = other.getDPI();
		if (imageType==null) imageType = other.getImageType();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uuid
	 */
	public UUID getUUID() {
		return uuid;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uuid the uuid to set
	 */
	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	//-------------------------------------------------------------------
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the keywords
	 */
	public List<String> getKeywords() {
		return keywords;
	}

	//-------------------------------------------------------------------
	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tags
	 */
	public List<MediaTag> getTags() {
		return tags;
	}

	//-------------------------------------------------------------------
	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<MediaTag> tags) {
		this.tags = tags;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the genre
	 */
	public List<Genre> getGenre() {
		return genre;
	}

	//-------------------------------------------------------------------
	/**
	 * @param genre the genre to set
	 */
	public void setGenre(List<Genre> genre) {
		this.genre = genre;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the artist
	 */
	public String getArtist() {
		return artist;
	}

	//-------------------------------------------------------------------
	/**
	 * @param artist the artist to set
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the series
	 */
	public String getSeries() {
		return series;
	}

	//-------------------------------------------------------------------
	/**
	 * @param series the series to set
	 */
	public void setSeries(String series) {
		this.series = series;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	//-------------------------------------------------------------------
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the license
	 */
	public LicenseType getLicense() {
		return license;
	}

	//-------------------------------------------------------------------
	/**
	 * @param license the license to set
	 */
	public void setLicense(LicenseType license) {
		this.license = license;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the copyright
	 */
	public String getCopyright() {
		return copyright;
	}

	//-------------------------------------------------------------------
	/**
	 * @param copyright the copyright to set
	 */
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the dpi
	 */
	public int getDPI() {
		return dpi;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dpi the dpi to set
	 */
	public void setDPI(int dpi) {
		this.dpi = dpi;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tileType
	 */
	public TileType getTileType() {
		return tileType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param tileType the tileType to set
	 */
	public void setTileType(TileType tileType) {
		this.tileType = tileType;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the geomorph
	 */
	public boolean isGeomorph() {
		return geomorph;
	}

	//-------------------------------------------------------------------
	/**
	 * @param geomorph the geomorph to set
	 */
	public void setGeomorph(boolean geomorph) {
		this.geomorph = geomorph;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the loopable
	 */
	public boolean isLoopable() {
		return loopable;
	}

	//-------------------------------------------------------------------
	/**
	 * @param loopable the loopable to set
	 */
	public void setLoopable(boolean loopable) {
		this.loopable = loopable;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sourceURL
	 */
	public URL getSourceURL() {
		return sourceURL;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sourceURL the sourceURL to set
	 */
	public void setSourceURL(URL sourceURL) {
		this.sourceURL = sourceURL;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the layerNames
	 */
	public List<String> getLayerNames() {
		return layerNames;
	}

	//-------------------------------------------------------------------
	/**
	 * @param layerNames the layerNames to set
	 */
	public void setLayerNames(List<String> layerNames) {
		this.layerNames = layerNames;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	//-------------------------------------------------------------------
	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	//-------------------------------------------------------------------
	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the imageType
	 */
	public ImageType getImageType() {
		return imageType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(ImageType imageType) {
		this.imageType = imageType;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the thumbnail
	 */
	public byte[] getThumbnail() {
		return thumbnail;
	}

	//-------------------------------------------------------------------
	/**
	 * @param thumbnail the thumbnail to set
	 */
	public void setThumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
	}

}

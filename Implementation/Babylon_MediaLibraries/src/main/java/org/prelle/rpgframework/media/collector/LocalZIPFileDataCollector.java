/**
 * 
 */
package org.prelle.rpgframework.media.collector;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.media.Media;

/**
 * @author prelle
 *
 */
public class LocalZIPFileDataCollector implements DataCollector {

	private Logger logger = Logger.getLogger("babylon.media.localzip");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		return url.toExternalForm().startsWith("file:") && url.toExternalForm().toLowerCase().endsWith(".zip");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#preFillMetadata(java.net.URL, org.prelle.rpgframework.media.metadata.SerializableMetadata)
	 */
	@Override
	public Collection<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		logger.warn("Unzip "+url);
		logger.warn("..."+url.getFile()+"    path="+url.getPath());
//		try {
//			ZipFile zip = new ZipFile(url.getFile());
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		return new ArrayList<DataCollector.MediaURL>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
		// TODO Auto-generated method stub

	}

}

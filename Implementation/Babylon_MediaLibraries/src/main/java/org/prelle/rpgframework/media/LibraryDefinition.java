package org.prelle.rpgframework.media;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

import de.rpgframework.media.MediaLibrary;

@Root(name="library")
public class LibraryDefinition {
	@Element
	private String name;
	@Element
	private String url;
	@Attribute
	private boolean enabled;
	@Element
	private String description;
	@Element
	private String login;
	@Element
	private String password;
	
	transient MediaLibrary instance;
	
	public LibraryDefinition() {}

	//-------------------------------------------------------------------
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	//-------------------------------------------------------------------
	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the url
	 */
	public String getURL() {
		return url;
	}

	//-------------------------------------------------------------------
	/**
	 * @param url the url to set
	 */
	public void setURL(String url) {
		this.url = url;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	//-------------------------------------------------------------------
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	//-------------------------------------------------------------------
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
}
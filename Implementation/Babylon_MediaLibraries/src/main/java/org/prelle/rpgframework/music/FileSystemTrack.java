/**
 * 
 */
package org.prelle.rpgframework.music;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.prelle.myid3lib.TaggedFile;
import org.prelle.rpgframework.media.AbstractMedia;

import de.rpgframework.music.Track;
import de.rpgframework.music.UniqueTrackID;

/**
 * @author Stefan
 *
 */
public class FileSystemTrack extends AbstractMedia implements Track {
	
	private Path file;
	private TaggedFile tagged;
	private BabylonUniqueTrackID uniqUD;

	//--------------------------------------------------------------------
	/**
	 */
	public FileSystemTrack(Path file, TaggedFile tagged, BabylonUniqueTrackID id) {
		this.file = file;
		this.tagged = tagged;
		this.uniqUD = id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return file.getFileName().toString();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.music.Track#getIdentifier()
	 */
	@Override
	public UniqueTrackID getIdentifier() {
		return uniqUD;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tagged
	 */
	public TaggedFile getTagged() {
		return tagged;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Media#getCategory()
	 */
	@Override
	public Category getCategory() {
		return Category.MUSIC;
	}

	//-------------------------------------------------------------------
	public byte[] getBytes() {
		try {
			return Files.readAllBytes(file);
		} catch (IOException e) {
			return null;
		}
	}

}

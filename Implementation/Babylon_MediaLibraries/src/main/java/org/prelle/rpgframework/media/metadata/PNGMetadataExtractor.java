/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.apache.log4j.Logger;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.rpgframework.media.LicenseType;
import de.rpgframework.media.RoleplayingMetadata.Category;

/**
 * @author prelle
 *
 */
public class PNGMetadataExtractor implements MetadataExtractor {
	
	private final static String UUID     = "rpgframework:uuid";
	private final static String CATEGORY = "rpgframework:category";
	private final static String ARTIST   = "rpgframework:artist";
	private final static String SERIES   = "rpgframework:series";
	private final static String TITLE    = "rpgframework:title";
	private final static String SOURCE_URL = "rpgframework:sourceURL";
	private final static String COPYRIGHT  = "rpgframework:copyright";
	private final static String LICENSE    = "rpgframework:license";
	private final static String KEYWORDS   = "rpgframework:keywords";
//	private final static String TAGS       = "rpgframework:tags";
	private final static String THUMBNAIL  = "rpgframework:thumbnail";

	private final static Logger logger = Logger.getLogger("babylon.media.meta");
	
	private final static String[] SUFFIX = new String[] {"png"};
	
//	private Gson gson;

	//-------------------------------------------------------------------
	public PNGMetadataExtractor() {
//		gson = new Gson();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#getSupportedSuffixes()
	 */
	@Override
	public List<String> getSupportedSuffixes() {
		return Arrays.asList(SUFFIX);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(Path localFile) {
		try {
			SerializableMetadata media = parseImageMetadata(ImageIO.createImageInputStream(localFile.toFile()));
			/*
			 * If size is unknown, open file
			 */
			if (media.getWidth()==0) {
				logger.debug("  get image size from file "+localFile);
				BufferedImage img = ImageIO.read(localFile.toFile());
				if (img!=null) {
					media.setWidth(img.getWidth());
					media.setHeight(img.getHeight());
				}
			}
			return media;
		} catch (Exception e) {
			logger.error("Failed reading metadata from file "+localFile,e);
			return new SerializableMetadata();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(InputStream in) {
		try {
			return parseImageMetadata(ImageIO.createImageInputStream(in));
		} catch (Exception e) {
			logger.error("Failed reading metadata from file stream",e);
			return new SerializableMetadata();
		}
	}
	
	//-------------------------------------------------------------------
	public SerializableMetadata parseImageMetadata(ImageInputStream input) {
		SerializableMetadata meta = new SerializableMetadata();
		
        try {
            Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
            if (readers==null || !readers.hasNext()) {
            	logger.warn("Cannot get ImageReader from stream");
            	return meta;
            }
            	
            ImageReader reader = readers.next(); // TODO: Validate that there are readers

            reader.setInput(input, true, false);
            
            
            /*
             * Walk through all TextEntries
             */
            IIOMetadataNode root = (IIOMetadataNode) reader.getImageMetadata(0).getAsTree(IIOMetadataFormatImpl.standardMetadataFormatName);
            
            List<String> keywords = new ArrayList<>();
            NodeList entries = root.getElementsByTagName("TextEntry");
            for (int i = 0; i < entries.getLength(); i++) {
                IIOMetadataNode node = (IIOMetadataNode) entries.item(i);
                String key = node.getAttribute("keyword");
                String val = node.getAttribute("value");
                logger.debug("found key "+key);
                try {
					switch (key) {
					case "XML:com.adobe.xmp":
						logger.debug("parse XMP");
						try {
							XMPParser.parseInto(meta, val);
						} catch (SAXException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case ARTIST    : meta.setArtist(val); break;
					case CATEGORY  : meta.setCategory(Category.valueOf(val)); break;
					case COPYRIGHT : meta.setCopyright(val); break;
					case KEYWORDS  : keywords.add(val); break;
					case LICENSE   : meta.setLicense(LicenseType.valueOf(val)); break;
					case SOURCE_URL: meta.setSourceURL(new URL(val)); break;
					case SERIES    : meta.setSeries(val); break;
					case TITLE     : meta.setTitle(val); break;
					case UUID	   : meta.setUUID(java.util.UUID.fromString(val)); break;
					default:
						logger.warn("Unprocessed key: "+key+"="+val);
					}
				} catch (MalformedURLException e) {
					logger.warn("Found invalid URL in PNG metadata: "+val);
				}
            }
            
            meta.setKeywords(keywords);
            
        } catch (IOException e) {
        	logger.error("Failed parsing PNG metadata",e);
		}
        
        return meta;
	}
	
//	//-------------------------------------------------------------------
//	private static String prettyPrint(Node node) {
//		try {
//			TransformerFactory tf = TransformerFactory.newInstance();
//			Transformer transformer = tf.newTransformer();
//			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//
//			StringWriter writer = new StringWriter();
//			transformer.transform(new DOMSource(node), new StreamResult(writer));
//			return writer.toString();
//		} catch (IllegalArgumentException | TransformerFactoryConfigurationError | TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	//-------------------------------------------------------------------
    private static void removeTextEntry(final IIOMetadata metadata, final String key) throws IIOInvalidTreeException {
    	IIOMetadataNode orig = (IIOMetadataNode) metadata.getAsTree(IIOMetadataFormatImpl.standardMetadataFormatName);
    	
        // Remove old nodes with this keyword
        NodeList list = orig.getElementsByTagName("TextEntry");
        for (int i=0; i<list.getLength(); i++) {
        	IIOMetadataNode oldNode = (IIOMetadataNode)list.item(i);
        	if (oldNode.getAttribute("keyword").equals(key)) {
        		oldNode.getParentNode().removeChild(oldNode);
        	}
        }

        metadata.setFromTree(IIOMetadataFormatImpl.standardMetadataFormatName, orig);
    }
	
	//-------------------------------------------------------------------
    private static void replaceTextEntry(final IIOMetadata metadata, final String key, final String value) throws IIOInvalidTreeException {
    	IIOMetadataNode orig = (IIOMetadataNode) metadata.getAsTree(IIOMetadataFormatImpl.standardMetadataFormatName);
    	removeTextEntry(metadata, key);
        // Remove old nodes with this keyword
        NodeList list = orig.getElementsByTagName("TextEntry");
        for (int i=0; i<list.getLength(); i++) {
        	IIOMetadataNode oldNode = (IIOMetadataNode)list.item(i);
        	if (oldNode.getAttribute("keyword").equals(key)) {
        		oldNode.getParentNode().removeChild(oldNode);
        	}
        }
    	
    	// Add new node
        IIOMetadataNode textEntry = new IIOMetadataNode("TextEntry");
        textEntry.setAttribute("keyword", key);
        textEntry.setAttribute("value", value);

        IIOMetadataNode text = new IIOMetadataNode("Text");
        text.appendChild(textEntry);

        orig.appendChild(text);

        metadata.setFromTree(IIOMetadataFormatImpl.standardMetadataFormatName, orig);
    }
	
	//-------------------------------------------------------------------
    private static void addTextEntry(final IIOMetadata metadata, final String key, final String value) throws IIOInvalidTreeException {
        IIOMetadataNode textEntry = new IIOMetadataNode("TextEntry");
        textEntry.setAttribute("keyword", key);
        textEntry.setAttribute("value", value);

        IIOMetadataNode text = new IIOMetadataNode("Text");
        text.appendChild(textEntry);

        IIOMetadataNode root = new IIOMetadataNode(IIOMetadataFormatImpl.standardMetadataFormatName);
        root.appendChild(text);

        metadata.mergeTree(IIOMetadataFormatImpl.standardMetadataFormatName, root);
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeMetadata(org.prelle.rpgframework.media.metadata.SerializableMetadata, java.io.InputStream, java.io.OutputStream)
	 */
	@Override
	public void writeMetadata(SerializableMetadata meta, Path imageFile) throws IOException {
		logger.debug("START writeMetadata");
		String suffix = imageFile.getFileName().toString();
		suffix = suffix.substring(suffix.lastIndexOf(".")+1, suffix.length());
		Path temporarySrc = Files.createTempFile("babylon", suffix);
		// Copy file
		Files.copy(new FileInputStream(imageFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);
		
		try (ImageInputStream input = ImageIO.createImageInputStream(new FileInputStream(temporarySrc.toFile()));
				ImageOutputStream output = ImageIO.createImageOutputStream(new FileOutputStream(imageFile.toFile()))) {

			byte[] thumbnail = MetadataUtil.createThumbnailFromImage(new FileInputStream(temporarySrc.toFile()));

			/*
			 * Read image
			 */
			Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
			if (!readers.hasNext())
				return;
			ImageReader reader = readers.next(); // TODO: Validate that there are readers
			reader.setInput(input, true, false);
			IIOImage image = reader.readAll(0, null);

			/* 
			 * Modify image
			 */
			if (meta.getArtist()   !=null)  replaceTextEntry(image.getMetadata(), ARTIST, meta.getArtist());
			if (meta.getCategory() !=null)  replaceTextEntry(image.getMetadata(), CATEGORY, meta.getCategory().name());
			if (meta.getCopyright()!=null)  replaceTextEntry(image.getMetadata(), COPYRIGHT, meta.getCopyright());
			if (meta.getLicense()  !=null)  replaceTextEntry(image.getMetadata(), LICENSE, meta.getLicense().name());
			if (meta.getSeries()   !=null)  replaceTextEntry(image.getMetadata(), SERIES, meta.getSeries());
			if (meta.getTitle()    !=null)  replaceTextEntry(image.getMetadata(), TITLE, meta.getTitle());
			if (meta.getSourceURL()!=null)  replaceTextEntry(image.getMetadata(), SOURCE_URL, meta.getSourceURL().toExternalForm());
			if (meta.getUUID()     !=null)  replaceTextEntry(image.getMetadata(), UUID, meta.getUUID().toString());
			if (meta.getKeywords() !=null) {
				removeTextEntry(image.getMetadata(), KEYWORDS);
				for (String word : meta.getKeywords()) {
					addTextEntry(image.getMetadata(), KEYWORDS, word);
				}
			}
			replaceTextEntry(image.getMetadata(), THUMBNAIL, Base64.getEncoder().encodeToString(thumbnail));

			/*
			 * Write image
			 */
			ImageWriter writer = ImageIO.getImageWriter(reader); // TODO: Validate that there are writers
			if (writer==null)
				return;
			writer.setOutput(output);
			writer.write(image);
		} catch (IOException e) {
			logger.error("Failed writing metadata",e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeThumbnail(byte[], java.nio.file.Path)
	 */
	@Override
	public void writeThumbnail(byte[] data, Path imageFile) throws IOException {
		logger.debug("START writeThumbnail");
		String suffix = imageFile.getFileName().toString();
		suffix = suffix.substring(suffix.lastIndexOf(".")+1, suffix.length());
		Path temporarySrc = Files.createTempFile("babylon", suffix);
		// Copy file
		Files.copy(new FileInputStream(imageFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);
		
		try (ImageInputStream input = ImageIO.createImageInputStream(new FileInputStream(temporarySrc.toFile()));
				ImageOutputStream output = ImageIO.createImageOutputStream(new FileOutputStream(imageFile.toFile()))) {
			
			/*
			 * Read image
			 */
			Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
			if (!readers.hasNext())
				return;
			ImageReader reader = readers.next(); // TODO: Validate that there are readers
			reader.setInput(input, true, false);
			IIOImage image = reader.readAll(0, null);

			/* 
			 * Modify image
			 */
			replaceTextEntry(image.getMetadata(), THUMBNAIL, Base64.getEncoder().encodeToString(data));

			/*
			 * Write image
			 */
			ImageWriter writer = ImageIO.getImageWriter(reader); // TODO: Validate that there are writers
			if (writer==null)
				return;
			writer.setOutput(output);
			writer.write(image);
		} catch (IOException e) {
			logger.error("Failed writing metadata",e);
		}
	}
	
	//-------------------------------------------------------------------
	private static byte[] readThumbnailFromMetadataNonStandard(InputStream in) {
        try (ImageInputStream input = ImageIO.createImageInputStream(in)) {
            Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
            if (readers==null || !readers.hasNext()) {
            	logger.warn("Cannot get ImageReader from stream");
            	return null;
            }
            	
            ImageReader reader = readers.next(); // TODO: Validate that there are readers
            reader.setInput(input, true, false);
            
            
            /*
             * Walk through all TextEntries
             */
            IIOMetadataNode root = (IIOMetadataNode) reader.getImageMetadata(0).getAsTree(IIOMetadataFormatImpl.standardMetadataFormatName);

            NodeList entries = root.getElementsByTagName("TextEntry");
            for (int i = 0; i < entries.getLength(); i++) {
            	IIOMetadataNode node = (IIOMetadataNode) entries.item(i);
            	String key = node.getAttribute("keyword");
            	String val = node.getAttribute("value");
            	switch (key) {
            	case THUMBNAIL : return Base64.getDecoder().decode(val);
            	default:
            	}
            }
        } catch (IOException e) {
        	logger.error("Failed parsing PNG metadata",e);
		}
        
        return null;

	}
	
	//-------------------------------------------------------------------
	private static byte[] readThumbnailFromMetadata(InputStream in) {
        try (ImageInputStream input = ImageIO.createImageInputStream(in)) {
        	// Find suitable metadata reader
        	Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
            if (!readers.hasNext()) {
            	in.close();
            	return null;
            }
            ImageReader reader = readers.next();
            
            // Tell the reader to process image with metadata
            reader.setInput(input, true, false);
		
            // Check if image has thumbnails at all
            if (!reader.hasThumbnails(0)) {
            	reader.dispose();
            	in.close();
            	return null;
            }
            
            BufferedImage scaled = reader.readThumbnail(0, 0);
//            return scaled;
			BufferedImage newBufferedImage = new BufferedImage(scaled.getWidth(),
					scaled.getHeight(), BufferedImage.TYPE_INT_RGB);
			  newBufferedImage.createGraphics().drawImage(scaled, 0, 0, Color.WHITE, null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write( newBufferedImage, "jpg", baos );
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			return imageInByte;
		} catch (IOException e) {
			logger.warn("Failed obtaining thumbnail: "+e);
		}
        return null;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readThumbnail(java.io.InputStream)
	 */
	@Override
	public byte[] readThumbnail(Path localFile) {
		logger.debug("readThumbnail");
		try {
			byte[] data = readThumbnailFromMetadata(new FileInputStream(localFile.toFile()));
			if (data!=null)
				return data;
			data = readThumbnailFromMetadataNonStandard(new FileInputStream(localFile.toFile()));
			if (data!=null)
				return data;
			
			return null;
		} catch (IOException e) {
			logger.warn("Failed obtaining thumbnail: "+e);
		} finally {
		}
        return null;
	}

}

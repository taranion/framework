/**
 * 
 */
package org.prelle.rpgframework.media.collector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.media.Media;

/**
 * @author prelle
 *
 */
public class PatreonCollector implements DataCollector {
	
	class ObjectRef {
		String type;
		String id;
		public String toString() {
			return type+"="+id;
		}
	}
	
	class Attachments {
		ObjectRef[] data;		
	}
	
	class Relationships {
		Attachments attachments;		
		Attachments user_defined_tags;
	}
	
	class FileAndURL {
		String name;
		String url;
	}
	
	class Attributes {
		FileAndURL post_file;
		String title;
		String name;
		String url;
		String full_name;
	}
	
	class PatreonData {
		String id;
		Attributes attributes;
		Relationships relationships;
	}
	
	class PatreonIncludes {
		
	}
	
	class PatreonResponse {
		PatreonData data;
		PatreonData[] included;
	}
	
	private Logger logger = Logger.getLogger("babylon.media.patreon");

	final static Pattern PATTERN = Pattern.compile("http[s]*://www.patreon.com/posts/([^?]*)?.*");
	
	final static Pattern FILE_PATTERN = Pattern.compile("http[s]*://www.patreon.com/file?([^\"]*)\"[^>]*>([^<])<.*");
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		return PATTERN.matcher(url.toString()).matches();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#preFillMetadata(java.net.URL, java.lang.String[])
	 */
	@Override
	public Collection<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		logger.debug("  START: prefill from "+url);
		List<MediaURL> ret = new ArrayList<>();

		try {
			Matcher matcher = PATTERN.matcher(url.toString());
			if (!matcher.matches())
				return new ArrayList<DataCollector.MediaURL>();
			String postAndName  = matcher.group(1);
			try {
				url = new URL("https://www.patreon.com/post/"+postAndName);
				preMeta.setSourceURL(url);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}


			try {
				String postID = postAndName.substring(postAndName.lastIndexOf('-')+1);
				URL apiURL = new URL("http://api.patreon.com/posts/"+postID);

				logger.debug("  Query "+apiURL);
				HttpURLConnection con = (HttpURLConnection) apiURL.openConnection();
				con.addRequestProperty("User-Agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0");
				//			con.addRequestProperty("Authorization", "Client-ID 0225f282f47a7bd");
				HttpURLConnection.setFollowRedirects(false);
				logger.trace("  Code "+con.getResponseCode());
				if (con.getResponseCode()==302) {
					logger.warn("Moved temporarily to "+con.getHeaderField("Location"));
					return new ArrayList<DataCollector.MediaURL>();
				}
				if (con.getResponseCode()==301) {
					logger.warn("Moved permanently to "+con.getHeaderField("Location"));
					return new ArrayList<DataCollector.MediaURL>();
				}
				if (con.getResponseCode()!=200) {
					logger.warn(convertStreamToString(con.getInputStream()));
					return new ArrayList<DataCollector.MediaURL>();
				}

				String json = convertStreamToString(con.getInputStream());
				logger.debug(json);
				PatreonResponse post = (new Gson()).fromJson(json, PatreonResponse.class);

				preMeta.setTitle(post.data.attributes.title);
				preMeta.setArtist(post.data.attributes.full_name);
				/*
				 * Find attachments
				 */
				if (post.data.relationships.attachments!=null) {
					// Walk through attachments
					for (ObjectRef ref : post.data.relationships.attachments.data) {
						logger.trace("  Attached "+ref.id);
						for (PatreonData incl : post.included) {
							if (incl.id.equals(ref.id)) {
								logger.info("    found "+incl.attributes.name+" at "+incl.attributes.url);
								url = new URL(incl.attributes.url);
								MediaURL toAdd = new MediaURL(url, incl.attributes.name);
								ret.add(toAdd);
							}
						}
					}
				} else
					throw new IllegalArgumentException("No attachments found - possibly patreons only?");
				/* 
				 * Walk through tags
				 */
				List<String> tags = new ArrayList<>();
				if (post.data.relationships.user_defined_tags!=null) {
					for (ObjectRef ref : post.data.relationships.user_defined_tags.data) {
						if (ref.type.equals("post_tag")) {
							if (ref.id.startsWith("user_defined;")) {
								tags.add(ref.id.substring("user_defined;".length()));
								logger.debug("  added tag: "+ref.id.substring("user_defined;".length()));
							} else
								logger.warn("  Unknown tag: "+ref);
						}
					}
					if (!tags.isEmpty())
						preMeta.setKeywords(tags);
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			logger.debug("  STOP : prefill from "+url);
		}
		return ret;
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("resource")
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
	}

}

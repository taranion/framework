/**
 * 
 */
package org.prelle.rpgframework.media;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import de.rpgframework.media.ImageMedia;
import de.rpgframework.media.Media;
import de.rpgframework.media.MediaLibrary;
import de.rpgframework.media.RoleplayingMetadata.Category;

/**
 * @author prelle
 *
 */
public class HttpMediaLibrary extends AbstractMediaLibrary implements MediaLibrary {

	private URI baseURI;
	private Path dbFile;

	//-------------------------------------------------------------------
	public HttpMediaLibrary(LibraryDefinition libDef, URI uri) throws IOException, SQLException {
		super(libDef);
		this.baseURI = uri;
		
		String id = libDef.getName().replace(' ', '_').replace('/', '_').replace('\\', '_');
		logger = Logger.getLogger("babylon.media."+id);
		
		URI dbURI = uri.resolve("database.hsql");
		logger.info("db URI =  "+dbURI);
		
		ReadableByteChannel rbc = Channels.newChannel(dbURI.toURL().openStream());
		dbFile = Files.createTempFile("salomon", "database.hsql");
		FileChannel.open(dbFile, StandardOpenOption.WRITE, StandardOpenOption.DELETE_ON_CLOSE).transferFrom(rbc, 0, Long.MAX_VALUE);
		logger.info("Downloaded to "+dbFile);
		
		con = DriverManager.getConnection("jdbc:hsqldb:file:"+dbFile, "SA", "");
	}

	//-------------------------------------------------------------------
	protected void openDatabase() {
		super.openDatabase();
		for (AbstractMedia media : knownMedia.values()) {
			try {
				URI fullURI = baseURI.resolve(URLEncoder.encode(media.getRelativePath().toString(), "UTF-8"));
				try {
					media.setAccessURL(fullURI.toURL());
				} catch (MalformedURLException e) {
					logger.error("Error converting to URL: "+fullURI+" : "+e);
				}
			} catch (UnsupportedEncodingException e) {
				logger.error("Encoding error when converting to URL: "+media.getRelativePath()+" : "+e);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getCategories()
	 */
	@Override
	public Collection<Category> getCategories() {
		logger.warn("TODO: getCategories()");
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getMedia(de.rpgframework.media.Media.Category)
	 */
	@Override
	public List<Media> getMedia(Category value) {
		// TODO Auto-generated method stub
		logger.warn("TODO: getMedia(Category)");
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#isReadOnly()
	 */
	@Override
	public boolean isReadOnly() {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getBytes(de.rpgframework.media.Media)
	 */
	@Override
	public byte[] getBytes(Media media) {
		logger.warn("TODO: getBytes("+media+")");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getInputStream(de.rpgframework.media.Media)
	 */
	@Override
	public InputStream getInputStream(Media media) {
		try {
			URL url = new URL(baseURI.toString()+"/"+media.getRelativePath());
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			if (con.getResponseCode()!=200)			
				return null;
			return con.getInputStream();
		} catch (IOException e) {
			logger.error("Failed obtaining inputstream for "+media,e);
			return null;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaLibrary#getThumbnail(de.rpgframework.media.ImageMedia)
	 */
	@Override
	public byte[] getThumbnail(ImageMedia img) {
//		try {
//			URL url = new URL(baseURI.toString()+"/"+img.getRelativePath());
//			HttpURLConnection con = (HttpURLConnection)url.openConnection();
//			if (con.getResponseCode()!=200)			
//				return null;
//			return con.getInputStream();
//		} catch (IOException e) {
//			logger.error("Failed obtaining inputstream for "+media,e);
			return null;
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaLibrary#getLocalPath(de.rpgframework.media.Media)
	 */
	@Override
	public Path getLocalPath(Media media) {
		return null;
	}

}

package org.prelle.rpgframework.media.collector;

import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;

import org.prelle.rpgframework.media.AbstractMedia;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.media.Media;

public interface DataCollector {
	
	public class MediaURL {
		public URL url;
		public String filename;
		public Path localFile;
		public SerializableMetadata meta;
		public AbstractMedia media;
		public MediaURL(URL url, String fname) {
			this.url = url;
			this.filename = fname;
		}
		public String toString() {
			return url+"("+filename+")";
		}
	}
	
	public boolean matches(URL url);
	
	public Collection<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta);
	
	public void fillMetadata(URL url, Media media);
	
}
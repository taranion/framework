/**
 * 
 */
package org.prelle.rpgframework.media.collector;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.media.Media;

/**
 * @author prelle
 *
 */
public class OpenGraphDataCollector implements DataCollector {

	private Logger logger = Logger.getLogger("babylon.media.opengraph");

	final static Pattern PATTERN = Pattern.compile("http[s]*://.*");
	final static Pattern LINE_PATTERN1 = Pattern.compile(".*property=\"([^\"]*)\".*content=\"([^\"]*)\".*");
	final static Pattern LINE_PATTERN2 = Pattern.compile(".*name=\"([^\"]*)\".*content=\"([^\"]*)\".*");
	
//	private Gson gson = new GsonBuilder().setPrettyPrinting().create();


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#matches(java.net.URL)
	 */
	@Override
	public boolean matches(URL url) {
		logger.debug("check for "+url+" = "+PATTERN.matcher(url.toString()).matches());
		return PATTERN.matcher(url.toString()).matches();
	}

	//-------------------------------------------------------------------
	private String[] getPropertyAndContent(String line) {
		Matcher matcher = LINE_PATTERN1.matcher(line);
		if (matcher.matches()) {
			return new String[] {matcher.group(1), matcher.group(2)};
		} 
		matcher = LINE_PATTERN2.matcher(line);
		if (matcher.matches()) {
			return new String[] {matcher.group(1), matcher.group(2)};
		} else
			logger.debug("  failed to find attributes in: "+line);
		
		return new String[2];
	}

	//-------------------------------------------------------------------
	private void process(String key, String value, SerializableMetadata preMeta) {
		switch (key) {
		case "og:title":
		case "title":
			logger.debug("  Found title: "+value);
			preMeta.setTitle(value); break;
		case "keywords": 
			logger.debug("  Found keywords: "+value);
			List<String> words = new ArrayList<>();
			StringTokenizer tok = new StringTokenizer(value,",");
			while (tok.hasMoreTokens()) {
				words.add(tok.nextToken().trim());
			}
			preMeta.setKeywords(words);
			break;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#preFillMetadata(java.net.URL, java.lang.String[])
	 */
	@Override
	public List<MediaURL> preFillMetadata(URL url, SerializableMetadata preMeta) {
		
			try {
				logger.debug("  Query "+url);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				logger.trace("  Code "+con.getResponseCode());
				if (con.getResponseCode()!=200) {
					return new ArrayList<>();
				}
				
				// Attempt one: SAX
				XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		        try {
		            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(con.getInputStream());
		            while(xmlEventReader.hasNext()){
		                XMLEvent xmlEvent = xmlEventReader.nextEvent();
		               if (xmlEvent.isStartElement()){
		                   StartElement startElement = xmlEvent.asStartElement();
		                   if(startElement.getName().getLocalPart().equals("meta")){
		                	   Attribute nameAttr = startElement.getAttributeByName(new QName("name"));
		                	   Attribute propAttr = startElement.getAttributeByName(new QName("property"));
		                	   Attribute contAttr = startElement.getAttributeByName(new QName("content"));
		                	   if (logger.isTraceEnabled())
		                		   logger.trace("..."+nameAttr+"...."+propAttr+"...."+contAttr);
		                	   if (nameAttr!=null && contAttr!=null)
		                		   process(nameAttr.getValue(), contAttr.getValue(), preMeta);
		                	   else if (propAttr!=null && contAttr!=null)
			                		  process(propAttr.getValue(), contAttr.getValue(), preMeta);
		                   }
		               }
		            }
		        } catch (Exception e) {
		        	logger.warn("Parsing failed - invalid HTML: "+e);
//					return preFillMetadataAlternative(url, preMeta);
		        }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new ArrayList<>();
	}
	
	//-------------------------------------------------------------------
	private List<MediaURL> preFillMetadataAlternative(URL url, SerializableMetadata preMeta) {
		
		try {
			logger.debug("  Query "+url);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setConnectTimeout(2000);
			logger.trace("  Code "+con.getResponseCode());
			if (con.getResponseCode()!=200) {
				return new ArrayList<>();
			}
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//			URL url2 = null;
//			String filename = null;
			while (true) {
				String line = in.readLine();
				if (line==null)
					break;
				int index = line.toLowerCase().indexOf("<meta");
				if (index>-1) {
					int end = line.indexOf('>', index);
					if (end<index)
						continue;
					String[] pair = getPropertyAndContent(line.substring(index, end));
					if (pair[0]==null)
						continue;
					logger.debug(pair[0]+" = "+pair[1]);
					switch (pair[0]) {
					case "og:description": preMeta.setTitle(pair[1]); break;
					case "og:title": if (preMeta.getTitle()==null) preMeta.setTitle(pair[1]); break;
					case "og:image:height": preMeta.setHeight(Integer.parseInt(pair[1])); break;
					case "og:image:width": preMeta.setWidth(Integer.parseInt(pair[1])); break;
//					case "og:image": 
//						if (url.toExternalForm().endsWith("/")) {
//							url2 = new URL(pair[1]);
//							filename = url2.getPath();
//							if (filename.lastIndexOf('/')>=0)
//								filename = filename.substring(filename.lastIndexOf('/')+1);
//						}
//						break;
//					case "og:url": 
//						url2 = new URL(pair[1]);
//						filename = url2.getPath();
//						if (filename.lastIndexOf('/')>=0)
//							filename = filename.substring(filename.lastIndexOf('/')+1);
//						break;
					case "pinterestapp:source": preMeta.setSourceURL(new URL(pair[1])); break;
					case "keywords": 
						List<String> words = new ArrayList<>();
						StringTokenizer tok = new StringTokenizer(pair[1],",");
						while (tok.hasMoreTokens()) {
							words.add(tok.nextToken().trim());
						}
						preMeta.setKeywords(words);
						break;
					}
				}
			}
//			if (url2!=null)
//				return Arrays.asList(new MediaURL(url2, filename));
//			else
				return new ArrayList<>();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<>();
}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("resource")
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.collector.DataCollector#fillMetadata(java.net.URL, de.rpgframework.media.Media)
	 */
	@Override
	public void fillMetadata(URL url, Media media) {
	}

}

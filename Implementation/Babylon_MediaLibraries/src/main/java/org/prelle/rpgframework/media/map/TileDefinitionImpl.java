/**
 * 
 */
package org.prelle.rpgframework.media.map;

import java.nio.file.Path;

import org.prelle.rpgframework.media.ImageMediaImpl;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.media.map.TileDefinition;
import de.rpgframework.media.map.TileType;

/**
 * @author prelle
 *
 */
public class TileDefinitionImpl extends ImageMediaImpl implements TileDefinition {
	
	private TileType tileType;
	private boolean geomorph;

	//-------------------------------------------------------------------
	public TileDefinitionImpl(Path file) {
		super(file, Category.MAPTILE);
	}

	//-------------------------------------------------------------------
	public TileDefinitionImpl(SerializableMetadata meta) {
		super(meta);
		tileType = meta.getTileType();
		geomorph = meta.isGeomorph();
		category = Category.MAPTILE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.map.TileDefinition#getTileType()
	 */
	@Override
	public TileType getTileType() {
		return tileType;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.map.TileDefinition#setTileType(de.rpgframework.media.map.TileType)
	 */
	@Override
	public void setTileType(TileType tileType) {
		this.tileType = tileType;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.map.TileDefinition#isGeomorph()
	 */
	@Override
	public boolean isGeomorph() {
		return geomorph;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.map.TileDefinition#setGeomorph(boolean)
	 */
	@Override
	public void setGeomorph(boolean geomorph) {
		this.geomorph = geomorph;
	}

}

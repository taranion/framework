/**
 * 
 */
package org.prelle.rpgframework.media.map;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.prelle.rpgframework.media.AbstractMedia;

import de.rpgframework.media.Media;
import de.rpgframework.media.MediaCollection;
import de.rpgframework.media.map.TileDefinition;
import de.rpgframework.media.map.Tileset;

/**
 * @author prelle
 *
 */
public class TilesetImpl extends AbstractMedia implements Tileset, MediaCollection<TileDefinition> {
	
	private transient Path baseDir;
	private List<UUID> elements;

	//-------------------------------------------------------------------
	public TilesetImpl(Path baseDir) {
		this.baseDir = baseDir;
		super.category = Category.TILESET;
		elements = new ArrayList<UUID>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaCollection#getLocalPath()
	 */
	@Override
	public Path getLocalPath() {
		return baseDir;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<TileDefinition> iterator() {
		List<TileDefinition> ret = new ArrayList<>();
		for (UUID key : elements) {
			Media media = library.getMedia(key);
			if (media instanceof TileDefinition)
				ret.add( (TileDefinition)media );
		}
		return ret.iterator();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaCollection#add(de.rpgframework.media.Media)
	 */
	@Override
	public void add(TileDefinition element) {
		if (!elements.contains(element.getUUID()))
			elements.add(element.getUUID());
	}

}

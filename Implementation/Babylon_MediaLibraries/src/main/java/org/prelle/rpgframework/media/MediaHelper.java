/**
 * 
 */
package org.prelle.rpgframework.media;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.media.metadata.JCodecVideoMetadataExtractor;
import org.prelle.rpgframework.media.metadata.MetadataUtil;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.media.ImageMedia;
import de.rpgframework.media.ImageType;
import de.rpgframework.media.Media;
import de.rpgframework.media.MediaTag;
import de.rpgframework.media.TagRegistry;
import de.rpgframework.media.map.TileDefinition;

/**
 * @author prelle
 *
 */
public class MediaHelper {

	private final static Logger logger = Logger.getLogger("babylon.media");
	
	static class CachedThumbnail {
		Instant lastAccess;
		byte[] data;
	}
	
	private static Map<UUID, CachedThumbnail> thumbnails = new HashMap<>();

	//-------------------------------------------------------------------
	public static String marshal(List<MediaTag> tags) {
		List<String> toJoin = new ArrayList<>();
		for (MediaTag tag : tags) {
			toJoin.add( (tag.getClass().getSimpleName()+":"+tag.getName()).toLowerCase() );
		}
		return String.join(",", toJoin);
	}

	//-------------------------------------------------------------------
	public List<MediaTag> unmarshal(String data) {
		List<MediaTag> tags = new ArrayList<>();
		for (String pairS : data.split(",")) {
			String[] pair = pairS.split(":");
			// Walk through all registered tags and compare with key
			Class<? extends MediaTag> found = null;
			for (Class<? extends MediaTag> cls : TagRegistry.getKnownTagTypes()) {
				if (cls.getSimpleName().toLowerCase().equals(pair[0])) {
					found = cls;
					break;
				}
			}
			if (found==null) {
				logger.warn("Unknown media tag: "+pairS);
				continue;
			}

			System.err.println("MediaHelper.unmarshal: TODO: find value of '"+pair[1]+"' in class "+found);
		}

		return tags;
	}

	//-------------------------------------------------------------------
	public static Media createMediaFromFile(Path file) throws IOException {
		logger.debug("populate metadata from "+file);

		String filename = file.getFileName().toString().toLowerCase();
		return populateMetadataFromFile(new FileInputStream(file.toFile()), filename);
	}

	//-------------------------------------------------------------------
	public static Media populateMetadataFromFile(InputStream ins, String filename) throws IOException {
		/*
		 * Switch depending on assumed content type
		 */
		if (filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".png")) {
			// Image
			SerializableMetadata meta = MetadataUtil.readMetadata(filename, ins, null);
			ImageMediaImpl media = new ImageMediaImpl(meta);
			return media;
		} else if (filename.endsWith(".gif")) {
			// Image
			SerializableMetadata meta = MetadataUtil.readMetadata(filename, ins, null);
			ImageMediaImpl media = new ImageMediaImpl(meta);
			return media;
		} else if (filename.endsWith(".mp3") || filename.endsWith(".ogg")) {
			// Audio
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void writeMetadataToFile(Media media, Path destFile) throws IOException {
		SerializableMetadata meta = new SerializableMetadata();
		meta.setUUID(media.getUUID());
		meta.setCategory(media.getCategory());
		meta.setArtist(media.getArtist());
		meta.setSeries(media.getSeries());
		meta.setTitle(media.getName());
		meta.setSourceURL(media.getSource());
		meta.setLicense(media.getLicense());
		meta.setCopyright(media.getCopyright());
		meta.setKeywords(media.getKeywords());
		meta.setTags(media.getTags());

		// Make a temporary copy
		Path tmpCopy = destFile.getParent().resolve(destFile.getFileName()+".copy");
		Files.copy(new FileInputStream(destFile.toString()), tmpCopy, StandardCopyOption.REPLACE_EXISTING);

		if (media instanceof ImageMedia) {
			// Image
			ImageMedia image = (ImageMedia)media;
			meta.setDPI(image.getDPI());
			if (image instanceof TileDefinition) {
//				meta.setTileType( ((TileDefinition)image).getTileType());
				// Geomorph
			}
			
			
			MetadataUtil.writeMetadata(meta, destFile);
		} else
			logger.error("Writing "+media.getClass()+" is not supported");
	}
	
	//-------------------------------------------------------------------
	public static BufferedImage scale(BufferedImage imageToScale, int dWidth, int dHeight) {
        BufferedImage scaledImage = null;
        if (imageToScale != null) {
            scaledImage = new BufferedImage(dWidth, dHeight, imageToScale.getType());
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
            graphics2D.dispose();
        }
        return scaledImage;
    }
	
//	//-------------------------------------------------------------------
//	public static byte[] readThumbnail(InputStream in) {
//		try {
//			ImageMetadata metadata = Imaging.getMetadata(in, "no-clue");
//			if (metadata!=null) {
//				if (metadata instanceof JpegImageMetadata) {
//					JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
//					if (jpegMetadata.getEXIFThumbnailData().length>0) {
//						return jpegMetadata.getEXIFThumbnailData();
//					}
//				}
//			} 
//		} catch (ImageReadException | IOException e) {
//			logger.error("Error reading image data",e);
//		}
//		return null;
//	}
	
	//-------------------------------------------------------------------
	public static byte[] scaleAsThumbnailImage(InputStream in) {
		try {
			BufferedImage img = ImageIO.read(in);
			if (img==null)
				throw new NullPointerException("No image in stream");
			BufferedImage scaled = scale(img, 150, 150);
			BufferedImage newBufferedImage = new BufferedImage(scaled.getWidth(),
					scaled.getHeight(), BufferedImage.TYPE_INT_RGB);
			  newBufferedImage.createGraphics().drawImage(scaled, 0, 0, Color.WHITE, null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write( newBufferedImage, "jpg", baos );
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
//			logger.debug("Return "+imageInByte.length+" bytes");
			return imageInByte;
		} catch (IOException e) {
			logger.error("Failed scaling to thumbail: "+e);
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public static byte[] scaleAsThumbnailVideo(Path path) {
//		byte[] buf = JCodecVideoMetadataExtractor.readThumbnail(path.toFile());
//		ByteArrayInputStream bais = new ByteArrayInputStream(buf);
		try {
			BufferedImage img = ImageIO.read(path.toFile());
			if (img==null)
				throw new NullPointerException("No image in stream");
			BufferedImage scaled = scale(img, 150, 150);
			BufferedImage newBufferedImage = new BufferedImage(scaled.getWidth(),
					scaled.getHeight(), BufferedImage.TYPE_INT_RGB);
			newBufferedImage.createGraphics().drawImage(scaled, 0, 0, Color.WHITE, null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write( newBufferedImage, "jpg", baos );
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			//		logger.debug("Return "+imageInByte.length+" bytes");
			return imageInByte;
	} catch (IOException e) {
		logger.error("Failed scaling to thumbail: "+e);
	}
	return null;
	}

	//-------------------------------------------------------------------
	public static byte[] getThumbnail(ImageMedia item) {
		CachedThumbnail cache = thumbnails.get(item.getUUID());
		if (cache!=null) {
			cache.lastAccess = Instant.now();
			return cache.data;
		}
		logger.debug("getThumbnail("+item.getRelativePath()+")");
		
		
		cache = new CachedThumbnail(); 
		try {
			cache.data = MetadataUtil.getThumbnail(item.getRelativePath().getFileName().toString(), item.getLibrary().getLocalPath(item));
			if (cache.data==null && (item.getLibrary() instanceof LocalFSMediaLibrary)) {
				// Generate a new thumbnail
				LocalFSMediaLibrary lib = (LocalFSMediaLibrary)item.getLibrary();
				Path absPath = lib.getBaseDirectory().resolve(item.getRelativePath());
				try {
					logger.warn("No thumbnail in metadata - generate and store one from "+absPath.toFile());
					if (item.getType()==null || item.getType()==ImageType.STATIC) {
						cache.data = scaleAsThumbnailImage(new FileInputStream(absPath.toFile()));
						if (cache.data!=null)
							MetadataUtil.writeThumbnail(cache.data, absPath);
					} else if (item.getType()==ImageType.VIDEO) {
						cache.data = scaleAsThumbnailVideo(absPath);
//						if (cache.data!=null)
//							MetadataUtil.writeThumbnail(cache.data, absPath);
					}
				} catch (FileNotFoundException e) {
					logger.error("Error generating thumbnail: "+e);
				}
			}
		} catch (NullPointerException e) {
			logger.error("No embedded image in "+item.getRelativePath());
		} catch (Exception e) {
			logger.error("Error generating thumbnail for "+item.getRelativePath()+": "+e,e);
		}
		cache.lastAccess = Instant.now();
		thumbnails.put(item.getUUID(), cache);
		
//		if (item.getType()==ImageType.VIDEO || item.getRelativePath().toString().endsWith(".m4v")) {
//			logger.fatal("STOP HERE");
//			System.exit(0);
//		}
		
		return cache.data;
	}
	
}

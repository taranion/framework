/**
 * 
 */
package foo.sm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import javax.swing.SwingUtilities;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.media.ImageMediaImpl;
import org.prelle.rpgframework.media.LibraryDefinition;
import org.prelle.rpgframework.media.LocalFSMediaLibrary;

import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.media.ImageMedia;
import de.rpgframework.media.LicenseType;
import de.rpgframework.media.Media;
import de.rpgframework.media.RoleplayingMetadata.Category;
import de.rpgframework.media.Webserver;
import de.rpgframework.media.map.Battlemap;
import de.rpgframework.media.map.TileDefinition;
import de.rpgframework.media.map.Tileset;
import javafx.embed.swing.JFXPanel;

/**
 * @author prelle
 *
 */
public class LocalFSLibraryTest {
	
	private final static Logger logger = Logger.getLogger("test");
	
	private static Path dir;
	private LocalFSMediaLibrary library;
	private Webserver web;

	//-------------------------------------------------------------------
	protected static void setupJavaFX() throws RuntimeException {
		final CountDownLatch latch = new CountDownLatch(1);
		try {
			SwingUtilities.invokeAndWait(() -> {
				new JFXPanel(); // initializes JavaFX environment
				latch.countDown();
			});
		} catch (InvocationTargetException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@SuppressWarnings("unchecked")
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		BasicConfigurator.configure();
		Enumeration<Appender> app = Logger.getRootLogger().getAllAppenders();	
		while (app.hasMoreElements()) {
			Appender a = app.nextElement();
			a.setLayout(new PatternLayout("%5p [%c] (%F:%L) - %m%n"));
		}
		Logger.getLogger("xml").setLevel(Level.WARN);
		Logger.getLogger("hsqldb").setLevel(Level.WARN);
		Logger.getLogger("babylon.session").setLevel(Level.WARN);
		dir = Files.createTempDirectory("babylon-medialib-test");
		logger.debug("Created "+dir);
		
//		setupJavaFX();
	}

	//-------------------------------------------------------------------
	private static void deleteDirContent(Path value) throws Exception {
		for (Path child : Files.newDirectoryStream(value)) {
			if (Files.isDirectory(child))
				deleteDirContent(child);
			Files.delete(child);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (Files.isDirectory(dir))
			deleteDirContent(dir);
		Files.deleteIfExists(dir);
		logger.debug("Deleted "+dir);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		LibraryDefinition libDef = new LibraryDefinition();
		libDef.setName("Testlibrary");
		library = new LocalFSMediaLibrary(libDef, dir);
		web = RPGFrameworkLoader.getInstance().getWebserver();
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		((LocalFSMediaLibrary)library).close();
	}

	//-------------------------------------------------------------------
//	@Test
	public void testLocalFile1() throws Exception {
		assertNotNull(web);
		
		Path directPath = dir.resolve("tile.img");
		ImageMediaImpl media = new ImageMediaImpl(directPath, Category.HANDOUT);
		media.setUUID(UUID.randomUUID());
		media.setLicense(LicenseType.UNKNOWN);
		library.internalAddMedia(media);
		
		assertNotNull(media.getAccessURL());
		assertEquals("http://192.168.0.2:6789/Testlibrary/tile.img",media.getAccessURL().toExternalForm());
		
		web.toURL(media.getRelativePath());
	}

}

/**
 * 
 */
package org.prelle.rpgframework.sessioncore;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.products.AdventureImpl;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventType;
import de.rpgframework.core.Group;
import de.rpgframework.core.Player;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.core.SessionContext;
import de.rpgframework.core.SessionService;
import de.rpgframework.products.Adventure;


/**
 * @author prelle
 *
 */
public class SessionServiceImpl implements SessionService {

	private final static Logger logger = Logger.getLogger("babylon.session");

	private final static String GROUP_FILENAME = "group.xml";
	private final static String ADVENTURE_FILENAME = "adventure.xml";
	
	private ConfigContainerImpl configRoot;
	
	/**
	 * Pointer to local directory containing adventure data
	 */
	private Path localAdvBaseDir;
	/**
	 * Pointer to local directory containing groups and sessions
	 */
	private Path localSessBaseDir;
	/**
	 * Pointer to remote directory to sync data with
	 */
	//	private Path remoteBaseDir;

//	private List<Adventure> adventures;
	private List<Group> groups;
	
	private Serializer marshaller;

	//--------------------------------------------------------------------
	/**
	 */
	public SessionServiceImpl(ConfigContainerImpl configRoot) throws IOException {
		this.configRoot = configRoot;
		marshaller = new Persister();
		groups     = new ArrayList<>();
//		adventures = new ArrayList<>();

		String dataDir = configRoot.getOption(RPGFramework.PROP_DATADIR).getStringValue();
		/*
		 *  Add player specific path
		 */
		localAdvBaseDir = FileSystems.getDefault().getPath(dataDir, "adventures");
		logger.info("Expect adventure data at "+localAdvBaseDir);

		// Ensure that directory exists
		try {
			Files.createDirectories(localAdvBaseDir);
		} catch (IOException e) {
			logger.fatal("Could not create player directory: "+e);
			throw e;
		}

		/*
		 *  Add group and session specific path
		 */
		localSessBaseDir = FileSystems.getDefault().getPath(dataDir, "groups");
		logger.info("Expect group and session data at "+localSessBaseDir);

		// Ensure that directory exists
		try {
			Files.createDirectories(localSessBaseDir);
		} catch (IOException e) {
			logger.fatal("Could not create group and session directory: "+e);
			throw e;
		}

	}

	//--------------------------------------------------------------------
	public void initialize() {
		if (RPGFrameworkLoader.getCallback()!=null) RPGFrameworkLoader.getCallback().message("Load characters and groups");
		// Load all data
//		Thread thread = new Thread(new Runnable() {
//			public void run() {
//				try {
//					Thread.sleep(200);
//				} catch (InterruptedException e) {
//					logger.error("Cannot sleep",e);
//				}
				logger.debug("---Load data------");
//				loadAdventures();
				loadCharacters();
				loadGroups();
//			}
//		}, "SessionServiceImpl");
//		thread.start();
		
		logger.debug("Init session screen");
	}

//	//--------------------------------------------------------------------
//	private Path getDefaultDataDir() {
//		String os   = System.getProperty("os.name");
//
//		if (os.equalsIgnoreCase("Linux"))
//			return FileSystems.getDefault().getPath(System.getProperty("user.home"), ".local", "share", "rpgframework");
//		else if (os.equalsIgnoreCase("windows"))
//			return FileSystems.getDefault().getPath(System.getProperty("user.home"), "ApplicationData", "rpgframework");
//		else
//			return FileSystems.getDefault().getPath(System.getProperty("user.home"), "rpgframework");
//	}
//
//	//--------------------------------------------------------------------
//	private void loadAdventures() {
//		logger.debug("START loadAdventures");
//		adventures = new ArrayList<Adventure>();
//		try {
//			DirectoryStream<Path> dir = Files.newDirectoryStream(localAdvBaseDir);
//			for (Path ruleDir : dir) {
//				if (!Files.isDirectory(ruleDir))
//					continue;
//				// Expect sub directory name to be a rulesystem
//				String rulesName = ruleDir.getFileName().toString();
//				RoleplayingSystem rules;
//				try {
//					rules = RoleplayingSystem.valueOf(rulesName.toUpperCase());
//				} catch (IllegalArgumentException e) {
//					logger.warn("Ignore adventures of unknown rulesystem '"+rulesName+"'");
//					continue;
//				}
//
//				/*
//				 * Now load all adventures within rule directory
//				 */
//				logger.debug("Load "+rules+" adventures from "+ruleDir);
//				for (Path advPath : Files.newDirectoryStream(ruleDir)) {
//					if (!Files.isDirectory(advPath))
//						continue;
//					logger.debug("Found adventure directory "+advPath);
//					Path path = advPath.resolve(ADVENTURE_FILENAME);
//					if (!Files.exists(path)) {
//						logger.warn("Ignore adventure with missing description: "+path);
//						continue;
//					}
//
//					try {
//						AdventureImpl adv = marshaller.read(AdventureImpl.class, new FileReader(path.toFile()));
//						adv.setBaseDirectory(advPath);
//						
//						/*
//						 * Load images
//						 */
//						for (LocalizedAdventureContent content : adv.getContent()) {
//							String filename = "cover_"+content.getLanguage()+".img";
//							Path filePath = advPath.resolve(filename);
//							try {
//								FileInputStream fin = new FileInputStream(filePath.toFile());
//								byte[] imgData = new byte[fin.available()];
//								fin.read(imgData);
//								fin.close();
//								logger.debug("Set cover for "+content.getLanguage()+": "+filePath);
//								content.setCover(imgData);
//							} catch (IOException e) {
//								logger.error("Failed reading cover image "+filePath+": "+e);
//							}
//						}
//						
//						
//						adventures.add(adv);
//						logger.info("Found adventure "+adv);
//					} catch (Exception e) {
//						logger.error("Failed reading adventure file "+path+": "+e);
//						System.exit(0);
//					}
//
//				}
//			}
//		} catch (IOException e) {
//			logger.error("Failed to load all adventures",e);
//		}
//		logger.debug("STOP  loadAdventures");
//	}

	//--------------------------------------------------------------------
	private void loadCharacters() {
		logger.debug("START loadCharacters");
		try {
			for (Player player : RPGFrameworkLoader.getInstance().getPlayerService().getPlayers()) {
				for (CharacterHandle handle : RPGFrameworkLoader.getInstance().getSessionManagement().getCharacterService().getCharacters(player)) {
					logger.debug("  loaded "+handle.getName());
				}
			}
		} catch (IOException e) {
			logger.error("Error loading players",e);
		}
		logger.debug("STOP  loadCharacters");
	}

	//--------------------------------------------------------------------
	private void loadGroups() {
		logger.debug("START loadGroups");
		groups = new ArrayList<Group>();
		try {
			DirectoryStream<Path> dir = Files.newDirectoryStream(localSessBaseDir);
			for (Path groupDir : dir) {
				if (!Files.isDirectory(groupDir))
					continue;
				// Expect sub directory name to be a group name
				String groupName = groupDir.getFileName().toString();
				// Load data into group
				Path path = groupDir.resolve(GROUP_FILENAME);
				if (!Files.exists(path)) {
					logger.warn("Ignore group with properties missing: "+path);
					continue;
				}
				
				logger.fatal("Read group "+path);
				try {
					GroupImpl group = marshaller.read(GroupImpl.class, new FileReader(path.toFile()));
					if (!group.getName().equals(groupName)) {
						logger.warn(path+" is expected to be in a directory named "+groupName);
					}
					group.setInternalData(this, groupDir, path);
					group.setLastChange(Files.getLastModifiedTime(path).toInstant());
					// Add links from session to group
					SessionContext lastSession = null;
					for (SessionContext sess : group.getAdventures()) {
						((SessionContextImpl)sess).setGroup(group);
						for (Player tmp : group) {
							if (sess.getCharacter(tmp)==null)
								logger.warn("No character for "+tmp.getName()+" in session "+sess.getAdventure()+" in group "+group.getName());
						}
						lastSession = sess;
						if (sess.getAdventure()==null) {
							logger.warn("No adventure set in session of group "+group.getName());
						}
					}
					// Add a session time
					if (lastSession!=null && lastSession.getLastTime()==null)
						lastSession.setLastTime(Files.getLastModifiedTime(path).toInstant());
					
					groups.add(group);
					logger.info("Found group "+group);
				} catch (Exception e) {
					logger.error("Failed reading group file "+path,e);
				}
			}
		} catch (IOException e) {
			logger.error("Failed to load all groups",e);
		}
		logger.debug("STOP  loadGroups");
//		logger.fatal("STOP HERE");
//		System.exit(0);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.SessionService#addPlayer(org.prelle.rpgtool.base.Group, org.prelle.rpgtool.base.Player)
	 */
	@Override
	public void addPlayer(Group group, Player player) throws IOException {
		logger.info("Added "+player+" to group "+group);
		((GroupImpl)group).add(player);
		EventBus.fireEvent(this, EventType.GROUP_CHANGED, group);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.SessionService#removePlayer(org.prelle.rpgtool.base.Group, org.prelle.rpgtool.base.Player)
	 */
	@Override
	public void removePlayer(Group group, Player player) throws IOException {
		((GroupImpl)group).remove(player);
		logger.info("Removed "+player+" from group "+group);
		EventBus.fireEvent(this, EventType.GROUP_CHANGED, group);
	}

	//--------------------------------------------------------------------
	public void saveGroup(Group grp) throws IOException {
		logger.info("saveGroup("+grp+")");
		for (Player tmp : grp) {
			logger.debug("Member "+tmp);
		}

		GroupImpl group = (GroupImpl)grp;

		/*
		 * Ensure the group directory exists
		 */
		Path grpDir = ((GroupImpl)group).getGroupDir();
		if (!Files.exists(group.getGroupDir())) {
			logger.debug("Creating group directory "+grpDir);
			try {
				Files.createDirectories(grpDir);
			} catch (IOException e) {
				logger.error("Failed creating group directory",e);
				throw e;
			}
		}

		// Build a property string for members
//		StringBuffer members = new StringBuffer();
//		Iterator<Player> it = group.iterator();
//		while (it.hasNext()) {
//			members.append(it.next().getName());
//			if (it.hasNext())
//				members.append("|");
//		}
		
		/*
		 * Store group details in properties files
		 */
		Path groupFile = group.getGroupFile();
		try {
			logger.debug("Storing group properties in "+groupFile);
			marshaller.write(group, groupFile.toFile());
		} catch (Exception e) {
			logger.error("Failed saving group",e);
			throw new IOException(e);
		}
//		Properties props = ((GroupImpl)group).getAsProperties();
//		props.put("members", members.toString());
//		props.store(new FileWriter(groupFile.toFile()), "UTF-8");

		EventBus.fireEvent(this, EventType.GROUP_CHANGED, group);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.SessionService#createGroup(java.lang.String)
	 */
	@Override
	public Group createGroup(String title) throws IOException {
		Path grpPath = localSessBaseDir.resolve(title);
		Path grpFile = grpPath.resolve(GROUP_FILENAME);
		GroupImpl group = new GroupImpl(this, title, grpPath);
		group.setGroupFile(grpFile);
		logger.info("Created still unsaved group "+group);
		
		EventBus.fireEvent(this, EventType.GROUP_ADDED, group);
		return group;

		//		if (!Files.exists(grpPath)) {
		//			logger.info("Creating group directory "+grpPath);
		//			try {
		//				Files.createDirectories(grpPath);
		//			} catch (IOException e) {
		//				logger.error("Failed creating group directory",e);
		//				throw e;
		//			}
		//		}
		//
		//		return new GroupImpl(this, title, grpPath);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.SessionService#createSession(org.prelle.rpgtool.base.Group, de.rpgframework.products.rpgtool.base.Adventure)
	 */
	@Override
	public SessionContext createSession(Group group, Adventure adv) throws IOException {
		SessionContextImpl session = new SessionContextImpl();
		session.setGroup(group);
		session.setAdventure(adv);
		return session;
	}

	//--------------------------------------------------------------------
	public void saveAdventure(Adventure grp) throws IOException {
		logger.info("saveAdventure("+grp+")");

		AdventureImpl real = (AdventureImpl)grp;

		/*
		 * Ensure the group directory exists
		 */
		Path advDir = ((AdventureImpl)real).getBaseDirectory();
		if (!Files.exists(advDir)) {
			logger.debug("Creating adventure directory "+advDir);
			try {
				Files.createDirectories(advDir);
			} catch (IOException e) {
				logger.error("Failed creating adventure directory",e);
				throw e;
			}
		}
		logger.debug("advDir = "+advDir);

		// Build a property string for members
//		StringBuffer members = new StringBuffer();
//		Iterator<Player> it = group.iterator();
//		while (it.hasNext()) {
//			members.append(it.next().getName());
//			if (it.hasNext())
//				members.append("|");
//		}
		
		/*
		 * Store details in properties files
		 */
		Path descFile = real.getBaseDirectory().resolve(ADVENTURE_FILENAME);
		try {
			logger.debug("Storing adventure description in "+descFile);
			marshaller.write(real, descFile.toFile());
		} catch (Exception e) {
			logger.error("Failed saving adventure",e);
			throw new IOException(e);
		}
		
		/*
		 * Store cover images
		 */
//		for (LocalizedAdventureContent local : real.getContent()) {
//			if (local.getCover()==null || local.getCover().length==0)
//				continue;
//			String filename = "cover_"+local.getLanguage()+".img";
//			Path filePath = real.getBaseDirectory().resolve(filename);
//			try {
//				FileOutputStream fout = new FileOutputStream(filePath.toFile());
//				fout.write(local.getCover());;
//				fout.flush();
//				fout.close();
//				logger.debug("Wrote "+filePath);
//			} catch (IOException e) {
//				logger.error("Failed writing cover image to "+filePath+": "+e);
//			}
//		}
		
//		Properties props = ((GroupImpl)group).getAsProperties();
//		props.put("members", members.toString());
//		props.store(new FileWriter(groupFile.toFile()), "UTF-8");

		EventBus.fireEvent(this, EventType.ADVENTURE_CHANGED, grp);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.SessionService#getGroups()
	 */
	@Override
	public List<Group> getGroups() throws IOException {
		List<Group> ret = new ArrayList<Group>(groups);
		Collections.sort(ret);

		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.SessionService#getSessions(org.prelle.rpgtool.base.Group)
	 */
	@Override
	public List<SessionContext> getSessions(Group group) throws IOException {
		return group.getAdventures();
	}

	//--------------------------------------------------------------------
	boolean renameGroup(GroupImpl group, String newName) {
		Path groupDir = group.getGroupDir();
		Path newDir   = groupDir.getParent().resolveSibling(newName);
		try {
			if (Files.exists(groupDir)) {
				logger.info("Moving group directory from "+groupDir+" to "+newDir);
				Files.move(groupDir, newDir);
			}
			return true;
		} catch (IOException e) {
			logger.error("Renaming group directory failed",e);
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionService#runSession(de.rpgframework.core.SessionContext)
	 */
	@Override
	public void runSession(SessionContext session) {
		// TODO Auto-generated method stub
		logger.warn("TODO: play session "+session);
		
		/*
		 * Switch genre
		 */
		Adventure adv = session.getAdventure();
		logger.debug("Adventure to run = "+adv);
		if (adv!=null) {
			RoleplayingSystem system = adv.getRules();
			if (system!=null) {
				// Found a rule system
				EventBus.fireEvent(session, EventType.RULESYSTEM_CHANGED, system);
				// Default to genres of rule system
				EventBus.fireEvent(session, EventType.GENRE_CHANGED, (Object[])system.getGenre());
			}
			// 
			EventBus.fireEvent(session, EventType.GROUP_SELECTED, session.getGroup());
			// Default to genres of rule system
			EventBus.fireEvent(session, EventType.SESSION_STARTED, session);
			//
			EventBus.fireEvent(session, EventType.ADVENTURE_STARTED, adv);
		}
	}

}

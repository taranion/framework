/**
 *
 */
package org.prelle.rpgframework.webserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;

import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventType;
import de.rpgframework.media.MediaLibrary;
import de.rpgframework.media.Webserver;

/**
 * @author prelle
 *
 */
public class WebserverImpl implements Runnable, Webserver {

	public final static int WEBSERVER_PORT = 6789;

	private final static Logger logger = Logger.getLogger("babylon.webserver");

	private Server server;
	private HandlerList handlers;

	/** Local, Remote */
	private Map<Path, Path> mappings;
	private Path tmpBaseDir;

	//-------------------------------------------------------------------
	public WebserverImpl() {
		server = new Server(WEBSERVER_PORT);
		mappings = new HashMap<>();

		// Configure ResourceHandler
		ResourceHandler resource_handler = new ResourceHandler();
//		resource_handler.setDirectoriesListed(true);  // Jetty 9.4
		resource_handler.setWelcomeFiles(new String[] { "index.html" });

		try {
			tmpBaseDir = Files.createTempDirectory("salomon");
			tmpBaseDir.toFile().deleteOnExit();
			mappings.put(tmpBaseDir, Paths.get("/"));
			resource_handler.setResourceBase(tmpBaseDir.toString());
		} catch (IOException e) {
			logger.fatal("Failed setting base resource directory",e);
		}

		// Add the ResourceHandler to the server.
		handlers = new HandlerList();
		handlers.setHandlers(new Handler[] { resource_handler });
		server.setHandler(handlers);

	}

	//-------------------------------------------------------------------
	public void start() {
		// Start the server
		Thread thread = new Thread(this, "WebServer");
		thread.start();
		
		// Inform about started webserver
		EventBus.fireEvent(this, EventType.WEBSERVER_STARTED, this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			logger.info("\n\n\nStart webserver\n\n");
			server.start();
			server.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Webserver#addDirectory(java.nio.file.Path, java.nio.file.Path)
	 */
	@Override
	public void addDirectory(Path localDir, Path mountPoint) {
		logger.info("Add directory "+localDir+" as "+mountPoint);

		ResourceHandler resource_handler = new ResourceHandler();
//		resource_handler.setDirectoriesListed(true);  // Jetty 9.4
		resource_handler.setWelcomeFiles(new String[] { "index.html" });
		resource_handler.setResourceBase(localDir.toString());

		handlers.addHandler(resource_handler);

		mappings.put(localDir, mountPoint);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Webserver#addFile(java.nio.file.Path, java.nio.file.Path)
	 */
	@Override
	public void addFile(Path localFile, Path mountPoint) {
		logger.info("Add local file "+localFile+" as "+mountPoint);

		ResourceHandler resource_handler = new ResourceHandler();
//		resource_handler.setDirectoriesListed(true);  // Jetty 9.4
		resource_handler.setWelcomeFiles(new String[] { "index.html" });
		resource_handler.setResourceBase(localFile.toString());

		handlers.addHandler(resource_handler);

		mappings.put(localFile, mountPoint);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Webserver#resolve(java.nio.file.Path)
	 */
	@Override
	public Path resolve(Path accessPath) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Webserver#toURL(java.nio.file.Path)
	 */
	@Override
	public URL toURL(Path localPath) {
		logger.warn("toURL "+localPath);

		/*
		 * Get local IP
		 */
		InetAddress localIP = null;
		try {
			localIP = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			logger.error("Failed to detect local IP: "+e);
			return null;
		}
		String baseURL = "http://"+localIP.getHostAddress()+":"+WEBSERVER_PORT;
		logger.info("baseURL = "+baseURL);
		logger.info("mappings= "+mappings);

		/*
		 * Detect external path
		 */
		for (Entry<Path, Path> entry : mappings.entrySet()) {
			//			logger.debug("* Entry "+entry.getKey()+" = "+entry.getValue());
			//			logger.info("   entry path as URI = "+entry.getValue().toUri());
			if (entry.getKey().equals(localPath))
				try {
					boolean isRoot = entry.getValue().getParent().equals(entry.getValue().getRoot());
					if (!isRoot) {
						logger.debug("Root = "+entry.getValue().getRoot()+" // "+entry.getValue().getRoot().toUri());
						logger.debug("Parent = "+entry.getValue().getParent()+" // "+entry.getValue().getParent().toUri());
						logger.debug("FS = "+entry.getValue().getFileSystem().getRootDirectories());
						logger.debug("Key "+entry.getKey().toUri());
						logger.debug("Val "+entry.getValue().toUri());
					}
					String local = entry.getValue().toUri().toString().substring(entry.getValue().getRoot().toUri().toString().length());
					URL retVal = new URL(baseURL+(isRoot?"/":"")+local);
					logger.debug("toURL("+localPath+") returns "+retVal);
					return retVal;
				} catch (Exception e) {
					logger.error("Failed to build URL",e);
					return null;
				}

			//			if (logger.isTraceEnabled())
			//				logger.debug("  Compare "+entry.getKey()+" with "+localPath+" = "+entry.getKey().relativize(localPath).toUri()+" // "+localPath.startsWith(entry.getKey()));
			if (localPath.startsWith(entry.getKey())) {
				logger.debug("localPath = "+localPath);
				
				//				logger.trace("  Found "+localPath);
				//				Path relPath = entry.getKey().relativize(localPath);
				try {
					//					logger.info("   Relative path = "+relPath);
					//					logger.info("   Relative path as URI = "+relPath.toUri());
					//					logger.info("   Local path as URI = "+localPath.toUri());
					//					logger.info("   entry path as URI = "+entry.getKey().toUri());
					//					logger.info("   result = "+localPath.toUri().toString().substring(entry.getKey().toUri().toString().length()));
					String local = localPath.toUri().toString().substring(entry.getKey().toUri().toString().length());
					logger.debug("local = "+local);
					String all = baseURL+"/"+entry.getValue()+"/"+local;
					URL retVal = new URL(URLEncoder.encode(all, "UTF-8"));
					logger.debug("toURL("+localPath+") returns "+retVal);
					return retVal;
				} catch (Exception e) {
					logger.error("Failed to build URL",e);
					return null;
				}
			}
		}

//		logger.fatal("Stop here");
//		System.exit(0);
		logger.debug("toURL("+localPath+") returns null");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.Webserver#getTempBaseDirectory()
	 */
	@Override
	public Path getTempBaseDirectory() {
		return tmpBaseDir;
	}

}

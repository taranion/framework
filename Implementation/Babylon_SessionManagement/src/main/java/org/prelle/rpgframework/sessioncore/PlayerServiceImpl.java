/**
 * 
 */
package org.prelle.rpgframework.sessioncore;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.RPGFrameworkImpl;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.addressbook.InfoType;
import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventType;
import de.rpgframework.core.Player;
import de.rpgframework.core.PlayerService;
import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.VCardVersion;

/**
 * @author prelle
 *
 */
public class PlayerServiceImpl implements PlayerService {

	private final static Logger logger = Logger.getLogger("babylon.player");

	private ConfigContainer configRoot;
	private ConfigOption<String> cfgDataDir;
	
	/**
	 * Pointer to local directory containing player data
	 */
	private Path localBaseDir;
	/**
	 * Pointer to remote directory to sync data with
	 */
//	private Path remoteBaseDir;
	
	private Player myself;
	private List<Player> allPlayers;
	
	//--------------------------------------------------------------------
	public PlayerServiceImpl(ConfigContainer configRoot) throws IOException {
		this.configRoot = configRoot;
		
		prepareConfigNode();
		
		allPlayers = new ArrayList<Player>();
		
		String dataDir = cfgDataDir.getStringValue();
		
		// Add player specific path
		localBaseDir = FileSystems.getDefault().getPath(dataDir, "player");
		logger.info("Expect player data at "+localBaseDir);
		
		// Ensure that directory exists
		try {
			Files.createDirectories(localBaseDir);
		} catch (IOException e) {
			logger.fatal("Could not create player directory: "+e);
			throw e;
		}
		
	}
	
	//------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void prepareConfigNode() {
		cfgDataDir = (ConfigOption<String>) configRoot.getOption(RPGFrameworkImpl.PROP_DATADIR);
	}
	
	//------------------------------------------------------------------
	public void initialize() {
		if (RPGFrameworkLoader.getCallback()!=null) RPGFrameworkLoader.getCallback().message("Load players");
		try {
			loadPlayers();

			// Get or generate myself as player
			Path playerDir = FileSystems.getDefault().getPath(localBaseDir.toString(), "myself");
			if (!Files.exists(playerDir)) {
				Files.createDirectories(playerDir);
			}
			logger.debug("My characters are located at "+playerDir);

			myself = getPlayer("myself");
			if (myself==null) {
				myself = createPlayer();
				((PlayerImpl)myself).setDirectory(playerDir);
				savePlayer(myself);
			}
		} catch (Exception e) {
			logger.fatal("Could not initialize player service",e);
			if (RPGFrameworkLoader.getCallback()!=null)
				RPGFrameworkLoader.getCallback().errorOccurred("PlayerService", "Could not initialize player service", e);
			System.exit(0);
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.PlayerService#getMyself()
	 */
	@Override
	public Player getMyself() {
		return myself;
	}
	
//	//--------------------------------------------------------------------
//	private Path getDefaultDataDir() {
//		String os   = System.getProperty("os.name");
//
//		if (os.equalsIgnoreCase("Linux"))
//			return FileSystems.getDefault().getPath(System.getProperty("user.home"), ".local", "share", "rpgframework");
//		else if (os.equalsIgnoreCase("windows"))
//			return FileSystems.getDefault().getPath(System.getProperty("user.home"), "ApplicationData", "rpgframework");
//		else
//			return FileSystems.getDefault().getPath(System.getProperty("user.home"), "rpgframework");
//	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.PlayerService#createPlayer()
	 */
	@Override
	public Player createPlayer() {
		Player created = new PlayerImpl(new VCard());
		allPlayers.add(created);
		EventBus.fireEvent(this, EventType.PLAYER_ADDED, created);
		return created;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.PlayerService#deletePlayer(org.prelle.rpgtool.base.Player)
	 */
	@Override
	public void deletePlayer(Player player) throws IOException {
		PlayerImpl real = (PlayerImpl)player;
		allPlayers.remove(player);
		
		logger.info("Delete player file "+real.getFile());
		Files.delete(real.getFile());

	}

	//--------------------------------------------------------------------
	/**
	 * Load all players from storage
	 */
	private void loadPlayers() throws IOException {
		if (allPlayers!=null && !allPlayers.isEmpty())
			throw new IllegalStateException("Already loaded");
		
		if (allPlayers==null)
			allPlayers = new ArrayList<Player>(); 

		DirectoryStream<Path> list = Files.newDirectoryStream(localBaseDir, new Filter<Path>(){
			@Override
			public boolean accept(Path path) throws IOException {
				return Files.isDirectory(path);
//				return path.toString().endsWith(".vcf");
			}});
		
		for (Path path : list) {
			logger.debug("try load "+path);
//			String filename = path.getFileName().toString();
//			String name = filename.substring(0, filename.lastIndexOf('.'));
			Path vcardFile = path.resolve("contact.vcf");
			logger.debug("try load "+vcardFile);
			
			
			VCard vcard = null;
			if (vcardFile.toFile().exists()) {
				vcard = Ezvcard.parse(new FileReader(vcardFile.toFile())).first();
			} else {
				logger.warn("No VCARD "+vcard+" in player directory "+path);
				vcard = new VCard();
			}
			if (vcard==null) {
				logger.error(path+" is no readable Vcard");
				vcard = new VCard();
			}

			PlayerImpl player = new PlayerImpl(vcard, vcardFile, path);
			player.setDirectory(path);
			if (path.endsWith("myself")) {
				player.set(InfoType.NICKNAME, "myself");
			} 
			allPlayers.add(player);
			logger.info("Loaded player "+player.getDisplayName());
		}
		
		Collections.sort(allPlayers);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.PlayerService#getPlayers()
	 */
	@Override
	public List<Player> getPlayers() throws IOException {
		return new ArrayList<Player>(allPlayers);
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.PlayerService#getPlayer(java.lang.String)
	 */
	public Player getPlayer(String search) throws IOException {
		for (Player player : allPlayers) {
			if (((PlayerImpl)player).getId().equals(search))
				return player;
			if (((PlayerImpl)player).getName().equals(search))
				return player;
			if (((PlayerImpl)player).getDisplayName().equals(search))
				return player;
		}
		
		DirectoryStream<Path> list = Files.newDirectoryStream(localBaseDir, new Filter<Path>(){
			@Override
			public boolean accept(Path path) throws IOException {
				return path.toString().endsWith(".vcf");
			}});
		
		for (Path path : list) {
			String filename = path.getFileName().toString();
			String name = filename.substring(0, filename.lastIndexOf('.'));
			VCard vcard = Ezvcard.parse(new FileReader(path.toFile())).first();
			if (vcard==null) {
				logger.error("Failed reading VCard "+path);
				return null;
			}
			PlayerImpl player = new PlayerImpl(vcard);
			player.setFile(path);
			
			if (name.equals(search))
			 return player;
		}
		
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.PlayerService#savePlayer(org.prelle.rpgtool.base.Player)
	 */
	@Override
	public void savePlayer(Player player) throws IOException {
		Path dir  = ((PlayerImpl)player).getDirectory();
		Path file = ((PlayerImpl)player).getFile();
		logger.warn("TODO: save player  "+file);
		
		// Create directory
		if (dir==null) {
			dir = FileSystems.getDefault().getPath(localBaseDir.toString(), player.getName());
			logger.debug("Create player directory "+dir);
			((PlayerImpl)player).setDirectory(file);
			// Create player directory
			Files.createDirectories(dir);
			// Create character directory
			Files.createDirectories(dir.resolve("characters"));
		}
		if (file==null) {
			file = FileSystems.getDefault().getPath(dir.toString(), "contact.vcf");
			((PlayerImpl)player).setFile(file);
		}
		Ezvcard.write(((PlayerImpl)player).getVCard()).version(VCardVersion.V4_0).go(file.toFile());
		
		EventBus.fireEvent(this, EventType.PLAYER_CHANGED, player);
	}

}

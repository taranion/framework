/**
 * 
 */
package org.prelle.rpgframework.charprov;

import java.nio.file.Path;

import org.prelle.rpgframework.character.BaseCharacterHandleLight;

import de.rpgframework.character.CharacterHandleFull;
import de.rpgframework.core.Player;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class BaseCharacterHandle extends BaseCharacterHandleLight implements CharacterHandleFull {
	
	private Player owner;

	//--------------------------------------------------------------------
	/**
	 */
	public BaseCharacterHandle(Path path, Player owner, RoleplayingSystem rules) {
		super(path, rules);
		this.owner = owner;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.CharacterHandleFull.character.CharacterHandle#getOwner()
	 */
	@Override
	public Player getOwner() {
		return owner;
	}

}

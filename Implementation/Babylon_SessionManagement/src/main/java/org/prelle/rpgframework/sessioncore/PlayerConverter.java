package org.prelle.rpgframework.sessioncore;

import java.util.NoSuchElementException;

import javax.sql.rowset.serial.SerialException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.apache.log4j.Logger;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

import de.rpgframework.RPGFrameworkLoader;

//-------------------------------------------------------------------
public class PlayerConverter implements XMLElementConverter<PlayerImpl>, StringValueConverter<PlayerImpl> {

	private final static  Logger logger = Logger.getLogger("babylon");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(PlayerImpl value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public PlayerImpl read(String id) throws Exception {
		PlayerImpl player = (PlayerImpl) RPGFrameworkLoader.getInstance().getPlayerService().getPlayer(id);
		if (player!=null) {
			return player;
		}
		throw new NoSuchElementException("No player named '"+id+"' found");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Converter#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, PlayerImpl value) throws Exception {
		node.setAttribute("idref", write(value));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Converter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public PlayerImpl read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		logger.trace("resolve "+ev);
		Attribute idAttr = ev.getAttributeByName(new QName("idref"));
		if (idAttr==null)
			throw new SerialException("Missing attribute 'idref' in "+ev);
		String id = idAttr.getValue();
		
		return read(id);
	}
	
}
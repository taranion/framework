/**
 * 
 */
package org.prelle.rpgframework.device;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventBusListener;
import de.rpgframework.core.EventType;
import de.rpgframework.core.SessionContext;
import de.rpgframework.devices.DeviceService;
import de.rpgframework.devices.RPGToolDevice;
import de.rpgframework.devices.DeviceFunction;

/**
 * @author prelle
 *
 */
public class DeviceServiceImpl implements DeviceService, EventBusListener {
	
	protected Logger logger = Logger.getLogger("babylon.device");
	
	private List<RPGToolDevice> devices;

	//-------------------------------------------------------------------
	/**
	 */
	public DeviceServiceImpl() {
		logger.debug("<init>");
		devices = new ArrayList<RPGToolDevice>();
		devices.add(new LocalMediaServer());
		
		EventBus.registerBusEventListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.DeviceService#getAvailableDevices()
	 */
	@Override
	public List<RPGToolDevice> getAvailableDevices() {
		return new ArrayList<RPGToolDevice>(devices);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.DeviceService#getAvailableDevices(de.rpgframework.devices.RPGToolDevice.DeviceFunction)
	 */
	@Override
	public List<RPGToolDevice> getAvailableDevices(DeviceFunction types) {
		logger.debug("getAvailableDevices("+types+") on "+devices);
		List<RPGToolDevice> ret = new ArrayList<RPGToolDevice>();
		for (RPGToolDevice tmp : devices) {
			logger.debug("search "+tmp.getName()+" = "+tmp.getSupportedFunctions()+" for "+types);
			if (tmp.getSupportedFunctions().contains(types)) {
				ret.add(tmp);
			}
		}
		
		Collections.sort(ret);
		logger.debug("  returns "+ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.DeviceService#getAvailableDevices(de.rpgframework.core.SessionContext, de.rpgframework.devices.RPGToolDevice.DeviceFunction)
	 */
	@Override
	public List<RPGToolDevice> getAvailableDevices(SessionContext context,
			DeviceFunction types) {
		// TODO Auto-generated method stub
		logger.warn("TODO: implement getAvailableDevices(SessionContext, RPGToolDeviceType)");
		return getAvailableDevices(types);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.EventBusListener#handleEvent(java.lang.Object, de.rpgframework.core.EventType, java.lang.Object[])
	 */
	@Override
	public void handleEvent(Object src, EventType type, Object... values) {
		switch (type) {
		case DEVICE_APPEARED:
			logger.info("A device appeared: "+Arrays.toString(values));
			RPGToolDevice toAdd = (RPGToolDevice) values[0];
			logger.debug("  functions = "+toAdd.getSupportedFunctions());
			devices.add(toAdd);
			break;
		case DEVICE_DISAPPEARED:
			logger.info("A device disappeared: "+Arrays.toString(values));
			RPGToolDevice toRemove = (RPGToolDevice) values[0];
			devices.remove(toRemove);
			break;
		default:
		}
	}

}

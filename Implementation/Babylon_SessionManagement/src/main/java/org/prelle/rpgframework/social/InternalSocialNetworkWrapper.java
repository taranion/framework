/**
 * 
 */
package org.prelle.rpgframework.social;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.api.InternalSocialNetwork;
import org.prelle.rpgframework.api.InternalSocialNetworkCallback;

import de.rpgframework.ConfigContainer;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.addressbook.Contact;
import de.rpgframework.addressbook.InfoType;
import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventType;
import de.rpgframework.core.Player;
import de.rpgframework.social.Friend;
import de.rpgframework.social.OnlineService;
import de.rpgframework.social.OnlineService.Feature;
import de.rpgframework.social.SocialNetworkProvider;

/**
 * @author prelle
 *
 */
public class InternalSocialNetworkWrapper implements SocialNetworkProvider, InternalSocialNetworkCallback {

	private final static Logger logger = Logger.getLogger("babylon");

	private List<InternalSocialNetwork> networks;
	private List<Friend> allContacts;

	//-----------------------------------------------------------------
	/**
	 */
	public InternalSocialNetworkWrapper(ConfigContainer config) {
		networks = new ArrayList<InternalSocialNetwork>();
		allContacts  = new ArrayList<Friend>();

		Iterator<InternalSocialNetwork> it = ServiceLoader.load(InternalSocialNetwork.class).iterator();
		while (it.hasNext()) {
			try {
				InternalSocialNetwork intern = it.next();
				logger.info("Found social network "+intern.getClass());
				networks.add(intern);
			} catch (Throwable e) {
				logger.fatal("Failed loading InternalSocialNetwork",e);
			}
		}

		try {
			ConfigContainer social = config.createContainer("social");
			
			/*
			 * Initialize all networks
			 */
			for (InternalSocialNetwork network : networks) {
				logger.warn("--------------init "+network+"-------------");
				network.initialize(social, this);
			}
		} catch (Exception e) {
			logger.fatal("Failed setting up social networks",e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#confirmSubscription(de.rpgframework.addressbook.Contact)
	 */
	@Override
	public boolean confirmSubscription(Contact arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#getContacts()
	 */
	@Override
	public List<Friend> getFriends() {
		return allContacts;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#getOnlineServices()
	 */
	@Override
	public Collection<OnlineService> getOnlineServices() {
		return new ArrayList<OnlineService>(networks);
	}

	@Override
	public boolean requestSubscription(Contact arg0, Map<String, String> arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Contact> search(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#setUsedService(de.rpgframework.social.OnlineService.Feature, de.rpgframework.social.OnlineService)
	 */
	@Override
	public void setUsedService(Feature feature, OnlineService service) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#bindPlayer(de.rpgframework.social.Friend, de.rpgframework.core.Player)
	 */
	@Override
	public void bindPlayer(Friend friend, Player player) {
		// TODO Auto-generated method stub
		logger.warn("TODO: bind friend "+friend+" to player "+player);
		
		URI     uri = friend.getIdentifier();
		String user = friend.getUser();
 		logger.info("uri  = "+uri);
 		logger.info("user = "+user);
 		
 		player.addID(uri);
 		try {
			RPGFrameworkLoader.getInstance().getPlayerService().savePlayer(player);
		} catch (IOException e) {
			logger.error("Failed saving player",e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#unbindPlayer(de.rpgframework.social.Friend, de.rpgframework.core.Player)
	 */
	@Override
	public void unbindPlayer(Friend friend, Player player) {
		// TODO Auto-generated method stub
		logger.warn("TODO: Unbind friend "+friend+" from player "+player);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.api.InternalSocialNetworkCallback#addedFriend(de.rpgframework.social.Friend)
	 */
	@Override
	public void addedFriend(Friend friend) {
		if (!allContacts.contains(friend))
			allContacts.add(friend);
		EventBus.fireEvent(this, EventType.SOCIAL_FRIEND_FOUND, friend);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.api.InternalSocialNetworkCallback#updatedFriend(de.rpgframework.social.Friend)
	 */
	@Override
	public void updatedFriend(Friend friend) {
		EventBus.fireEvent(this, EventType.SOCIAL_FRIEND_UPDATED, friend);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#getBoundPlayer(de.rpgframework.social.Friend)
	 */
	@Override
	public Player getBoundPlayer(Friend contact) {
		logger.debug("getBoundPlayer for "+contact.getIdentifier()+"   user="+contact.getUser());
		try {
			/*
			 * Consult IDs
			 */
			for (Player player : RPGFrameworkLoader.getInstance().getPlayerService().getPlayers()) {
				if (player.containsID(contact.getIdentifier())) 
					return player;
			}
			/*
			 * Consult first and last names
			 */
			for (Player player : RPGFrameworkLoader.getInstance().getPlayerService().getPlayers()) {
				if (player.get(InfoType.FIRSTNAME)==null) continue;
				if (player.get(InfoType.LASTNAME )==null) continue;
				boolean fnMatch = player.get(InfoType.FIRSTNAME).equals(contact.get(InfoType.FIRSTNAME));
				boolean lnMatch = player.get(InfoType.LASTNAME ).equals(contact.get(InfoType.LASTNAME ));
				if (fnMatch && lnMatch) {
					logger.debug("Guess that "+player+" matches "+contact+" by name");
					return player;
				}
			}
		} catch (IOException e) {
			logger.error("Failed accessing players",e);
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.SocialNetworkProvider#getBoundFriends(de.rpgframework.core.Player)
	 */
	@Override
	public List<Friend> getBoundFriends(Player player) {
		List<Friend> ret = new ArrayList<Friend>();
		outer:
		for (URI uri : player.getIDs()) {
			logger.debug(" is "+uri+" a known friend?");
			for (Friend friend : allContacts) {
				if (uri.equals(friend.getIdentifier())) {
					ret.add(friend);
					continue outer;
				}
			}
			
		}
		return ret;
	}

}

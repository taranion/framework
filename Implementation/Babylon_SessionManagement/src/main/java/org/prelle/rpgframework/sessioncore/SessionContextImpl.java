/**
 * 
 */
package org.prelle.rpgframework.sessioncore;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.products.AdventureConverter;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementConvert;
import org.prelle.simplepersist.MapConvert;
import org.prelle.simplepersist.Root;

import de.rpgframework.character.CharacterHandleFull;
import de.rpgframework.core.Group;
import de.rpgframework.core.Player;
import de.rpgframework.core.SessionContext;
import de.rpgframework.products.Adventure;

/**
 * @author prelle
 *
 */
@Root(name="session")
public class SessionContextImpl implements SessionContext {

	private final static Logger logger = Logger.getLogger("babylon");

//	private transient SessionServiceImpl service;
//	private transient Path groupDir;
//	private transient Path groupFile;
	
	private Instant lastTime;
	
	private Group group;
	@Element(name="adventure")
	@ElementConvert(AdventureConverter.class)
	private Adventure adventureResolved;
	@Element
	@MapConvert(keyConvert=PlayerConverter.class,valConvert=CharacterHandleConverter.class)
	private Map<Player, CharacterHandleFull> characters;
	
	//--------------------------------------------------------------------
	public SessionContextImpl() {
		characters = new HashMap<Player, CharacterHandleFull>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#getAdventure()
	 */
	@Override
	public Adventure getAdventure() {
		if (adventureResolved!=null)
			return adventureResolved;
		
		logger.warn("No adventure for session in group "+group);
//		for (Adventure adv : RPGFrameworkLoader.getInstance().getSessionService().getAdventures()) {
//			if (adv.g)
//		}
		
		return null;
	}

	//-------------------------------------------------------------------
	public void setAdventure(Adventure adv) {
		adventureResolved = adv;
	}

	//-------------------------------------------------------------------
	public void setGroup(Group grp) {
		this.group = grp;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#getGroup()
	 */
	@Override
	public Group getGroup() {
		return group;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#setCharacter(de.rpgframework.core.Player, de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public void setCharacter(Player player, CharacterHandleFull charac) {
		if (charac!=null)
			characters.put(player, charac);
		else
			characters.remove(player);
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#getCharacter(de.rpgframework.core.Player)
	 */
	@Override
	public CharacterHandleFull getCharacter(Player player) {
//		logger.warn("TODO: search "+player+"  in "+characters.keySet());
//		for (Player foo : characters.keySet()) {
//			logger.debug("compare "+foo.getId()+" with "+player.getId()+"     and "+foo+" with "+player);
//			if (foo.getId().equals(player.getId())) {
//				logger.debug("Found it");
//				return characters.get(foo);
//			}
//				
//		}
//		logger.debug(player.getName()+" plays "+characters.get(player));
		return characters.get(player);
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SessionContext other) {
		if (lastTime!=null && other.getLastTime()!=null)
			return lastTime.compareTo(other.getLastTime());
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#getLastTime()
	 */
	@Override
	public Instant getLastTime() {
		return lastTime;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#setLastTime(java.time.Instant)
	 */
	@Override
	public void setLastTime(Instant lastTime) {
		this.lastTime = lastTime;
	}

}

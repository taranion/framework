/**
 * 
 */
package org.prelle.rpgframework.support.combat.map;

import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;

import org.apache.log4j.Logger;

import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.map.BattleMap;
import de.rpgframework.support.combat.map.BattleMapListener;

/**
 * @author prelle
 *
 */
public abstract class CommonBattleMapImpl<C extends Combatant, R> implements BattleMap<C, R> {
	
	private final static Logger logger = Logger.getLogger("combat.map.constellation");
	
	private Map<BattleMapListener, BattleMapListener> listener;

	//-------------------------------------------------------------------
	/**
	 */
	protected CommonBattleMapImpl() {
		listener = new WeakHashMap<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#addBattleMapListener(de.rpgframework.support.combat.map.BattleMapListener)
	 */
	@Override
	public void addBattleMapListener(BattleMapListener callback) {
		if (!listener.containsKey(callback))
			listener.put(callback, callback);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#removeBattleMapListener(de.rpgframework.support.combat.map.BattleMapListener)
	 */
	@Override
	public void removeBattleMapListener(BattleMapListener callback) {
		listener.remove(callback);
	}

	//-------------------------------------------------------------------
	protected void fireMapChange() {
		for (BattleMapListener callback : new ArrayList<BattleMapListener>(listener.keySet())) {
			try {
				callback.battleMapChanged();
			} catch (Exception e) {
				logger.error("Failed delivering BattleMap event",e);
			}
		}
	}

}

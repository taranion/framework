/**
 * 
 */
package org.prelle.rpgframework.support.combat.map;

import de.rpgframework.support.combat.map.Direction;

/**
 * @author prelle
 *
 */
public class MatrixPositionImpl {

	private	int x;
	private	int y;
	private	Direction facing;

	//-------------------------------------------------------------------
	public MatrixPositionImpl(int x, int y)  {
		this.x = x;
		this.y = y;
		facing = Direction.NORTH;
	}

	//-------------------------------------------------------------------
	public void set(int x, int y) {
		this.x = x;
		this.y = y;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the facing
	 */
	public Direction getFacing() {
		return facing;
	}
}

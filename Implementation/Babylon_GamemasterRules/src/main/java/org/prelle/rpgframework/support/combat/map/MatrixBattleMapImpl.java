/**
 * 
 */
package org.prelle.rpgframework.support.combat.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.rpgframework.support.combat.Combat;
import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.map.MatrixBattleMap;
import de.rpgframework.support.combat.map.MatrixMap;
import de.rpgframework.support.combat.map.PossibleTarget;
import de.rpgframework.support.combat.map.RangeCalculator;

/**
 * @author prelle
 *
 */
public class MatrixBattleMapImpl<C extends Combatant,R> extends CommonBattleMapImpl<C, R> implements MatrixBattleMap<C,R> {
	
	private final static Logger logger = Logger.getLogger("rpgframework.combat");
	
	private MatrixMap realMap;
	private Combat<C,R> combat;
	private Map<C, MatrixPositionImpl> positions;
	@SuppressWarnings("unused")
	private RangeCalculator<R> rangeCalculator;

	//-------------------------------------------------------------------
	/**
	 */
	public MatrixBattleMapImpl(Combat<C,R> combat) {
		this.combat = combat;
		positions = new HashMap<C, MatrixPositionImpl>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getUnpositioned()
	 */
	@Override
	public List<C> getUnpositioned() {
		List<C> ret = new ArrayList<C>();
		for (C comb : combat.getCombatants()) {
			if (!positions.containsKey(comb))
				ret.add(comb);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getHeroes()
	 */
	@Override
	public List<C> getPositioned() {
		List<C> ret = new ArrayList<C>();
		for (C comb : combat.getCombatants()) {
			if (positions.containsKey(comb))
				ret.add(comb);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#setRangeCalculator(de.rpgframework.support.combat.map.RangeCalculator)
	 */
	@Override
	public void setRangeCalculator(RangeCalculator<R> calculator) {
		this.rangeCalculator = calculator;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getOpponentsInRange(de.rpgframework.support.combat.Combatant, java.lang.Object)
	 */
	@Override
	public List<PossibleTarget<C, R>> getOpponentsInRange(C comb, R range) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixBattleMap#getPosition(de.rpgframework.support.combat.Combatant)
	 */
	@Override
	public int[] getPosition(C comb) {
		MatrixPositionImpl pos = positions.get(comb);
		if (pos==null)
			return null;
		return new int[]{pos.getX(), pos.getY()};
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixBattleMap#setPosition(de.rpgframework.support.combat.Combatant, int, int)
	 */
	@Override
	public void setPosition(C comb, int x, int y) {
		logger.info("Move combatant "+comb+" to "+x+","+y);
		MatrixPositionImpl pos = positions.get(comb);
		if (pos==null) {
			pos = new MatrixPositionImpl(x, y);
			positions.put(comb, pos);
		}
		pos.set(x,y);
		
		// Inform listener
		fireMapChange();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getCombat()
	 */
	@Override
	public Combat<C, R> getCombat() {
		return combat;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 24;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 13;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixBattleMap#getMap()
	 */
	@Override
	public MatrixMap getMap() {
		return realMap;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixBattleMap#setMap(de.rpgframework.support.combat.map.MatrixMap)
	 */
	@Override
	public void setMap(MatrixMap map) {
		this.realMap = map;
	}

}

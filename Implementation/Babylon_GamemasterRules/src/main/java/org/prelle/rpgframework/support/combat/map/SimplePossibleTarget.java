/**
 * 
 */
package org.prelle.rpgframework.support.combat.map;

import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.map.PossibleTarget;

/**
 * @author Stefan
 *
 */
public class SimplePossibleTarget<C extends Combatant,R> implements PossibleTarget<C,R> {

	private C comb;
	private R range;
	
	//--------------------------------------------------------------------
	public SimplePossibleTarget(C comb, R range) {
		this.comb = comb;
		this.range= range;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.PossibleTarget#getCombatant()
	 */
	@Override
	public C getCombatant() {
		return comb;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the range
	 */
	public R getRange() {
		return range;
	}

	//--------------------------------------------------------------------
	/**
	 * @param range the range to set
	 */
	void setRange(R range) {
		this.range = range;
	}

}

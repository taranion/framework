/**
 * 
 */
package org.prelle.rpgframework.support.combat.map;

import org.prelle.rpgframework.media.AbstractMedia;

import de.rpgframework.support.combat.map.MatrixMap;

/**
 * @author prelle
 *
 */
public class MatrixMapImpl extends AbstractMedia implements MatrixMap {

	//-------------------------------------------------------------------
	/**
	 */
	public MatrixMapImpl() {
		super.category = Category.BATTLEMAP_STATIC;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#getWidth()
	 */
	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#getHeight()
	 */
	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#renderImage(int, int, int, int)
	 */
	@Override
	public byte[] renderImage(int x, int y, int w, int h) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#getFieldSize()
	 */
	@Override
	public double getFieldSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#setFieldSize(double)
	 */
	@Override
	public void setFieldSize(double value) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#getOffsetX()
	 */
	@Override
	public int getOffsetX() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#setOffsetX(int)
	 */
	@Override
	public void setOffsetX(int value) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#getOffsetY()
	 */
	@Override
	public int getOffsetY() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.MatrixMap#setOffsetY(int)
	 */
	@Override
	public void setOffsetY(int value) {
		// TODO Auto-generated method stub

	}

}

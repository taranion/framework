/**
 * 
 */
package org.prelle.rpgframework.support.combat.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.rpgframework.support.combat.Combat;
import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.map.ConstellationBattleMap;
import de.rpgframework.support.combat.map.PossibleTarget;

/**
 * @author prelle
 *
 */
public class ConstellationBattleMapImpl<C extends Combatant, R> extends CommonBattleMapImpl<C, R> implements ConstellationBattleMap<C, R> {
	
	private final static Logger logger = Logger.getLogger("combat.map.constellation");

	private Combat<C,R> combat;
	private Map<Combatant, Collection<PossibleTarget<C,R>>> whoFightsWho;

	//-------------------------------------------------------------------
	/**
	 */
	public ConstellationBattleMapImpl(Combat<C,R> combat) {
		this.combat = combat;
		whoFightsWho = new HashMap<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getUnpositioned()
	 */
	@Override
	public List<C> getUnpositioned() {
		List<C> ret = new ArrayList<C>();
		for (C key : combat.getCombatants()) {
			if (!whoFightsWho.containsKey(key))
				ret.add(key);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getPositioned()
	 */
	@Override
	public List<C> getPositioned() {
		List<C> ret = new ArrayList<C>();
		for (C key : combat.getCombatants()) {
			if (whoFightsWho.containsKey(key))
				ret.add(key);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getOpponentsInRange(de.rpgframework.support.combat.Combatant, java.lang.Object)
	 */
	@Override
	public List<PossibleTarget<C, R>> getOpponentsInRange(C comb, R range) {
		logger.warn("TODO: getOpponentsInRange()");
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.ConstellationBattleMap#setConstellation(de.rpgframework.support.combat.Combatant, de.rpgframework.support.combat.Combatant, java.lang.Object)
	 */
	@Override
	public void setConstellation(C attacker, C target, R range) {
		// Add anew
		Collection<PossibleTarget<C,R>> previous1 = whoFightsWho.get(attacker);
		if (previous1==null) {
			previous1 = new ArrayList<PossibleTarget<C,R>>();
			whoFightsWho.put(attacker, previous1);
			previous1.add(new SimplePossibleTarget<C,R>(target, range));
		} else {
			// Ensure target is not already in list
			boolean found = false;
			for (PossibleTarget<C,R> poss : new ArrayList<>(previous1)) {
				if (poss.getCombatant()==target) {
					// 
					((SimplePossibleTarget<C,R>)poss).setRange(range);
					found = true;
				}
			}
			if (!found)
				previous1.add(new SimplePossibleTarget<C,R>(target, range));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.map.BattleMap#getCombat()
	 */
	@Override
	public Combat<C, R> getCombat() {
		return combat;
	}

}

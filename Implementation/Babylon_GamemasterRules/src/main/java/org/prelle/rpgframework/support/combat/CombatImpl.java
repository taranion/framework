/**
 * 
 */
package org.prelle.rpgframework.support.combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import de.rpgframework.support.combat.Combat;
import de.rpgframework.support.combat.CombatEvent;
import de.rpgframework.support.combat.CombatEventType;
import de.rpgframework.support.combat.CombatListener;
import de.rpgframework.support.combat.Combatant;
import de.rpgframework.support.combat.Party;
import de.rpgframework.support.combat.map.BattleMap;

/**
 * @author prelle
 *
 */
public class CombatImpl<C extends Combatant,R> implements Combat<C,R> {

	private final static Logger logger = Logger.getLogger("rpgframework.combat");
	
	private List<CombatListener<C,R>> listener;
	private List<Party<C>> parties;
	private State state;
	private BattleMap<C, R> battlemap;

	//-------------------------------------------------------------------
	/**
	 */
	public CombatImpl() {
		listener = new ArrayList<>();
		parties = new ArrayList<Party<C>>();
		state   = State.CHOOSE_COMBATANTS;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#addGroup(de.rpgframework.support.combat.Party)
	 */
	@Override
	public void addGroup(Party<C> grp) {
		logger.warn("addGroup("+grp+")");
//		if (parties.contains(grp))
//			return;
		
		parties.add(grp);
		fireEvent(CombatEventType.PARTY_ADDED, grp);
		for (C tmp : grp)
			fireEvent(CombatEventType.COMBATANT_ADDED, tmp);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#addAsGroup(de.rpgframework.support.combat.Combatant[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Party<C> addAsGroup(C... combatants) {
		logger.warn("addAsGroup("+combatants+")");
		Party<C> party = new Party<>();
		party.addAll(Arrays.asList(combatants));
		
		addGroup(party);
		return party;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#addToGroup(de.rpgframework.support.combat.Party, de.rpgframework.support.combat.Combatant[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void addToGroup(Party<C> party, C... combatants) {
		logger.warn("addToGroup("+party+": "+combatants+")");
		if (!parties.contains(party))
			throw new NoSuchElementException("Group is no existing party in this combat");
		
		party.addAll(Arrays.asList(combatants));
		
		for (C tmp : combatants)
			fireEvent(CombatEventType.COMBATANT_ADDED, tmp);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#removeFromGroup(de.rpgframework.support.combat.Party, de.rpgframework.support.combat.Combatant[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void removeFromGroup(Party<C> party, C... combatants) {
		if (!parties.contains(party))
			throw new NoSuchElementException("Group is no existing party in this combat");
		
		party.removeAll(Arrays.asList(combatants));
		
		for (C tmp : combatants)
			fireEvent(CombatEventType.COMBATANT_REMOVED, tmp);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#getGroups()
	 */
	@Override
	public List<Party<C>> getGroups() {
		return new ArrayList<>(parties);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#getCombatants()
	 */
	@Override
	public List<C> getCombatants() {
		List<C> ret = new ArrayList<>();
		for (Party<C> grp : parties)
			ret.addAll(grp);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#getState()
	 */
	@Override
	public State getState() {
		return state;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#setState(de.rpgframework.support.combat.Combat.State)
	 */
	@Override
	public void setState(State newState) {
		// TODO Auto-generated method stub
		State oldState = state;
		state = newState;
		if (oldState!=newState)
			fireEvent(CombatEventType.COMBAT_STATE_CHANGED, newState);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#addListener(de.rpgframework.support.combat.CombatListener)
	 */
	@Override
	public void addListener(CombatListener<C,R> callback) {
		if (!listener.contains(callback)) {
			logger.debug("Add "+callback+" as listener");
			listener.add(callback);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#removeListener(de.rpgframework.support.combat.CombatListener)
	 */
	@Override
	public void removeListener(CombatListener<C,R> callback) {
		listener.remove(callback);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#fireEvent(de.rpgframework.support.combat.CombatEventType, java.lang.Object[])
	 */
	@Override
	public void fireEvent(CombatEventType type, Object... data) {
		CombatEvent<C,R> event = new CombatEvent<C,R>(this, type, data);
		logger.info("Fire "+type+" "+Arrays.toString(data));
		
		for (CombatListener<C,R> callback : listener) {
			try {
				callback.combatChanged(event);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//-------------------------------------------------------------------
	public void setBattleMap(BattleMap<C, R> data) {
		this.battlemap = data;
		fireEvent(CombatEventType.BATTLEMAP_DEFINED, data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.support.combat.Combat#getBattleMap()
	 */
	@Override
	public BattleMap<C, R> getBattleMap() {
		return battlemap;
	}

}

/**
 * 
 */
package org.prelle.rpgframework.genericrpg.modification;

import java.util.Date;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public abstract class ModificationBase<T> implements Modification {
	
	
	protected int expCost;
	protected Date date;

	//--------------------------------------------------------------------
	public ModificationBase() {
	}

	//-------------------------------------------------------------------
    public Modification clone() {
    	try {
    		return (Modification) super.clone();
    	} catch ( CloneNotSupportedException e ) {
    		throw new InternalError();
    	}
    }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification other) {
		return date.compareTo(other.getDate());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {		
		return expCost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
		this.expCost = expCost;
	}

}

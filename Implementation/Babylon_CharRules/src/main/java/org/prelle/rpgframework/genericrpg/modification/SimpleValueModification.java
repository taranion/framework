/**
 * 
 */
package org.prelle.rpgframework.genericrpg.modification;

import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public class SimpleValueModification<T> extends ModificationBase<T> implements ValueModification<T> {

	private T attribute;
	private int val;
	private transient Object source;
	
	//--------------------------------------------------------------------
	public SimpleValueModification() {
	}
	
	//--------------------------------------------------------------------
	public SimpleValueModification(T att, int val) {
		this.attribute = att;
		this.val = val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.ValueModification#getModifiedItem()
	 */
	@Override
	public T getModifiedItem() {
		return attribute;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.ValueModification#getValue()
	 */
	@Override
	public int getValue() {
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.ValueModification#setValue(int)
	 */
	@Override
	public void setValue(int value) {
		this.val = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

}

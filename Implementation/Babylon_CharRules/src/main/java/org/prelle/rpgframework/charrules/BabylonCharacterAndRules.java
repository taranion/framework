/**
 * 
 */
package org.prelle.rpgframework.charrules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.BabylonPlugin;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.LoadProductsBootStep;
import org.prelle.rpgframework.LoadRulePluginsBootStep;
import org.prelle.rpgframework.RPGFrameworkImpl;
import org.prelle.rpgframework.character.BaseCharacterProviderLight;
import org.prelle.rpgframework.character.EventingCharacterProviderLight;
import org.prelle.rpgframework.print.PrintManagerImpl;
import org.prelle.rpgframework.products.ProductServiceImpl;

import de.rpgframework.FunctionCharacterAndRules;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader.FunctionType;
import de.rpgframework.RulePlugin;
import de.rpgframework.RulePluginFeatures;
import de.rpgframework.boot.StandardBootSteps;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.print.PrintManager;
import de.rpgframework.products.ProductService;

/**
 * @author prelle
 *
 */
public class BabylonCharacterAndRules implements BabylonPlugin, FunctionCharacterAndRules {
	
	private final static Logger logger = Logger.getLogger("babylon.car");

	private CharacterProvider characterService;
	private ProductServiceImpl productService;
	private PrintManagerImpl printService;
	
	private RPGFrameworkInitCallback callback;
	
	private LoadRulePluginsBootStep rulePlugins;

	//-------------------------------------------------------------------
	public BabylonCharacterAndRules() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#getType()
	 */
	@Override
	public FunctionType getType() {
		return FunctionType.CHARACTERS_AND_RULES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#initialize(org.prelle.rpgframework.RPGFrameworkImpl, de.rpgframework.ConfigContainer, de.rpgframework.RPGFrameworkInitCallback)
	 */
	@Override
	public void initialize(RPGFrameworkImpl fwImpl, ConfigContainerImpl configRoot, RPGFrameworkInitCallback callback, List<RoleplayingSystem> limit) {
		logger.debug("START: initialize");
		this.callback = callback;
//		this.configRoot = configRoot;
		try {
			characterService = new EventingCharacterProviderLight(new BaseCharacterProviderLight(configRoot));
			productService   = new ProductServiceImpl(configRoot);
			printService     = new PrintManagerImpl(configRoot);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		fwImpl.setCharacterAndRules(this);

//		loadRulePlugins(fwImpl, limit);
//		
//		productService.initialize();

		rulePlugins = new LoadRulePluginsBootStep(configRoot);
		fwImpl.addStepDefinition(StandardBootSteps.ROLEPLAYING_SYSTEMS, rulePlugins);
		fwImpl.addStepDefinition(StandardBootSteps.PRODUCT_DATA, new LoadProductsBootStep(productService));
		logger.debug("STOP : initialize");
	}
	
	//--------------------------------------------------------------------
	private boolean isPluginLoaded(List<RulePlugin<?>> alreadyLoaded, RoleplayingSystem rules, String search) {
		// Find all loaded plugins with the required
		for (RulePlugin<?> loaded : alreadyLoaded) {
			if (loaded.getRules()==rules && loaded.getID().equals(search))
				return true;
		}
		return false;
	}
	
//	//--------------------------------------------------------------------
//	private void loadRulePlugins(RPGFramework instance, List<RoleplayingSystem> limit) {
//		logger.debug("START----------Load rule plugins------------------------");
//		if (callback!=null)
//			callback.message("Initialize rule plugins");
//		if (callback!=null)
//			callback.progressChanged(0.01);
//		
//		
//		
//		@SuppressWarnings("rawtypes")
//		Iterator<RulePlugin> it = ServiceLoader.load(RulePlugin.class, RPGFramework.class.getClassLoader()).iterator();
//		while (it.hasNext()) {
//			try {
//				@SuppressWarnings("rawtypes")
//				RulePlugin plugin = it.next();
//				if (limit!=null && !limit.contains(plugin.getRules())) {
//					logger.warn("Ignore plugin "+plugin.getReadableName()+" / "+plugin.getClass().getSimpleName()+" by user configuration");
//					continue;
//				}
//				if (callback!=null)
//					callback.message("Load "+plugin.getClass());
//				logger.info("Found rule plugin "+plugin.getClass().getSimpleName());
//				rulePlugins.add(plugin);
//			} catch (Throwable e) {
//				logger.fatal("Error instantiating plugin",e);
//				e.printStackTrace();
//			}
//		}
//		/*
//		 * Now sort plugins. First by roleplaying system, than by features
//		 */
//		logger.debug("Sort plugins");
//		try {
//			Collections.sort(rulePlugins, new Comparator<RulePlugin<?>>() {
//				public int compare(RulePlugin<?> o1, RulePlugin<?> o2) {
//					if (o1.getRules()!=o2.getRules()) {
//						return Integer.compare(o1.getRules().ordinal(), o2.getRules().ordinal());
//					}
//					if (o1.getSupportedFeatures().contains(RulePluginFeatures.PERSISTENCE)) return -1;
//					return ((Integer)o1.getRequiredPlugins().size()).compareTo(o2.getRequiredPlugins().size());
//				}
//			});
//		} catch (Throwable e) {
//			logger.fatal("Cannot sort plugins: "+e);
//			e.printStackTrace();
//		}
//		// Make multiple iterations
//		List<RulePlugin<?>> notLoaded  = new ArrayList<>(rulePlugins);
//		List<RulePlugin<?>> successful = new ArrayList<>();
//		boolean changed = false;
//		do {
//			changed = false;
//			outer:
//			for (RulePlugin<?> plugin : notLoaded) {
//				// Are requirements met
//				try {
//					for (String search : plugin.getRequiredPlugins()) {
//						if (!isPluginLoaded(successful, plugin.getRules(), search)) {
//							logger.debug("Cannot load "+plugin.getRules()+"/"+plugin.getID()+" yet, because "+plugin.getRules()+"/"+search+" is missing");
//							continue outer;
//						}
//					}
//				} catch (Throwable e) {
//					System.err.println("Error determing plugin requirements for "+plugin.getClass()+": "+e);
//					e.printStackTrace();
//					logger.fatal("Error determing plugin requirements for "+plugin.getClass()+": "+e,e);
//				}
//
//				Package pack = plugin.getClass().getPackage();
//				logger.debug("Initialize "+plugin.getClass()+" // "+pack.getImplementationTitle()+" // "+pack.getImplementationVersion());
//				try {
//					if (callback!=null)
//						callback.message("Initialize "+plugin.getClass().getSimpleName());
//					plugin.init();
//					plugin.attachConfigurationTree(instance.getPluginConfigurationNode());
//					changed = true;
//					successful.add(plugin);
//					double percent = ((double)successful.size()) / ((double)rulePlugins.size());
//					if (callback!=null)
//						callback.progressChanged(0.5*percent);
//				} catch (Throwable e) {
//					System.err.println("Error loading plugin: "+e);
//					e.printStackTrace();
//					logger.fatal("Error loading plugin: "+e,e);
//					if (callback!=null)
//						callback.errorOccurred("RPGFrameworkLoader", "Error loading plugin "+plugin.getID(), e);
//				}
//			}
//			notLoaded.removeAll(successful);
//		} while (changed);
//		
//		rulePlugins.removeAll(notLoaded);
//		for (RulePlugin<?> failed : notLoaded) {
//			logger.fatal("Failed loading plugin "+failed.getRules()+"/"+failed.getID()+" // "+failed.getReadableName());
//			for (String search : failed.getRequiredPlugins()) {
//				if (!isPluginLoaded(successful, failed.getRules(), search)) {
//					logger.fatal("Cannot load "+failed.getRules()+"/"+failed.getID()+" yet, because "+failed.getRules()+"/"+search+" is missing");
//					if (callback!=null)
//						callback.errorOccurred("RPGFrameworkLoader", "Cannot load "+failed.getRules()+"/"+failed.getID()+" yet, because "+failed.getRules()+"/"+search+" is missing", null);
//				}
//			}
//		}
//		logger.debug("STOP ----------Load rule plugins------------------------");
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getRulePlugins()
	 */
	@Override
	public List<RulePlugin<?>> getRulePlugins() {
		return rulePlugins.getRulePlugins();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getCharacterService()
	 */
	@Override
	public CharacterProvider getCharacterService() {
		return characterService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getProductService()
	 */
	@Override
	public ProductService getProductService() {
		return productService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getPrintManager()
	 */
	@Override
	public PrintManager getPrintManager() {
		return printService;
	}

}

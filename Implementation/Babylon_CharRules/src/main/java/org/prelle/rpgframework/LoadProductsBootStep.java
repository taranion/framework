/**
 *
 */
package org.prelle.rpgframework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

import org.apache.log4j.Logger;
import org.prelle.rpgframework.products.ProductServiceImpl;

import de.rpgframework.ConfigOption;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.RulePlugin;
import de.rpgframework.RulePluginFeatures;
import de.rpgframework.boot.BootStep;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author Stefan
 *
 */
public class LoadProductsBootStep implements BootStep {

	private final static Logger logger = Logger.getLogger("babylon.car");

	private ProductServiceImpl products;

	//-------------------------------------------------------------------
	public LoadProductsBootStep(ProductServiceImpl products) {
		this.products = products;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getID()
	 */
	@Override
	public String getID() {
		return "PRODUCT_DATA";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getWeight()
	 */
	@Override
	public int getWeight() {
		return 20;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#shallBeDisplayedToUser()
	 */
	@Override
	public boolean shallBeDisplayedToUser() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#execute()
	 */
	@Override
	public boolean execute(RPGFrameworkInitCallback callback) {
		logger.debug("START----------Load product data------------------------");
		try {
			logger.info("Call ProductServiceImpl.initialize()");
			products.initialize();
		} catch (Throwable e) {
			logger.error("Failed loading product data",e);
			return false;
		} finally {
			logger.debug("STOP: loadProductData--------------------------");
		}
		return false;
	}

}

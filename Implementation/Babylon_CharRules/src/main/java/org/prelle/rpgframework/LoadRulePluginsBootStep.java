/**
 *
 */
package org.prelle.rpgframework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import org.apache.log4j.Logger;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.ConfigOption.Type;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.RulePlugin;
import de.rpgframework.RulePluginFeatures;
import de.rpgframework.boot.BootStep;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author Stefan
 *
 */
public class LoadRulePluginsBootStep implements BootStep {

	private final static Logger logger = Logger.getLogger("babylon.car");

	private List<RulePlugin<?>> rulePlugins;
	private List<RulePlugin<?>> acceptedRulePlugins;
	private ConfigContainer configRoot;
	private ConfigOption<Boolean> cfgAskOnStartup;
	private Map<RoleplayingSystem, ConfigOption<Boolean>> cfgPerRule;

	//-------------------------------------------------------------------
	public LoadRulePluginsBootStep(ConfigContainerImpl configRoot2) {
		this.configRoot = configRoot2;
		rulePlugins = new ArrayList<RulePlugin<?>>();
		acceptedRulePlugins = new ArrayList<RulePlugin<?>>();
		cfgPerRule  = new HashMap<RoleplayingSystem, ConfigOption<Boolean>>();

		this.configRoot = configRoot.createContainer("rules");
		cfgAskOnStartup = configRoot.createOption("askOnStartUp", Type.BOOLEAN, Boolean.TRUE);

		@SuppressWarnings("rawtypes")
		Iterator<RulePlugin> it = ServiceLoader.load(RulePlugin.class, RPGFramework.class.getClassLoader()).iterator();
		while (it.hasNext()) {
			try {
				RulePlugin<?> plugin = it.next();
				rulePlugins.add(plugin);
			} catch (Throwable e) {
				logger.fatal("Error instantiating plugin",e);
				e.printStackTrace();
			}
		}
		/*
		 * Now sort plugins. First by roleplaying system, than by features
		 */
		logger.debug("Sort plugins");
		try {
			Collections.sort(rulePlugins, new Comparator<RulePlugin<?>>() {
				public int compare(RulePlugin<?> o1, RulePlugin<?> o2) {
					if (o1.getRules()!=o2.getRules()) {
						return Integer.compare(o1.getRules().ordinal(), o2.getRules().ordinal());
					}
					if (o1.getSupportedFeatures().contains(RulePluginFeatures.PERSISTENCE)) return -1;
					return ((Integer)o1.getRequiredPlugins().size()).compareTo(o2.getRequiredPlugins().size());
				}
			});
		} catch (Throwable e) {
			logger.fatal("Cannot sort plugins: "+e);
			e.printStackTrace();
		}

		/*
		 * Detect all possible RoleplayingSystems
		 */
		List<RoleplayingSystem> rules = new ArrayList<RoleplayingSystem>();
		ConfigContainer perSystem = this.configRoot.createContainer("perSystem");
		for (RulePlugin<?> tmp : rulePlugins) {
			if (!rules.contains(tmp.getRules())) {
				rules.add(tmp.getRules());
				ConfigOption<Boolean> cfgRule = perSystem.createOption(tmp.getRules().name(), Type.BOOLEAN, true);
				cfgRule.setName(tmp.getRules().getName());
				cfgPerRule.put(tmp.getRules(), cfgRule);
			}
		}
		logger.debug("Available roleplaying systems: "+rules);
	}

	//-------------------------------------------------------------------
	public List<RulePlugin<?>> getRulePlugins() {
		return acceptedRulePlugins;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getID()
	 */
	@Override
	public String getID() {
		return "ROLEPLAYINGSYSTEMS";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getWeight()
	 */
	@Override
	public int getWeight() {
		return 40;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#shallBeDisplayedToUser()
	 */
	@Override
	public boolean shallBeDisplayedToUser() {
		return (Boolean)cfgAskOnStartup.getValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		List<ConfigOption<?>> ret = new ArrayList<ConfigOption<?>>();
		ret.addAll(cfgPerRule.values());
		ret.add(cfgAskOnStartup);
		return ret;
	}

	//--------------------------------------------------------------------
	private boolean isPluginLoaded(List<RulePlugin<?>> alreadyLoaded, RoleplayingSystem rules, String search) {
		// Find all loaded plugins with the required
		for (RulePlugin<?> loaded : alreadyLoaded) {
			if (loaded.getRules()==rules && loaded.getID().equals(search))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#execute()
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public boolean execute(RPGFrameworkInitCallback callback) {
		logger.debug("START----------Load rule plugins------------------------");
		if (callback!=null) {
			callback.progressChanged(0);
			callback.message("Initialize rule plugins");
		}

		Iterator<RulePlugin> it = ServiceLoader.load(RulePlugin.class, RPGFramework.class.getClassLoader()).iterator();
		while (it.hasNext()) {
			try {
				RulePlugin plugin = it.next();
				ConfigOption<Boolean> opt = cfgPerRule.get(plugin.getRules());
				if (opt!=null && opt.getValue()==Boolean.FALSE) {
					logger.warn("Ignore plugin "+plugin.getReadableName()+" / "+plugin.getClass().getSimpleName()+" by user configuration");
					continue;
				}
				if (callback!=null)
					callback.message("Load "+plugin.getClass());
				logger.info("Found rule plugin "+plugin.getClass().getSimpleName());
				acceptedRulePlugins.add(plugin);
			} catch (Throwable e) {
				logger.fatal("Error instantiating plugin",e);
				e.printStackTrace();
			}
		}

		// Make multiple iterations
		List<RulePlugin<?>> notLoaded  = new ArrayList<>(acceptedRulePlugins);
		List<RulePlugin<?>> successful = new ArrayList<>();
		boolean changed = false;
		do {
			changed = false;
			outer:
			for (RulePlugin<?> plugin : notLoaded) {
				// Are requirements met
				try {
					for (String search : plugin.getRequiredPlugins()) {
						if (!isPluginLoaded(successful, plugin.getRules(), search)) {
							logger.debug("Cannot load "+plugin.getRules()+"/"+plugin.getID()+" yet, because "+plugin.getRules()+"/"+search+" is missing");
							continue outer;
						}
					}
				} catch (Throwable e) {
					System.err.println("Error determing plugin requirements for "+plugin.getClass()+": "+e);
					e.printStackTrace();
					logger.fatal("Error determing plugin requirements for "+plugin.getClass()+": "+e,e);
				}

				Package pack = plugin.getClass().getPackage();
				logger.debug("Initialize "+plugin.getClass()+" // "+pack.getImplementationTitle()+" // "+pack.getImplementationVersion());
				try {
					if (callback!=null)
						callback.message("Initialize "+plugin.getClass().getSimpleName());
					plugin.init();
					plugin.attachConfigurationTree(RPGFrameworkLoader.getInstance().getPluginConfigurationNode());
					changed = true;
					successful.add(plugin);
					double percent = ((double)successful.size()) / ((double)rulePlugins.size());
					if (callback!=null)
						callback.progressChanged(0.5*percent);
				} catch (Throwable e) {
					System.err.println("Error loading plugin: "+e);
					e.printStackTrace();
					logger.fatal("Error loading plugin: "+e,e);
					if (callback!=null)
						callback.errorOccurred("RPGFrameworkLoader", "Error loading plugin "+plugin.getID(), e);
				}
			}
			notLoaded.removeAll(successful);
		} while (changed);

		rulePlugins.removeAll(notLoaded);
		for (RulePlugin<?> failed : notLoaded) {
			logger.fatal("Failed loading plugin "+failed.getRules()+"/"+failed.getID()+" // "+failed.getReadableName());
			for (String search : failed.getRequiredPlugins()) {
				if (!isPluginLoaded(successful, failed.getRules(), search)) {
					logger.fatal("Cannot load "+failed.getRules()+"/"+failed.getID()+" yet, because "+failed.getRules()+"/"+search+" is missing");
					if (callback!=null)
						callback.errorOccurred("RPGFrameworkLoader", "Cannot load "+failed.getRules()+"/"+failed.getID()+" yet, because "+failed.getRules()+"/"+search+" is missing", null);
				}
			}
		}
		logger.debug("STOP ----------Load rule plugins------------------------");
		// TODO Auto-generated method stub
		callback.progressChanged(1.0);
		return true;
	}

}

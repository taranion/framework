/**
 *
 */
package foo;

import java.util.List;

import org.apache.log4j.BasicConfigurator;

import de.rpgframework.ConfigOption;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.boot.StandardBootSteps;

/**
 * @author Stefan
 *
 */
public class MyApp {

	//--------------------------------------------------------------------
	public static void main(String[] args) {
//		BasicConfigurator.configure();

		RPGFramework framework = RPGFrameworkLoader.getInstance();
		System.out.println("****MyApp");
		framework.addBootStep(StandardBootSteps.ROLEPLAYING_SYSTEMS);
//		framework.addBootStep(StandardBootSteps.PRODUCT_DATA);

		System.out.println("****MyApp: Call initialize");
		framework.initialize(new RPGFrameworkInitCallback() {

			@Override
			public void progressChanged(double value) {
				System.out.println("PROGRESS: "+value);
			}

			@Override
			public void message(String mess) {
				System.out.println("MESSAGE: "+mess);
			}

			@Override
			public void errorOccurred(String location, String detail,
					Throwable exception) {
				System.out.println("ERROR: "+detail+"    at "+location);
			}

			@Override
			public void showConfigOptions(String id,
					List<ConfigOption<?>> configuration) {
				System.err.println("-----Configure "+id+"------------------");
				for (ConfigOption<?> opt : configuration) {
					System.err.println("* "+opt);
					if (opt.getLocalId().equals("SPLITTERMOND"))
						((ConfigOption<Boolean>)opt).set(false);
				}
			}
		});
	}

}
